<?php

return [
    'callback' => '/social/callback',
    'redirect' => '/',
    'vk' => [
		'appId' => '',
        'appSecret' => '',
        'appToken' => ''
    ],
    'fb' => [
        'appId' => '',
        'appSecret' => ''
    ],
    'gg' => [
        'appId' => '',
        'appSecret' => ''
    ],
    'ig' => [
        'appId' => '',
        'appSecret' => ''
    ],
];
