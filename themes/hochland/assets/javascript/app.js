var modals = [];
var registerSuccessModal,
    registerCodeModal,
    thankYouModal,
    authModal,
    failModal,
    congratsModal,
    infoModal,
    passwordRestoreModal,
    passwordRestore2Modal,
    passwordRestore3Modal,
    passwordRestore4Modal,
    feedbackOkModal;
$(function() {
    $('.form-control select').selectric({
        onInit: function() {
            $('.selectric-scroll').mCustomScrollbar()
        }
    });
    $('*').imagesLoaded({background: true}, function() {
        $('.loading-overlay').fadeOut(100);
    });

    /*$(window).on('resize', function() {
        $('.form-content').height($(window).height() - $('header').height())
    }).resize();*/
    $('body').on('click', '.mobile-nav-btn', function(e) {
        e.preventDefault();
        $('.header-nav-container').show();
        $('.overlay').show();
    })
    $('body').on('click', '.close-menu-btn', function(e) {
        e.preventDefault();
        $('.header-nav-container').hide();
        $('.overlay').hide();
    })
    $('.overlay').on('click', function() {
        $(this).hide();
        $('.header-nav-container').hide();
    })
    $(window).on('resize', function() {
        if (!Modernizr.mq('(max-width: 760px)')) {
            $('.overlay').hide();
            $('.header-nav-container').show();
        } else {
            //$('.header-nav-container').hide();
        }
    }).resize();
    $('.winners-content .tabs').tabslet({
        container: '#tabs-container'
    });

    $('.winners-content .tabs').on("_after", function() {
        if ($('.prizes-list ul li:eq(0)').hasClass('active')) {
            $('.prizes-list-stamp').removeClass('prizes-list-money');
        } else {
            $('.prizes-list-stamp').addClass('prizes-list-money');
        }
        // do stuff here
    });

    $('body').on('click', '.prizes-list ul li:eq(1) a', function(e) {
        console.log('a');
        $('.prizes-list-stamp').addClass('prizes-list-money');
    })

    $('body').on('click', '.prizes-list ul li:eq(0) a', function(e) {
        console.log('b');
        $('.prizes-list-stamp').removeClass('prizes-list-money');

    });

    $('body').on('click', '[name=code]', function() {
        setCaretToPos($(this)[0], 0)
    })
    $('body').on('click', '[name=phone]', function() {
        setCaretToPos($(this)[0], 4)
    })
});

var setSelectionRange = function(input, selectionStart, selectionEnd) {
    if (input.setSelectionRange) {
        input.focus();
        input.setSelectionRange(selectionStart, selectionEnd);
    } else if (input.createTextRange) {
        var range = input.createTextRange();
        range.collapse(true);
        range.moveEnd('character', selectionEnd);
        range.moveStart('character', selectionStart);
        range.select();
    }
}

var setCaretToPos = function(input, pos) {
    setSelectionRange(input, pos, pos);
}

var addModal = function(modal) {
    modals.push(modal);
}
var closeModals = function() {
    for (var i = 0; i < modals.length; i++) {
        modals[i].close();
        // $(modals[i].selector).find('input[type=text], input[type=password], input[type=email], input[type=tel], textarea').val('');
        modals.splice(i, 1);
    }
}
var registerSuccess = function(isClose) {
    if (isClose) {
        closeModals();
    }
    registerSuccessModal = $('.register-success-modal').bPopup({
        closeClass: 'modal-close',
        follow: [false, false],
        modalColor: 'white',
        content: 'image'
    });
    addModal(registerSuccessModal);
}
var registerCode = function(isClose) {
    if (isClose) {
        closeModals();
    }
    registerCodeModal = $('.register-code-modal').bPopup({
        closeClass: 'modal-close',
        follow: [false, false],
        modalColor: 'white',
        content: 'image'
    });
    addModal(registerCodeModal);
}
var thankYou = function(isClose) {
    if (isClose) {
        closeModals();
    }
    thankYouModal = $('.thank-you-modal').bPopup({
        closeClass: 'modal-close',
        follow: [false, false],
        modalColor: 'white',
        content: 'image',
        onClose: function() {
            if ($(this).data('href')) {
                window.location.replace($(this).data('href'));
            }
        }
    });
    addModal(thankYouModal);
}
var feedbackOk = function() {
    feedbackOkModal = $('.feedback-ok-modal').bPopup({
        // closeClass: 'modal-close',
        follow: [false, false],
        modalColor: 'white',
        content: 'image',
        onClose: function() {
            if ($(this).data('reload')) {
                window.location.reload();
            }
            if ($(this).data('href')) {
                window.location.replace($(this).data('href'));
            }
        }
    });
}
var auth = function(isClose) {
    if (isClose) {
        closeModals();
    }
    authModal = $('.auth-modal').bPopup({
        closeClass: 'modal-close',
        follow: [false, false],
        modalColor: 'white',
        content: 'image'
    });
    addModal(authModal);
    return false;
}
var fail = function(isClose) {
    if (isClose) {
        closeModals();
    }
    failModal = $('.fail-modal').bPopup({
        closeClass: 'modal-close',
        follow: [false, false],
        modalColor: 'white',
        content: 'image',
        onClose: function() {
            if ($(this).data('href')) {
                window.location.replace($(this).data('href'));
            }
        }
    });
    addModal(failModal);
}
var congrats = function(isClose) {
    if (isClose) {
        closeModals();
    }
    congratsModal = $('.congrats-modal').bPopup({
        closeClass: 'modal-close',
        follow: [false, false],
        modalColor: 'white',
        content: 'image',
        onClose: function() {
            if ($(this).data('href')) {
                window.location.replace($(this).data('href'));
            }
        }
    });
    addModal(congratsModal);
}
var passwordRestore = function(isClose) {
    if (isClose) {
        closeModals();
    }
     passwordRestoreModal = $('.password-restore-modal').bPopup({
        closeClass: 'modal-close',
        follow: [false, false],
        modalColor: 'white',
        content: 'image'
    });
    addModal(passwordRestoreModal);
}
var passwordRestore2 = function(isClose) {
    if (isClose) {
        closeModals();
    }
    passwordRestore2Modal = $('.password-restore-modal-2').bPopup({
        closeClass: 'modal-close',
        follow: [false, false],
        modalColor: 'white',
        content: 'image'
    });
    addModal(passwordRestore2Modal);
}
var passwordRestore3 = function(isClose) {
    if (isClose) {
        closeModals();
    }
    passwordRestore3Modal = $('.password-restore-modal-3').bPopup({
        closeClass: 'modal-close',
        follow: [false, false],
        modalColor: 'white',
        content: 'image',
        onClose: function() {
            if ($(this).data('href')) {
                window.location.replace($(this).data('href'));
            }
        }
    });
    addModal(passwordRestore3Modal);
}
var passwordRestore4 = function(isClose) {
    if (isClose) {
        closeModals();
    }
    passwordRestore4Modal = $('.password-restore-modal-4').bPopup({
        closeClass: 'modal-close',
        follow: [false, false],
        modalColor: 'white',
        content: 'image',
        onClose: function() {
            if ($(this).data('href')) {
                window.location.replace($(this).data('href'));
            }
        }
    });
    addModal(passwordRestore4Modal);
}
var infoShow = function(isClose) {
    if (isClose) {
        closeModals();
    }
    infoModal = $('.info-modal').bPopup({
        closeClass: 'modal-close',
        follow: [false, false],
        modalColor: 'white',
        content: 'image',
        onClose: function() {
            if ($(this).data('href')) {
                window.location.replace($(this).data('href'));
            }
        }
    });
    addModal(infoModal);
}
// /* Клик на "Забыли пароль?" */
// $('body').on('click', '.forgot-password', function(e) {
//     e.preventDefault();
//     /* Закрываем окно авторизации */
//     authModal.close();
//     /* Открываем окно восстановления пароля */
//     passwordRestore();
// })
// /* Клик на "Вернуться к авторизации" */
// $('body').on('click', '.back-to-auth a', function(e) {
//     e.preventDefault();
//     /* Закрываем окно восстановления пароля */
//     passwordRestoreModal.close();
//     /* Открываем окно авторизации */
//     auth();
// })
