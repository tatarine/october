$(function(){
    console.log('Jquery Ready!');

    $.fn.cleanVal = function(e) {
        return $(this).data($.mask.dataName)();
    }
    var showAjaxErrors = function(form) {
        $('#'+form).find('input')
            .removeClass('valid')
            .addClass('error');
        $('#'+form).find('.has-success')
            .removeClass('has-success')
            .addClass('has-ajax-error');
    };
    var showAjaxErrorsCustom = function(form, message) {
        var inputs = $('#'+form).find('[data-ajax-errors]');
        inputs.removeClass('valid')
            .addClass('error');
        inputs.parent('.has-success')
            .removeClass('has-success')
            .addClass('has-ajax-error-custom');
        $('#'+form).find('.form-ajax-errors-custom').html(message.join('<br/>'));
    };
    $('.phone-field, [name=phone]').mask('+7 (###) ### ## ##');
    $('[name=code]').mask('*********');
    /*$('input[data-toggle=mask]').each(function() {
        console.log($(this).data('mask'));
        $(this).mask($(this).data('mask'));
    });*/
    // AJAX
    $(window).on('ajaxErrorMessage', function(e, message){
        e.preventDefault();
        console.log(message);
    })
    $('form').on('ajaxError', function (e, context, status, error, data) {
        e.preventDefault();
        // console.log('AJAX', status, error, data);
        $('form').find('.error').removeClass('error');
        $('form').find('.has-ajax-error').removeClass('has-ajax-error');
        $('form').find('.has-ajax-error-custom').removeClass('has-ajax-error-custom');
        // $('.form-ajax-errors').hide();
        if (data.status == 500) {
            if (status == 'signin') {
                showAjaxErrors('form-auth');
                showAjaxErrors('form-auth-reset');
            } else if (status == 'captcha') {
                $('.g-recaptcha').parent('div').addClass('has-ajax-error');
                for (var i = 0; i < grecaptchas.length; i++) {
                    grecaptcha.reset(grecaptchas[i]);
                }
            } else if (status == 'restore') {
                showAjaxErrors('form-restore');
            } else if (status == 'reset') {
                showAjaxErrors('form-reset');
            } else {
                try {
                    var response = JSON.parse(data.responseText);
                    if (response.code == 400) {
                        if (response.errors['user_phone']) {
                            showAjaxErrorsCustom('registration', response.message);
                        } else {
                            $('.g-recaptcha').each(function(index, el) {
                                grecaptcha.reset(grecaptchas[i]);
                            });
                            showAjaxErrorsCustom('form-load', response.message);
                            showAjaxErrorsCustom('form-load-cab', response.message);
                            showAjaxErrorsCustom('form-load-greetings', response.message);
                        }
                    } else {
                        console.log('JSON parse error with status 500 and unknown code');
                    }
                } catch(e) {
                    console.log('JSON parse error with status 500');
                }
            }
        }
        if (data.status == 406) {
            try {
                var response = JSON.parse(data.responseText);
                if (response.X_OCTOBER_ERROR_FIELDS) {
                    var fields = response.X_OCTOBER_ERROR_FIELDS;
                    for (var field in fields) {
                        if (fields.hasOwnProperty(field)) {
                            $('[name=' + field + ']').removeClass('valid').addClass('error');
                            $('[name=' + field + ']').parent('div').removeClass('has-success').addClass('has-ajax-error');
                        }
                    }
                }
                for (var i = 0; i < grecaptchas.length; i++) {
                    grecaptcha.reset(grecaptchas[i]);
                }
            } catch(e) {
                console.log('JSON parse error with status 406');
            }
        }
    });

    // Validation
    $.formUtils.addValidator({
        name : 'select',
        validatorFunction : function(value, $el, config, language, $form) {
            return !!value;
        },
        errorMessage : 'Select validate error message.',
        errorMessageKey: 'badSelect'
    });

    $.formUtils.addValidator({
        name : 'autocomplete',
        validatorFunction : function(value, $el, config, language, $form) {
            return !!value;
        },
        errorMessage : 'Autocomplete validate error message.',
        errorMessageKey: 'badAutocomplete'
    });

    $.formUtils.addValidator({
        name : 'mask',
        validatorFunction : function(value, $el, config, language, $form) {
            if ($el.cleanVal()) {
                return $el.cleanVal().length == $el.data('validation-mask');
            }
            return false;
        },
        errorMessage : 'Mask validate error message.',
        errorMessageKey: 'badMask'
    });

    $.formUtils.addValidator({
        name : 'confirm',
        validatorFunction : function(value, $el, config, language, $form) {
            var name = $el.data('validation-confirm');
            var input = $form.find('input[name='+name+']');
            return value === input.val();
        },
        errorMessage : 'Confirm validate error message.',
        errorMessageKey: 'badCOnfirm'
    });

    // Registration
    $.validate({
      form : '#registration',
      validateHiddenInputs: true,
      onSuccess: function($f) {
        if(window.ga) ga('send', 'event', 'Registration', 'click');
        var phone = $('#registration').find('input[name=phone]').cleanVal();
        $('#registration').request('onRegister', {data: {phone:phone}});
      }
    });

    $('#registration-submit').on('click', function(e) {
        e.preventDefault();
        $('#registration').submit();
    });

    $('#registration').on('submit', function(e) {
        e.preventDefault();
    });

    // Authorisation
    $.validate({
      form : '#form-auth',
      onSuccess: function($f) {
        var phone = $('#form-auth').find('input[name=phone]').cleanVal();
        $('#form-auth').request('onSignin', {data: {phone:phone}});
      }
    });

    $('#form-auth-submit').on('click', function(e) {
        e.preventDefault();
        $('#form-auth').submit();
    });

    $('#form-auth').on('submit', function(e) {
        e.preventDefault();
    });

    // Authorisation
    $.validate({
      form : '#form-auth-reset',
      onSuccess: function($f) {
        var phone = $('#form-auth-reset').find('input[name=phone]').cleanVal();
        $('#form-auth-reset').request('onSignin', {data: {phone:phone}});
      }
    });

    $('#form-auth-reset-submit').on('click', function(e) {
        e.preventDefault();
        $('#form-auth-reset').submit();
    });

    $('#form-auth-reset').on('submit', function(e) {
        e.preventDefault();
    });

    // Restore access
    $.validate({
      form : '#form-restore',
      onSuccess: function($f) {
        $('#form-restore-submit').addClass('disabled');
		$('#form-restore').request('onRestorePassword', {
			success: function(data) {
				passwordRestore4(true);
			    this.success(data);
			}
		});
      }
    });

    $('#form-restore-submit').on('click', function(e) {
        e.preventDefault();
        $('#form-restore').submit();
    });

    $('#form-restore').on('submit', function(e) {
        e.preventDefault();
    });

    // Reset password
    $.validate({
      form : '#form-reset',
      onSuccess: function($f) {
        $('#form-reset-submit').addClass('disabled');
		$('#form-reset').request('onResetPassword', {
			success: function(data) {
				passwordRestore3(true);
			    this.success(data);
			}
		});
      }
    });

    $('#form-reset-submit').on('click', function(e) {
        e.preventDefault();
        $('#form-reset').submit();
    });

    $('#form-reset').on('submit', function(e) {
        e.preventDefault();
    });

    // Feedback
    $.validate({
      form : '#form-feedback',
      onSuccess: function($f) {
        $('#form-feedback-submit').addClass('disabled');//.prop('disabled', true);
        var phone = $('#form-feedback').find('input[name=phone]').cleanVal();
		$('#form-feedback').request('onFeedback', { data: {phone:phone},
			success: function(data) {
                // if(window.ga) ga('send', 'event', 'Feedback', 'success');
                console.log('Feedback success');
                feedbackOk();
			    this.success(data);
			}
		});
      }
    });

    $('#form-feedback-submit').on('click', function(e) {
        e.preventDefault();
        $('#form-feedback').submit();
    });

    $('#form-feedback').on('submit', function(e) {
        e.preventDefault();
    });

    // Load code
    $.validate({
      form : '#form-load-cab',
      onSuccess: function($f) {
        $('#form-load-cab-submit').addClass('disabled');
		$('#form-load-cab').request('onLoadCode', {
            update: {'modals/load-code-success': '#modals'},
			success: function(data) {
                for (var i = 0; i < grecaptchas.length; i++) {
                    grecaptcha.reset(grecaptchas[i]);
                }
                $('#form-load-cab').find("input[type=text], textarea").val("");
			    this.success(data);
			}
		});
      }
    });

    $('#form-load-cab-submit').on('click', function(e) {
        e.preventDefault();
        $('#form-load-cab').submit();
    });

    $('#form-load-cab').on('submit', function(e) {
        e.preventDefault();
    });

    // Load code
    $.validate({
      form : '#form-load',
      onSuccess: function($f) {
        $('#form-load-submit').addClass('disabled');
		$('#form-load').request('onLoadCode', {
            update: {'modals/load-code-success': '#modals'},
			success: function(data) {
                for (var i = 0; i < grecaptchas.length; i++) {
                    grecaptcha.reset(grecaptchas[i]);
                }
                $('#form-load').find("input[type=text], textarea").val("");
			    this.success(data);
			}
		});
      }
    });

    $('#form-load-submit').on('click', function(e) {
        e.preventDefault();
        $('#form-load').submit();
    });

    $('#form-load').on('submit', function(e) {
        e.preventDefault();
    });

    // Load code
    $.validate({
      form : '#form-load-greetings',
      onSuccess: function($f) {
        $('#form-load-greetings-submit').addClass('disabled');
		$('#form-load-greetings').request('onLoadCode', {
            update: {'modals/load-code-success': '#modals'},
			success: function(data) {
                for (var i = 0; i < grecaptchas.length; i++) {
                    grecaptcha.reset(grecaptchas[i]);
                }
                $('#form-load-greetings').find("input[type=text], textarea").val("");
			    this.success(data);
			}
		});
      }
    });

    $('#form-load-greetings-submit').on('click', function(e) {
        e.preventDefault();
        $('#form-load-greetings').submit();
    });

    $('#form-load-greetings').on('submit', function(e) {
        e.preventDefault();
    });

    // Winner Search
    $.validate({
      form : '#winners-search',
      onSuccess: function($f) {
        // _gaq.push(['_trackEvent', 'Winners', 'search']);
        if(window.ga) ga('send', 'event', 'Winners', 'search');
        $('#winners-search-submit').addClass('disabled');
        var search = $('#winners-search').find('input[name=search]').cleanVal();
        $('#winners-search').request('onSearchWinners', {data: {search:search}});
      }
    });

    $('#winners-search-submit').on('click', function(e) {
        e.preventDefault();
        $('#winners-search').submit();
    });

    $('#winners-search').on('submit', function(e) {
        e.preventDefault();
    });


    // Cabinet prizes
    // $('body').on('click', '[data-prize-check]', function(e) {
    //     e.preventDefault();
    //     var _self = $(this),
    //         data = _self.data();
    //     $.request('onCheckPrize', {
    //         data: data,
    //         success: function(data) {
	// 		    this.success(data);
	// 		}
    //     });
    // });

    // $('body').on('click', '[data-prize-confirm]', function(e) {
    //     e.preventDefault();
    //     var _self = $(this),
    //         data = _self.data();
    //     $.request('onRequestAddPrize',{
    //         data: data,
    //         success: function(data) {
    //             this.success(data);
    //         }
    //     });
    // });

    $('body').find('[data-refresh]').each(function() {
        var _self   = $(this),
            target  = _self.attr('id'),
            method  = _self.data('refresh'),
            delay   = _self.data('refresh-delay'),
            partial = _self.data('refresh-partial');
        setInterval(function(){
            $.request(method, {
                data: {target: target, partial: partial}
            });
        }, delay*1000);
        $.request(method, {
            data: {target: target, partial: partial}
        });
    });

    // $('.cabinet-prizes').on('click', '[data-message]', function(e) {
    //     e.preventDefault();
    //     if (!$(this).hasClass('has-message') && !$(this).data('message-only-close')) {
    //         $(this).addClass('has-message');
    //     }
    //     var _class = $(this).data('message-class');
    //     if (_class) {
    //         $(this).addClass(_class);
    //     }
    // });
    // $('.cabinet-prizes').on('click', '[data-message-close]', function(e) {
    //     e.preventDefault();
    //     e.stopPropagation();
    //     var _container = $(this).closest('[data-message]');
    //         _container.removeClass('has-message');
    //     var _class = _container.data('message-class');
    //     if (_class) {
    //         _container.removeClass(_class);
    //     }
    // });

    //Paginate
    $('div[data-paginate]').on('click', '[data-paginate-page]', function(e) {
        e.preventDefault();
        var data = $(this).parents('div[data-paginate]').data();
            data['paginateId'] = $(this).parents('div[data-paginate]').attr('id');
            data['page'] = $(this).data('paginate-page');
        $.request(data.paginate, {data:data});
    });
});
