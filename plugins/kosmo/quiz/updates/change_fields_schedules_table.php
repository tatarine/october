<?php namespace Kosmo\Quiz\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ChangeFieldsSchedulesTable extends Migration
{

    public function up()
    {
        Schema::table('kosmo_quiz_schedules', function($table)
        {
            $table->string('slug')->unique()->index();
            $table->text('description')->nullable();
            $table->boolean('is_enabled')->default(0);
            $table->integer('status')->default(0);
        });

        Schema::table('kosmo_quiz_schedules', function($table)
        {
            $table->dropColumn('amount');
            $table->dropColumn('prize_id');
        });
    }

    public function down()
    {
        Schema::table('kosmo_quiz_schedules', function($table)
        {
            $table->integer('prize_id')->unsigned()->index();
            $table->integer('amount')->default(0);
        });
        Schema::table('kosmo_quiz_schedules', function($table)
        {
            $table->dropColumn('slug');
            $table->dropColumn('description');
            $table->dropColumn('is_enabled');
            $table->dropColumn('status');
        });
    }

}
