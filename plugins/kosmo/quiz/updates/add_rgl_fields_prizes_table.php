<?php namespace Kosmo\Quiz\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddRglFieldsPrizesTable extends Migration
{

    public function up()
    {
        Schema::table('kosmo_quiz_prizes', function($table)
        {
            $table->integer('rgl_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kosmo_quiz_prizes', function($table)
        {
            $table->dropColumn('rgl_id');
        });
    }

}
