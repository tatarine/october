<?php namespace Kosmo\Quiz\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateWinningsTable extends Migration
{

    public function up()
    {
        Schema::create('kosmo_quiz_winnings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('prize_id')->unsigned()->index();
            $table->integer('ticket_id')->nullable()->index();
            $table->enum('status', ['raffled', 'filled', 'sent', 'delivered']);
            $table->timestamp('raffled_at')->nullable();
            $table->timestamp('filled_at')->nullable();
            $table->timestamp('sent_at')->nullable();
            $table->timestamp('delivered_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kosmo_quiz_winnings');
    }

}
