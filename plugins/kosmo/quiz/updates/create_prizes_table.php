<?php namespace Kosmo\Quiz\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePrizesTable extends Migration
{

    public function up()
    {
        Schema::create('kosmo_quiz_prizes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->text('comment')->nullable();
            $table->string('cdn_key')->nullable();
            $table->string('type')->nullable();
            $table->integer('available')->default(0);
            $table->integer('unavailable')->default(0);
            $table->integer('amount')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kosmo_quiz_prizes');
    }

}
