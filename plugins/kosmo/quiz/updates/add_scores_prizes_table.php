<?php namespace Kosmo\Quiz\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddScoresPrizesTable extends Migration
{

    public function up()
    {
        Schema::table('kosmo_quiz_prizes', function($table)
        {
            $table->integer('scores')->default(1);
        });
    }

    public function down()
    {
        Schema::table('kosmo_quiz_prizes', function($table)
        {
            $table->dropColumn('scores');
        });
    }

}
