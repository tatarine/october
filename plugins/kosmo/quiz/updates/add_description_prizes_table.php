<?php namespace Kosmo\Quiz\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddDescriptionPrizesTable extends Migration
{

    public function up()
    {
        Schema::table('kosmo_quiz_prizes', function($table)
        {
            $table->longText('description')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kosmo_quiz_prizes', function($table)
        {
            $table->dropColumn('description');
        });
    }

}
