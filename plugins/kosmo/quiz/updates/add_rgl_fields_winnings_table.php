<?php namespace Kosmo\Quiz\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddRglFieldsWinningsTable extends Migration
{

    public function up()
    {
        Schema::table('kosmo_quiz_winnings', function($table)
        {
            $table->integer('rgl_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kosmo_quiz_winnings', function($table)
        {
            $table->dropColumn('rgl_id');
        });
    }

}
