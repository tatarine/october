<?php namespace Kosmo\Quiz\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddFieldsWinningsTable extends Migration
{

    public function up()
    {
        Schema::table('kosmo_quiz_winnings', function($table)
        {
            $table->string('user_email')->nullable();
            $table->string('prize_name')->nullable();
            $table->string('ticket_number')->nullable();
            $table->string('tickets_count')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kosmo_quiz_winnings', function($table)
        {
            $table->dropColumn('user_email');
            $table->dropColumn('prize_name');
            $table->dropColumn('ticket_number');
            $table->dropColumn('tickets_count');
        });
    }

}
