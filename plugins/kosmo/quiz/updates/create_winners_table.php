<?php namespace Kosmo\Quiz\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateWinnersTable extends Migration
{
    public function up()
    {
        Schema::create('kosmo_quiz_winners', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('middlename')->nullable();
            $table->text('description')->nullable();
            $table->string('phone')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('city_name')->nullable();
            $table->integer('prize_id')->nullable();
            $table->string('prize_name')->nullable();
            $table->string('type')->nullable();
            $table->boolean('is_enabled')->default(0);
            $table->timestamp('raffled_at');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kosmo_quiz_winners');
    }
}
