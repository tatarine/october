<?php namespace Kosmo\Quiz\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateSchedulesTable extends Migration
{

    public function up()
    {
        Schema::create('kosmo_quiz_schedules', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('prize_id')->unsigned()->index();
            $table->integer('amount')->default(0);
            $table->timestamp('started_at');
            $table->timestamp('ended_at');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kosmo_quiz_schedules');
    }

}
