<?php namespace Kosmo\Quiz;

use Backend;
use System\Classes\PluginBase;

/**
 * Quiz Plugin Information File
 */
class Plugin extends PluginBase
{

    public $require = ['RainLab.Translate'];
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Quiz',
            'description' => 'Quiz plugin',
            'author'      => 'Kosmoport',
            'icon'        => 'icon-folder-o'
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'category'    => 'Quiz',
                'label'       => 'Settings',
                'description' => 'Manage quiz settings.',
                'icon'        => 'icon-birthday-cake',
                'class'       => 'Kosmo\Quiz\Models\Settings',
                'order'       => 500,
                'keywords'    => 'quiz settings',
                'permissions' => ['kosmo.quiz.access_settings']
            ]
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            // 'Kosmo\Quiz\Components\GameForm' => 'game',
            'Kosmo\Quiz\Components\WinningTable' => 'winningsTable',
            'Kosmo\Quiz\Components\ScheduleBoard' => 'scheduleBoard',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'kosmo.quiz.access' => [
                'tab' => 'Quiz',
                'label' => 'Quiz common access'
            ],
            'kosmo.quiz.settings' => [
                'tab' => 'Quiz',
                'label' => 'Quiz settings access'
            ],
            'kosmo.quiz.prizes' => [
                'tab' => 'Quiz',
                'label' => 'Prizes and schedule access'
            ],
        ];
    }


    public function registerReportWidgets()
    {
        return [];
        return [
            'Kosmo\Quiz\ReportWidgets\Winnings' => [
                'label'   => 'Winnings',
                'context' => 'dashboard'
            ],
            'Kosmo\Quiz\ReportWidgets\Votes' => [
                'label'   => 'Votes',
                'context' => 'dashboard'
            ],
        ];
    }
    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'quiz' => [
                'label'       => 'Призы',
                'url'         => Backend::url('kosmo/quiz/prize'),
                'icon'        => 'icon-folder-o',
                'permissions' => ['kosmo.quiz.*'],
                'order'       => 6,
                'sideMenu'    => [
                    'winner' => [
                        'label'     => 'Победители',
                        'icon'      => 'icon-trophy',
                        'url'       => Backend::url('kosmo/quiz/winner'),
                        'permissions' => ['kosmo.quiz.*'],
                    ],
                    'winning' => [
                        'label'     => 'Победы',
                        'icon'      => 'icon-heart',
                        'url'       => Backend::url('kosmo/quiz/winning'),
                        'permissions' => ['kosmo.quiz.*'],
                    ],
                    'prize' => [
                        'label'     => 'Призы',
                        'icon'      => 'icon-gift',
                        'url'       => Backend::url('kosmo/quiz/prize'),
                        'permissions' => ['kosmo.quiz.*'],
                    ],
                    'schedule' => [
                        'label'     => 'Расписание',
                        'icon'      => 'icon-calendar',
                        'url'       => Backend::url('kosmo/quiz/schedule'),
                        'permissions' => ['kosmo.quiz.*'],
                    ],
                ]
            ]
        ];
    }

}
