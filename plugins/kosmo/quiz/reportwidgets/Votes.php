<?php namespace Kosmo\Quiz\ReportWidgets;

use DB;
use Carbon\Carbon;
use Kosmo\Quiz\Models\Winning;
use Kosmo\Quiz\Models\Prize;
use Backend\Classes\ReportWidgetBase;

class Votes extends ReportWidgetBase
{

    public function defineProperties()
    {
        return [
            'title' => [
                'title'             => 'Widget title',
                'default'           => 'Ответы',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'The Widget Title is required.'
            ],
            'days' => [
                'title'             => 'Number of days to display data for',
                'default'           => '7',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$'
            ]
        ];
    }

    public function render()
    {
        $days = intval($this->property('days'));
        if ($days > 0) {
            $start = Carbon::now()->subDays($days)->startOfDay();
            $end = Carbon::now()->endOfDay();
        }

        $grouped = [];
        $grouped['total'] = [];

        $results = [];
        $results['success'] = 0;
        $results['fail'] = 0;
        $results['total'] = 0;

        $votes = DB::table('kosmo_quiz_level_puzzle_votes as pv')
                ->select('pv.id', 'pv.created_at', 'pv.status');
        if ($days > 0) {
            $votes = $votes->where('pv.created_at', '>=', $start)->where('pv.created_at', '<=', $end);
        }
        $votes = $votes->orderBy('pv.created_at')->get();

        if (count($votes)) {
            $date = null;
            foreach ($votes as $vote) {
                $created_at = Carbon::parse($vote->created_at);
                if (!$date || !$created_at->lte($date)) {
                    $date = $created_at->endOfDay();
                    $grouped['success'][$date->timestamp] = 0;
                    $grouped['fail'][$date->timestamp] = 0;
                    $grouped['total'][$date->timestamp] = 0;
                }
                if ($vote->status == 'success') {
                    $grouped['success'][$date->timestamp] += 1;
                    $results['success'] += 1;
                } else {
                    $grouped['fail'][$date->timestamp] += 1;
                    $results['fail'] += 1;
                }
                $grouped['total'][$date->timestamp] += 1;
                $results['total'] += 1;
            }
        }

        $this->vars['items'] = $grouped;
        $this->vars['results'] = $results;

        $grouped = [];
        $grouped['total'] = [];

        $results = [];
        $results['complete'] = 0;
        $results['nocomplete'] = 0;
        $results['unique'] = 0;
        $results['total'] = 0;

        $games = DB::table('kosmo_quiz_level_votes as v')
            ->select('v.id', 'v.user_id', 'v.status', 'v.created_at');
        if ($days > 0) {
            $games = $games->where('v.created_at', '>=', $start)->where('v.created_at', '<=', $end);
        }
        $games = $games->orderBy('v.created_at')->get();

        if (count($games)) {
            $date = null;
            $unique = [];
            foreach ($games as $game) {
                $created_at = Carbon::parse($game->created_at);
                if (!$date || !$created_at->lte($date)) {
                    $date = $created_at->endOfDay();
                    $grouped['complete'][$date->timestamp] = 0;
                    $grouped['nocomplete'][$date->timestamp] = 0;
                    $grouped['unique'][$date->timestamp] = 0;
                    $grouped['total'][$date->timestamp] = 0;
                }
                if ($game->status == 'complete') {
                    $grouped['complete'][$date->timestamp] += 1;
                    $results['complete'] += 1;
                } else {
                    $grouped['nocomplete'][$date->timestamp] += 1;
                    $results['nocomplete'] += 1;
                }
                if(!in_array($game->user_id, $unique)) {
                    $unique[] = $game->user_id;
                    $grouped['unique'][$date->timestamp] += 1;
                    $results['unique'] += 1;
                }
                $grouped['total'][$date->timestamp] += 1;
                $results['total'] += 1;
            }
        }

        $this->vars['gitems'] = $grouped;
        $this->vars['gresults'] = $results;

        return $this->makePartial('widget');
    }
}
?>
