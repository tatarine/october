<?php namespace Kosmo\Quiz\ReportWidgets;

use DB;
use Carbon\Carbon;
use Kosmo\Quiz\Models\Winning;
use Kosmo\Quiz\Models\Prize;
use Backend\Classes\ReportWidgetBase;

class Winnings extends ReportWidgetBase
{

    public function defineProperties()
    {
        return [
            'title' => [
                'title'             => 'Widget title',
                'default'           => 'Призы',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'The Widget Title is required.'
            ],
            'days' => [
                'title'             => 'Number of days to display data for',
                'default'           => '7',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$'
            ]
        ];
    }

    public function render()
    {
        $days = ($this->property('days')) ? $this->property('days') : 7;
        $types = Prize::getTypes();

        $grouped = [];
        $data = [];
        $results = [];
        $grouped['total'] = [];
        $data['total'] = [];
        $results['total'] = 0;

        foreach ($types as $key => $type) {
            $grouped[$key] = [];
            $data[$key] = [];
            $results[$key] = 0;
        }

        $subitems = [];
        $subitemType = Prize::TYPE_DAILY;
        $subitems[$subitemType] = [];
        $prizes = Prize::where('type', $subitemType)->get();

        if (!$prizes->isEmpty()) {
            foreach ($prizes as $prize) {
                $subitems[$subitemType][$prize->name] = [];
                $results[$prize->name] = 0;
            }
        }

        $winnings = DB::table('kosmo_quiz_winnings as w')
                ->leftJoin('kosmo_quiz_prizes as p', 'w.prize_id', '=', 'p.id')
                ->select('w.id', 'w.raffled_at', 'p.type', 'p.name')
                ->orderBy('w.raffled_at')
                ->get();

        $date = null;
        if (count($winnings)) {
            foreach ($winnings as $winning) {
                if ($winning->type != Prize::TYPE_WEEKLY) {
                    $raffled_at = Carbon::parse($winning->raffled_at);
                    if (!$date || !$raffled_at->lte($date)) {
                        $date = $raffled_at->endOfDay();
                        foreach ($types as $key => $type) {
                            $grouped[$key][$date->timestamp] = 0;
                        }
                        $grouped['total'][$date->timestamp] = 0;
                        if (!$prizes->isEmpty()) {
                            foreach ($prizes as $prize) {
                                $subitems[$subitemType][$prize->name][$date->timestamp] = 0;
                            }
                        }
                    }
                    $grouped[$winning->type][$date->timestamp] += 1;
                    $grouped['total'][$date->timestamp] += 1;
                    if ($winning->type == $subitemType) {
                        $subitems[$subitemType][$winning->name][$date->timestamp] += 1;
                        $results[$winning->name] += 1;
                    }
                    $results[$winning->type] += 1;
                    $results['total'] += 1;
                }
            }
        };

        foreach ($grouped as $key => $group) {
            foreach ($group as $ts => $value) {
                $data[$key][] = '['.($ts*1000).','.$value.']';
            }
        }

        $this->vars['types']    = $types;
        $this->vars['items']    = $grouped;
        $this->vars['data']     = $data;
        $this->vars['subitems'] = $subitems;
        $this->vars['results']  = $results;
        // dd($grouped, $data, $subitems, $results, $types);
        return $this->makePartial('widget');
    }
}
?>
