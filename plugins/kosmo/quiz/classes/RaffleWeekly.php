<?php namespace Kosmo\Quiz\Classes;

use DB;
use Mail;
use Session;
use Carbon\Carbon;
use Kosmo\Quiz\Models\Prize;
use Kosmo\Profile\Models\User;

class RaffleWeekly extends RaffleBase
{
    protected static $type;
    protected static $codesLimit;
    protected static $friendsLimit;

    protected function init() {
        self::$type = Prize::TYPE_WEEKLY;
    }

    public function beforeRaffle($user, $data = null) {
        if ($user->checkWinningsByType(self::$type)) {
            return;
        }
        return true;
    }

    public function raffle($user, $prizes, $data = null, $time = null) {
        if (!isset($data['prize'])) {
            return;
        }
        if (!$prize = $data['prize']) {
            return;
        }
        if (!$prize->available > 0) {
            return;
        }
        return $data['prize'];
    }

    public function play($user, $data = null, $time = null)
    {
        if (!$user) {
            return;
        }
        if (!$before = $this->beforeRaffle($user, $data)) {
            return;
        }
        if (!$prize = $this->raffle($user, [], $data, $time)) {
            return;
        }
        if (!$after = $this->afterRaffle($user, $prize, $data)) {
            return;
        }
        return $this->register($user, $prize, $data, $time);
    }
}
