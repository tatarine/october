<?php namespace Kosmo\Quiz\Classes;


class RaffleAchieved extends RaffleBase
{
    public function beforeRaffle($user, $data = null) {
        if (!$prize = $data['prize']) {
            return;
        }
        if ($user->checkWinningsLimit()) {
            return;
        }
        return true;
    }

    public function prizes($user, $data = null) {
        return collect([$data['prize']]);
    }

    public function raffle($user, $prizes, $data = null, $time = null) {
        return $prizes->first();
    }
}
