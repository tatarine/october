<?php namespace Kosmo\Quiz\Classes;

use DB;
use Mail;
use Session;
use Carbon\Carbon;
use Kosmo\Quiz\Models\Prize;
use Kosmo\Profile\Models\User;

class RaffleAssured extends RaffleBase
{
    protected static $type;
    protected static $codesLimit;
    protected static $friendsLimit;

    protected function init() {
        self::$type = Prize::TYPE_ASSURED;
        self::$codesLimit = User::LIMIT_CODES;
        self::$friendsLimit = User::LIMIT_FRIENDS;
    }

    public function beforeRaffle($user, $data = null) {
        if ($user->checkWinningsByType(self::$type)) {
            return;
        }
        if (!$friends = $user->getGoodFriends()) {
            return;
        }
        if ($friends->isEmpty()) {
            return;
        }
        if ($friends->count() < self::$friendsLimit) {
            return;
        }
        return true;
    }

    public function prizes($user, $data = null) {
        $prizes = Prize::available()->whereType(self::$type)->get();
        return $prizes;
    }

    public function raffle($user, $prizes, $data = null, $time = null) {
        $key = $prizes->keys()[rand(0, $prizes->count() - 1)];
        return $prizes[$key];
    }
}
