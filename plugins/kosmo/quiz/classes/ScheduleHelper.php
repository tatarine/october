<?php namespace Kosmo\Quiz\Classes;

use Carbon\Carbon;
use Kosmo\Quiz\Models\Schedule;
use Kosmo\Quiz\Models\Settings;

class ScheduleHelper
{
    use \October\Rain\Support\Traits\Singleton;

    protected function init() {}

    public function getMainSchedule()
    {
        if (!$slug = Settings::get('schedule.main')) {
            return;
        }
        if (!$schedule = Schedule::enabled()->bySlug($slug)->first()) {
            return;
        }
        return $schedule;
    }

    public function getSchedules()
    {
        $result = ['now' => [], 'past' => [], 'waiting' => []];
        if ($main = $this->getMainSchedule()) {
            $result['main'] = $main;
        }
        $schedules = Schedule::enabled()->get();
        foreach ($schedules as $schedule) {
            // if ($schedule->id == $main->id) {
            //     continue;
            // }
            if ($schedule->checkWaiting()) {
                $schedule->setWaiting();
            }
            if ($schedule->checkNow()) {
                $schedule->setNow();
            }
            if ($schedule->checkPast()) {
                $schedule->setPast();
            }
            if ($schedule->isWaiting()) {
                $result['waiting'][] = $schedule;
            }
            if ($schedule->isNow()) {
                $result['now'][] = $schedule;
            }
            if ($schedule->isPast()) {
                $result['past'][] = $schedule;
            }
        }
        return $result;
    }
}
