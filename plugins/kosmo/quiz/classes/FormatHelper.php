<?php namespace Kosmo\Quiz\Classes;

class FormatHelper
{
    public static function numberWithZeros($number, $zeros = 4)
    {
        return sprintf('%0'.$zeros.'d', $number);
    }

}
