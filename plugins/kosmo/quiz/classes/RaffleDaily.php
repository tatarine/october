<?php namespace Kosmo\Quiz\Classes;

use DB;
use Log;
use Mail;
use Cookie;
use Session;
use Carbon\Carbon;
use Kosmo\Quiz\Models\Settings;
use Kosmo\Quiz\Models\Prize;
use Illuminate\Support\Collection;

class RaffleDaily extends RaffleBase
{
    protected static $type;
    protected static $anyway;

    protected function init() {
        self::$type = Prize::TYPE_DAILY;
        self::$anyway = false;
    }

    public function beforeRaffle($user, $data = null) {
        if ($user->checkWinningsByType(self::$type)) {
            return;
        }
        if ($user->checkWinningByBlockList())
        {
            return;
        }
        // if ($already = Cookie::get('already')) {
        //     return;
        // }
        if ($user->checkWinningsByIp(self::$type)) {
            return;
        }
        return true;
    }

    protected function getAvailableNow($scheduleAmount, $hour = false)
    {
        $barriers = [0, 8, 16, 20, 24];
        $quotas = [0.2, 0.6, 0.2, 1];
        $currentHour = $hour === false ? Carbon::now()->hour : $hour;
        $availableNow = 0;
        for ($i = 0; $i < count($quotas); $i++) {
            if ($currentHour >= $barriers[$i]) {
                $quota = $quotas[$i]*$scheduleAmount;
                $availableNow += ($quota*(min(1, ($currentHour-$barriers[$i])/($barriers[$i+1]-$barriers[$i]))));
            }
        }
        return ceil($availableNow);
    }

    protected function getRandomMinutes()
    {
        $currentMinute = Carbon::now()->minute;
        $interval = Settings::getPeriod();
        $period = [];
        $period[0] = rand(1, 59);
        $period[1] = ($period[0] + $interval >= 59) ? 59 : $period[0] + $interval;
        sort($period);
        $rnd = ($currentMinute >= $period[0]) && ($currentMinute <= $period[1]);
        return $rnd;
    }

    public function prizes($user, $data = null) {
        $prizes = Prize::whereType(self::$type)->get();

        $filtered = $prizes->filter(function ($prize) {
            if (!$prize->available){
                return;
            }
            $scheduleAmount = $prize->getScheduleAmount();
            if ($scheduleAmount <= 0){
                return;
            }
            $raffledAmount = $prize->getRaffledAmount();
            if ($raffledAmount >= $scheduleAmount) {
                return;
            }
            return true;
        });

        $totalScheduleAmount = 0;
        $totalRaffledAmount = 0;
        foreach ($prizes as $prize) {
            $totalScheduleAmount += $prize->getScheduleAmount();
            $totalRaffledAmount += $prize->getRaffledAmount();
        }
        $totalAvailableNow = $this->getAvailableNow($totalScheduleAmount);
        $diff = $totalAvailableNow - $totalRaffledAmount;
        Log::info('Total: '.$totalAvailableNow.'-'.$totalRaffledAmount.'='.$diff);
        if ($diff <= 0) {
            return new Collection;
        } elseif ($diff > 1 && $this->getRandomMinutes()) {
            self::$anyway = true;
        }
        return $filtered;
    }

    public function raffle($user, $prizes, $data = null, $time = null) {
        $raffle = false;
        if (env('WINNER') || self::$anyway) {
            $raffle = true;
        } else {
            $chances = [];
            $dayStart   = Carbon::now()->setTime(8, 0, 0);
            $dayEnd     = Carbon::now()->setTime(23, 0, 0);
            $happyStart = Carbon::now()->setTime(18, 0, 0);
            $happyEnd   = Carbon::now()->setTime(20, 0, 0);
            $now        = ($time) ? $time : Carbon::now();
            if ($now->between($dayStart, $dayEnd)) {
                if ($now->between($happyStart, $happyEnd)) {
                    if ($chance = Settings::getChance('happy')) {
                        $chances[] = $chance;
                    }
                } else {
                    if ($chance = Settings::getChance('day')) {
                        $chances[] = $chance;
                    }
                }
            } else {
                if ($chance = Settings::getChance('night')) {
                    $chances[] = $chance;
                }
            }
            if ($chance = Settings::getChance(self::$type)) {
                $chances[] = $chance;
            }
            foreach ($chances as $key => $value) {
                if ($key == 0) {
                    $chance = $value;
                } else {
                    $chance = $chance * $value;
                }
            }

            if ($chance >= (rand(1, 100)/100)) {
                $raffle = true;
            }
        }
        if ($raffle) {
            // Cookie::queue('already', time(), 525600);
            $key = $prizes->keys()[rand(0, $prizes->count() - 1)];
            return $prizes[$key];
        }
        return;
    }
}
