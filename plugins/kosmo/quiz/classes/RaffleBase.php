<?php namespace Kosmo\Quiz\Classes;

use Carbon\Carbon;
use Kosmo\Quiz\Models\Winning;

class RaffleBase
{
    use \October\Rain\Support\Traits\Singleton;

    // protected static $now;
    protected static $result;

    protected function init() {}

    public function beforeRaffle($user, $data = null) {
        return true;
    }

    public function prizes($user, $data = null) {}

    public function raffle($user, $prizes, $data = null, $time = null) {}

    public function afterRaffle($user, $prize, $data = null) {
        return true;
    }

    public function play($user, $data = null, $time = null)
    {
        if (!$user) {
            return;
        }
        if (!$before = $this->beforeRaffle($user, $data)) {
            return;
        }
        $prizes = $this->prizes($user, $data);
        if ($prizes->isEmpty()) {
            return;
        }
        if (!$prize = $this->raffle($user, $prizes, $data, $time)) {
            return;
        }

        if (!$after = $this->afterRaffle($user, $prize, $data)) {
            return;
        }
        return $this->register($user, $prize, $data, $time);
    }

    public function register($user, $prize, $data = null, $time = null)
    {
        $winning = new Winning;

        $winning->user_id = $user->id;
        $winning->user_email = $user->email;

        $winning->prize_id = $prize->id;
        $winning->prize_name = $prize->name;

        if (isset($data['ticket']) && $ticket = $data['ticket']) {
            $winning->ticket_id = $ticket->id;
            $winning->ticket_number = $ticket->number;
        }
        if (isset($data['tickets_count'])) {
            $winning->tickets_count = $data['tickets_count'];
        }
        if (isset($data['vote']) && $vote = $data['vote']) {
            $winning->vote_id = $vote->id;
        }
        if (isset($data['friend']) && $friend = $data['friend']) {
            $winning->friend_id = $friend->id;
        }
        if (isset($data['rgl_id'])) {
            $winning->rgl_id = $data['rgl_id'];
        }
        if ($time) {
            $winning->raffled_at = $time;
        }
        $winning->save();

        $prize->unavailable = $prize->unavailable + 1;
        $prize->save();
        return $winning;
    }
}
