<?php namespace Kosmo\Quiz\Models;

use Model;
use Kosmo\Quiz\Models\Schedule;

/**
 * settings Model
 */
class Settings extends Model
{

    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'kosmo_quiz_settings';

    public $settingsFields = 'fields.yaml';

    public static function getChance($chance)
    {
        $value = self::get($chance);
        if ($value < 0 || $value > 100) {
            return 0;
        }
        return $value/100;
    }

    public static function getPeriod()
    {
        $value = self::get('period');
        if ($value < 1) {
            return 1;
        }
        if ($value > 59) {
            return 59;
        }
        return $value;
    }

    public function listSchedules()
    {
        $list = ['' => 'Выберите период'];
        return $list + Schedule::enabled()->lists('description', 'slug');
    }
}
