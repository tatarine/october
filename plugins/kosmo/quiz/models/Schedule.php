<?php namespace Kosmo\Quiz\Models;

use Model;
use Carbon\Carbon;

/**
 * Schedule Model
 */
class Schedule extends Model
{
    const STATUS_PAST    = 0;
    const STATUS_NOW     = 50;
    const STATUS_WAITING = 100;

    /**
     * Implements.
     */
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Validation rules.
     */
    public $rules = [
        'slug'  => 'required|unique:kosmo_quiz_schedules',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kosmo_quiz_schedules';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Date fields
     */
    protected $dates = ['started_at', 'ended_at'];

    /**
     * Events
     */
    public function beforeCreate()
    {
        $this->status = self::STATUS_WAITING;
        $this->is_enabled = 1;
    }

    /**
     * Scopes
     */
    public function scopeEnabled($query)
    {
        return $query->where('is_enabled', 1);
    }

    public function scopeBySlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

    public function scopeByStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function scopePast($query)
    {
        return $query->where('status', self::STATUS_PAST);
    }

    public function scopeNow($query)
    {
        return $query->where('status', self::STATUS_NOW);
    }

    public function scopeWait($query)
    {
        return $query->where('status', self::STATUS_WAITING);
    }

    /**
     * Methods
     */
    public function checkNow($time = null)
    {
        if (!$time || !($time instanceof Carbon)) {
            $time = Carbon::now();
        }
        return $time->between($this->started_at, $this->ended_at);
    }

    public function checkPast($time = null)
    {
        if (!$time || !($time instanceof Carbon)) {
            $time = Carbon::now();
        }
        return $time->gt($this->ended_at);
    }

    public function checkWaiting($time = null)
    {
        if (!$time || !($time instanceof Carbon)) {
            $time = Carbon::now();
        }
        return $time->lt($this->started_at);
    }

    /**
     * Statuses
     */
    public function listStatuses()
    {
        return [
            self::STATUS_WAITING  => 'Waiting',
            self::STATUS_NOW      => 'Now',
            self::STATUS_PAST     => 'Past',
        ];
    }

    public function getStatusName()
    {
        $list = $this->listStatuses();
        return $list[$this->status];
    }

    public function checkStatus($status)
    {
        return $this->status == $status;
    }

    public function isWaiting()
    {
        return $this->checkStatus(self::STATUS_WAITING);
    }

    public function isNow()
    {
        return $this->checkStatus(self::STATUS_NOW);
    }

    public function isPast()
    {
        return $this->checkStatus(self::STATUS_PAST);
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this->save();
    }

    public function setWaiting()
    {
        return $this->setStatus(self::STATUS_WAITING);
    }

    public function setNow()
    {
        return $this->setStatus(self::STATUS_NOW);
    }

    public function setPast()
    {
        return $this->setStatus(self::STATUS_PAST);
    }
}
