<?php namespace Kosmo\Quiz\Models;

use Model;
use Kosmo\Profile\Classes\FormatHelper;

/**
 * Winner Model
 */
class Winner extends Model
{
    const TYPE_PERMANENT  = 'permanent';
    const TYPE_PHOTO      = 'photo';
    const TYPE_DAILY      = 'daily';
    const TYPE_WEEKLY     = 'weekly';

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kosmo_quiz_winners';

    /**
     * @var array Translatable fields
     */
    public $translatable = ['firstname', 'lastname', 'middlename', 'city_name', 'prize_name', 'description'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Dates fields
     */
    protected $dates = ['raffled_at'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'city' => ['Kosmo\Profile\Models\Location', 'key' => 'city_id'],
        'prize' => ['Kosmo\Quiz\Models\Prize'],
    ];

    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'photo' => 'System\Models\File',
    ];
    public $attachMany = [];

    /**
    * @var function Scopes
    */
    public function scopeEnabled($query)
    {
        return $query->where('is_enabled', 1);
    }

    public function scopeByType($query, $type)
    {
        return $query->where('type', $type);
    }

    public function scopePermanent($query)
    {
        return $query->where('type', self::TYPE_PERMANENT);
    }

    public function scopePhoto($query)
    {
        return $query->where('type', self::TYPE_PHOTO);
    }

    /**
     * Options
     */
    public function getTypeOptions()
    {
        return [
            self::TYPE_DAILY    => 'Каждый день',
            self::TYPE_WEEKLY   => 'Каждую неделю',
        ];
    }

    public function getTypeOption($value = null)
    {
        if (!$value){
            $value = $this->type;
        }
        $options = $this->getTypeOptions();
        if (!isset($options[$value])) {
            return 'Не указан';
        }
        return $options[$value];
    }

    /**
     * Mutators
     */
    public function getThumb($width, $height, $mode)
    {
        if (!$this->photo) {
            return;
        }
        return $this->photo->getThumb($width, $height, ['mode' => $mode]);
    }

    public function getPhoneWithPreffix($withPlus = true)
    {
        return FormatHelper::phoneWithPrefix($this->phone, $withPlus);
    }

    public function getPhoneFormatted($withMask = true)
    {
        return FormatHelper::phoneWithMask($this->phone, $withMask);
    }
}
