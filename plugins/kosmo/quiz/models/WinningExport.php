<?php namespace Kosmo\Quiz\Models;

use DB;
use Carbon\Carbon;
use Kosmo\Export\Classes\ExportModel;
use Kosmo\Quiz\Models\Winning;

class WinningExport extends ExportModel
{
    protected $fillable = ['begin_at', 'end_at'];

    public function exportData($columns, $sessionKey = null)
    {
        $result = [];
        $query = DB::table('kosmo_quiz_winnings as w')
                ->leftJoin('kosmo_quiz_prizes as p', 'w.prize_id', '=', 'p.id')
                ->leftJoin('kosmo_profile_users as u', 'w.user_id', '=', 'u.id')
                ->leftJoin('kosmo_profile_locations as l', 'u.city_id', '=', 'l.id')
                ->select('w.user_id',
                        'u.name as user_name',
                        'u.email as user_email',
                        'u.phone as user_phone',
                        'l.name as user_city',
                        'w.prize_id',
                        'p.name as prize_name',
                        'w.id',
                        'w.status',
                        'w.raffled_at',
                        'w.filled_at',
                        'w.sent_at',
                        'w.delivered_at');
        if ($this->begin_at) {
            $begin_at = Carbon::createFromFormat('Y-m-d H:i:s', $this->begin_at)->startOfDay()->toDateTimeString();
            $query = $query->where('w.raffled_at', '>=', $begin_at);
        }
        if ($this->end_at) {
            $end_at = Carbon::createFromFormat('Y-m-d H:i:s', $this->end_at)->endOfDay()->toDateTimeString();
            $query = $query->where('w.raffled_at', '<=', $end_at);
        }
        $tickets = $query->get();

        $model = new Winning;
        foreach ($tickets as $i => $ticket) {
            $result[$i] = [];
            foreach ($ticket as $key => $value) {
                if (in_array($key, $columns)) {
                    if ($key == 'status') {
                        $result[$i][$key] = $model->getStatusOption($value);
                    } else {
                        $result[$i][$key] = $value;
                    }
                }
            }
        }
        return $result;
    }
}
?>
