<?php namespace Kosmo\Quiz\Models;

use Mail;
use Model;
use Carbon\Carbon;

/**
 * Winning Model
 */
class Winning extends Model
{
    const STATUS_RAFFLED = 'raffled';
    const STATUS_FILLED = 'filled';
    const STATUS_SENT = 'sent';
    const STATUS_DELIVERED = 'delivered';

    const EXPIRE = 60*60*24*10;  //10 days
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kosmo_quiz_winnings';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $dates = [
        'raffled_at',
        'filled_at',
        'sent_at',
        'delivered_at',
    ];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'user' => ['Kosmo\Profile\Models\User'],
        'prize' => ['Kosmo\Quiz\Models\Prize'],
        'ticket' => ['Kosmo\Profile\Models\Ticket'],
        // 'vote' => ['Kosmo\Quiz\Models\LevelPuzzleVote', 'key' => 'vote_id'],
        // 'friend' => ['Kosmo\Profile\Models\User', 'key' => 'friend_id'],
    ];

    public $hasOne = [
        'delivery' => ['Kosmo\Delivery\Models\Delivery'],
        // 'raffle' => ['Kosmo\Profile\Models\Raffle', 'key' => 'winning_id']
    ];

    public function getStatusOptions()
    {
        return [
            self::STATUS_RAFFLED => 'Выигран',
            self::STATUS_FILLED => 'Заполнен',
            self::STATUS_SENT => 'Отправлен',
            self::STATUS_DELIVERED => 'Доставлен'
        ];
    }

    public function getStatusOption($value = null)
    {
        if (!$value)
            $value = $this->status;

        $options = $this->getStatusOptions();

        if (!isset($options[$value]))
            return 'Неверный статус';

        return $options[$value];
    }

    /**
     * Events
     */
    public function beforeCreate()
    {
        if (!$this->raffled_at) {
            $this->raffled_at = Carbon::now();
        }
    }

    public function beforeUpdate()
    {
        if ($this->status == self::STATUS_SENT) {
            $this->sent_at = Carbon::now();
        }
        if ($this->status == self::STATUS_DELIVERED) {
            $this->delivered_at = Carbon::now();
        }
    }
    /**
     * Scopes
     */
    public function scopeRaffledToday($query)
    {
        $today = Carbon::today();
        $tommorow = Carbon::tomorrow();
        return $query->where('raffled_at', '>=', $today)
                     ->where('raffled_at', '<', $tommorow);
    }

    public function scopeHasPrize($query, $type)
    {
        return $query->whereHas('prize', function($query) use($type) {
            return $query->where('type', $type);
        });
    }

    public static function raffledTodayByType($type = null)
    {
        $winnings = Winning::raffledToday()->get();
        if ($type) {
            $winnings = $winnings->filter(function ($item) use ($type) {
                return $item->prize->type == $type;
            });
        }
        return $winnings;
    }
    /**
     * Attributes
     */
    public function isRaffled()
    {
        return $this->status == self::STATUS_RAFFLED;
    }

    public function isFilled()
    {
        return $this->status == self::STATUS_FILLED;
    }

}
