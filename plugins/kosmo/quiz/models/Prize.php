<?php namespace Kosmo\Quiz\Models;

use Model;
use Carbon\Carbon;
use Kosmo\Quiz\Classes\FormatHelper;
/**
 * Prize Model
 */
class Prize extends Model
{
    const TYPE_GRAND    = 'grand';
    const TYPE_DAILY    = 'daily';
    const TYPE_WEEKLY   = 'weekly';
    const TYPE_MONTHLY  = 'monthly';
    const TYPE_ASSURED  = 'assured';
    const TYPE_ACHIEVED = 'achieved';

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kosmo_quiz_prizes';

    /**
     * @var array Guarded fields
     */
    public $translatable = ['name', 'comment', 'description'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasMany = [
        'winnings'=> ['Kosmo\Quiz\Models\Winning']
    ];

    /**
     * Options
     */
    public function getTypeOptions()
    {
        return [
            self::TYPE_ASSURED  => 'Гарантированный приз',
            self::TYPE_ACHIEVED => 'Достигаемый приз',
            self::TYPE_DAILY    => 'Ежедневный приз',
            self::TYPE_WEEKLY   => 'Еженедельный приз',
            self::TYPE_MONTHLY  => 'Ежемесячный приз',
            self::TYPE_GRAND    => 'Главный приз',
        ];
    }

    public function getTypeOption($value = null)
    {
        if (!$value){
            $value = $this->type;
        }
        $options = $this->getTypeOptions();
        if (!isset($options[$value])) {
            return 'Не указан';
        }
        return $options[$value];
    }

    public function getAvailableFormatted($zeros = 4)
    {
        return FormatHelper::numberWithZeros($this->available, $zeros);
    }
    /**
     * Events
     */
    public function beforeSave()
    {
        if ($this->isDirty('amount') || $this->isDirty('unavailable')) {
            $this->available = $this->amount - $this->unavailable;
        }
    }

    /**
     * Scopes
     */
    public function scopeAssured($query)
    {
        return $query->whereType(Prize::TYPE_ASSURED);
    }

    public function scopeAchieved($query)
    {
        return $query->whereType(Prize::TYPE_ACHIEVED);
    }

    public function scopeDaily($query)
    {
        return $query->whereType(Prize::TYPE_DAILY);
    }

    public function scopeWeekly($query)
    {
        return $query->whereType(Prize::TYPE_WEEKLY);
    }

    public function scopeAvailable($query)
    {
        return $query->where('available', '>', 0);
    }

    public function scopeByRglId($query, $value)
    {
        return $query->where('rgl_id', $value);
    }
    /**
     * Methods
     */
    public function isAvailable()
    {
        return ($this->available > 0);
    }

    public function checkScores($scores)
    {
        return ($scores >= $this->scores);
    }

    public function getImagePath($folder)
    {
        return $folder.'/'.$this->cdn_key;
    }
}
