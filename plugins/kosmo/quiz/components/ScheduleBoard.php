<?php namespace Kosmo\Quiz\Components;

use Request;
use Redirect;
use Kosmo\Quiz\Classes\ScheduleHelper;
use Cms\Classes\ComponentBase;

class ScheduleBoard extends ComponentBase
{
    public $paths = [];

    public function componentDetails()
    {
        return [
            'name'        => 'ScheduleBoard Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function init()
    {
        $this->paths[] = 'registration';
    }

    public function onRun()
    {
        if ($main = ScheduleHelper::instance()->getMainSchedule()) {
            if ($main->isPast()) {
                if ($this->checkRedirect()) {
                    return Redirect::to('/');
                }
            }
        }
        if ($result = ScheduleHelper::instance()->getSchedules()) {
            $this->page['schedules'] = $result;
        }
    }

    public function checkRedirect()
    {
        return in_array(Request::path(), $this->paths);
    }
}
