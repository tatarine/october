<?php namespace Kosmo\Quiz\Components;

use DB;
use Carbon\Carbon;
use Kosmo\Quiz\Models\Prize;
use Kosmo\Quiz\Models\Winner;
use Kosmo\Quiz\Models\Winning;
use Kosmo\Profile\Models\User;
use Cms\Classes\ComponentBase;

class WinningTable extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'WinningTable Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'type' => [
                'title'       => 'Winners Type',
                'description' => 'Winners type property',
                'type'        => 'string',
                'default'     => 'weekly'
            ],
            'size' => [
                'title'       => 'Page Size',
                'description' => 'Page size property',
                'type'        => 'string',
                'default'     => '10'
            ],
            'page' => [
                'title'       => 'Page Number',
                'description' => 'Page number property',
                'type'        => 'string',
                'default'     => '1'
            ],
        ];
    }

    public function init()
    {

    }

    public function onRun()
    {
        $dailyWinners = $this->getWinnersPerPage('daily');
        $this->page['dailyWinners'] = $dailyWinners;
        $this->page['dailyPagination'] = $dailyWinners;

        $weeklyWinners = $this->getWinnersPerPage('weekly');
        $this->page['weeklyWinners'] = $weeklyWinners;
        $this->page['weeklyPagination'] = $weeklyWinners;
    }

    public function onPaginateWinners()
    {
        $post = post();
        if (!$type = post('type')) {
            return;
        }
        if (!$page = post('page')) {
            return;
        }
        $winners = $this->getWinnersPerPage($type, $page);
        $this->page['type'] = $type;
        $this->page['winners'] = $winners;
        $this->page['pagination'] = $winners;

        $result = [];
        $result['#'.$post['paginateTarget']] = $this->renderPartial($post['paginatePartial']);
        $result['#'.$post['paginateId']] = $this->renderPartial($post['paginateTemplate']);
        return $result;
    }

    public function onSearchWinners()
    {
        if (!$type = post('type')) {
            return;
        }
        if (!$search = post('search')) {
            return;
        }
        if (!is_string($search)) {
            return;
        }
        $this->page['winners'] = $this->getWinnersBySearch($type, $search);
        $result = [];
        $result['#table-'.$type] = $this->renderPartial('tables/'.$type);
        return $result;
    }

    protected function getWinnersBySearch($type = null, $search = '')
    {
        if (!$type) {
            $type = $this->property('type');
        }
        $search = str_replace(' ', '', $search);
        $users = Winner::enabled()->byType($type)->where('phone', 'like', '%'.$search.'%')->get()->lists('id');
        if (empty($users)) {
            return;
        }
        $winners = $this->getWinners($type, $users)->get();
        return $winners;
    }

    protected function getWinnersPerPage($type = null, $page = null, $size = null)
    {
        if (!$page) {
            $page = $this->property('page');
        }
        if (!$size) {
            $size = $this->property('size');
        }
        $winners = $this->getWinners($type);
        return $winners->paginate($size, $page);
    }

    protected function getWinners($type = null, $users = [])
    {
        if (!$type) {
            $type = $this->property('type');
        }
        $query = Winner::enabled()->byType($type)->orderBy('raffled_at', 'desc');
        if (!empty($users)) {
            $query = $query->whereIn('id', $users);
        }
        // if ($type == 'permanent') {
        //     $query = Winner::enabled()->permanent()->orderBy('raffled_at', 'desc');
        //     if (!empty($users)) {
        //         $query = $query->whereIn('id', $users);
        //     }
        // } elseif ($type == 'photo') {
        //     $query = Winner::enabled()->photo()->orderBy('raffled_at', 'desc');
        //     if (!empty($users)) {
        //         $query = $query->whereIn('id', $users);
        //     }
        // } else {
        //     $query = Winning::hasPrize($type)->orderBy('raffled_at', 'desc');
        //     if (!empty($users)) {
        //         $query = $query->whereIn('user_id', $users);
        //     }
        //     $query = $query->remember(10);
        // }
        return $query;
        // return $query->remember(10);
    }
}
