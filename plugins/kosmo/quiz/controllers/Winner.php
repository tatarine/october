<?php namespace Kosmo\Quiz\Controllers;

use Redirect;
use BackendMenu;
use Backend\Classes\Controller;
use Kosmo\Quiz\Models\Winner as WinnerModel;

/**
 * Winner Back-end Controller
 */
class Winner extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kosmo.Quiz', 'quiz', 'winner');
    }

    public function onDetachCity()
    {
        $id = post('model');
        if (!$winner = WinnerModel::find($id)) {
            return Redirect::refresh();
        }
        $winner->city_id = null;
        $winner->save();
        return Redirect::refresh();
    }

    public function onDetachPrize()
    {
        $id = post('model');
        if (!$winner = WinnerModel::find($id)) {
            return Redirect::refresh();
        }
        $winner->prize_id = null;
        $winner->save();
        return Redirect::refresh();
    }
}
