<?php namespace Kosmo\Quiz\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Schedule Back-end Controller
 */
class Schedule extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kosmo.Quiz', 'quiz', 'schedule');
    }
}