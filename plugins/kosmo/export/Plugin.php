<?php namespace Kosmo\Export;

use Backend;
use System\Classes\PluginBase;

/**
 * export Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Export',
            'description' => 'Export plugin',
            'author'      => 'Kosmoport',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Kosmo\Export\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'kosmo.export.some_permission' => [
                'tab' => 'export',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'export' => [
                'label'       => 'export',
                'url'         => Backend::url('kosmo/export/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['kosmo.export.*'],
                'order'       => 500,
            ],
        ];
    }

}
