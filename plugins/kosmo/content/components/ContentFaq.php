<?php namespace Kosmo\Content\Components;

use Kosmo\Content\Models\Faq;
use Cms\Classes\ComponentBase;

class ContentFaq extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'ContentFaq Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['faqs'] = Faq::active()->get();
    }
}
