<?php namespace Kosmo\Content;

use Backend;
use System\Classes\PluginBase;

/**
 * Content Plugin Information File
 */
class Plugin extends PluginBase
{

    public $require = ['RainLab.Translate'];
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Content',
            'description' => 'Content plugin',
            'author'      => 'Kosmoport',
            'icon'        => 'icon-folder-o'
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Kosmo\Content\Components\ContentFaq' => 'contentFaq',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'kosmo.content.access' => [
                'tab' => 'Content',
                'label' => 'Content common access'
            ],
            'kosmo.content.faq' => [
                'tab' => 'Content',
                'label' => 'Faq access'
            ]
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'content' => [
                'label'       => 'Контент',
                'url'         => Backend::url('kosmo/content/faq'),
                'icon'        => 'icon-folder-o',
                'permissions' => ['kosmo.content.access'],
                'order'       => 6,
                'sideMenu'    => [
                    'faq' => [
                        'label'     => 'FAQ',
                        'icon'      => 'icon-question-circle',
                        'url'       => Backend::url('kosmo/content/faq'),
                        'permissions' => ['kosmo.content.faq'],
                    ],
                ]
            ]
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                // A local method, i.e $this->makeTextAllCaps()
                'toCdn' => [$this, 'reroute2cdn']
            ]
        ];
    }

    public function reroute2cdn($value='')
    {
        return str_replace('http://'.$_SERVER['HTTP_HOST'], config('cms.cdnUrl'), $value);
    }

}
