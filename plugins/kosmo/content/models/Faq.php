<?php namespace Kosmo\Content\Models;

use Model;

/**
 * faq Model
 */
class Faq extends Model
{
    const STATUS_INACTIVE   = 'inactive';
    const STATUS_ACTIVE     = 'active';

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kosmo_content_faqs';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    public $translatable = ['question', 'answer'];

    public function getStatusOptions()
    {
        return [
            self::STATUS_INACTIVE => 'Неактивный',
            self::STATUS_ACTIVE    => 'Активный',
        ];
    }

    public function getStatusOption($status = null)
    {
        if (!$status) {
            $status = $this->status;
        }
        $options = $this->getStatusOptions();
        if (!isset($options[$status])) {
            return;
        }

        return $options[$status];
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

}
