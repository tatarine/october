<?php namespace Kosmo\Feedback\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Feedback Back-end Controller
 */
class Feedback extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kosmo.Feedback', 'feedback', 'feedback');
    }
}
