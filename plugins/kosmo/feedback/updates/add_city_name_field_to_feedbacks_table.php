<?php namespace Kosmo\Port\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddCityNameFieldToFeedbacksTable extends Migration
{

    public function up()
    {
        Schema::table('kosmo_feedback_feedbacks', function($table)
        {
            $table->string('city_name')->nullable()->after('city_id');
        });
    }

    public function down()
    {
        Schema::table('kosmo_feedback_feedbacks', function ($table) {
            $table->dropColumn('city_name');
        });
    }
}
