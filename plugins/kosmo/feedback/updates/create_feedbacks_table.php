<?php namespace Kosmo\Port\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFeedbacksTable extends Migration
{

    public function up()
    {
        Schema::create('kosmo_feedback_feedbacks', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->integer('city_id')->nullable()->unsigned();
            $table->text('message')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kosmo_feedback_feedbacks');
    }
}
