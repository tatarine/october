<?php namespace Kosmo\Feedback\Updates;

use DB;
use October\Rain\Database\Updates\Seeder;

class SeedFeedbackMailTemplatesTable extends Seeder
{
    public function run()
    {
        DB::table('system_mail_templates')->where('code', 'like', 'kosmo.feedback::mail.support')
            ->delete();

        DB::table('system_mail_templates')->insert([
            'code' => 'kosmo.feedback.mail.support',
            'subject' => 'Feedback',
            'description' => 'Feedback template',
            'content_html' => '<p>​Сообщение от {{name}}, {{email}}, {{phone}}, {{city}}.</p><p>{{text}}</p>',
            'content_text' => 'Сообщение от {{name}}, {{email}}, {{phone}}, {{city}}. {{text}}',
            'layout_id' => 1,
            'is_custom' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
