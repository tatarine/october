<?php namespace Kosmo\Feedback\Models;

use Model;
use Kosmo\Profile\Classes\FormatHelper;

/**
 * Feedback Model
 */
class Feedback extends Model
{
    public $table = 'kosmo_feedback_feedbacks';

    public $belongsTo = [
        'city' => ['Kosmo\Profile\Models\Location', 'key' => 'city_id'],
    ];

    public function getPhoneFormatted($withMask = true)
    {
        return FormatHelper::phoneWithMask($this->phone, $withMask);
    }
}
