<?php namespace Kosmo\Feedback;

use Backend;
use System\Classes\PluginBase;

/**
 * Feedback Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Feedback',
            'description' => 'Feedback plugin',
            'author'      => 'Kosmo',
            'icon'        => 'icon-fax'
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Kosmo\Feedback\Components\FeedbackForm' => 'feedbackForm',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'kosmo.feedback.access' => [
                'tab' => 'Feedback',
                'label' => 'Feedback common access'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'feedback' => [
                'label'       => 'Запросы',
                'url'         => Backend::url('kosmo/feedback/feedback'),
                'icon'        => 'icon-question',
                'permissions' => ['kosmo.feedback.feedback'],
                'order'       => 7,
            ],
        ];
    }

}
