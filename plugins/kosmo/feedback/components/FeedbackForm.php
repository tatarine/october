<?php namespace Kosmo\Feedback\Components;

use Mail;
use Cookie;
use Validator;
use ValidationException;
use Kosmo\Feedback\Models\Feedback;
use Cms\Classes\ComponentBase;

class FeedbackForm extends ComponentBase
{
    const RESTORED_PERIOD = 60;

    public function componentDetails()
    {
        return [
            'name'        => 'Feedback form component',
            'description' => 'Getting data from feedback form'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onFeedback()
    {
        if ($this->getRestored()) {
            return;
        }
        $data = post();
        $rules = [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email|between:6,64',
            'message' => 'required|between:8,256'
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }
        if (array_key_exists('city_id', $data)) {
            if (!is_numeric($data['city_id']) && is_string($data['city_id'])) {
                $data['city_name'] = $data['city_id'];
                unset($data['city_id']);
            }
        }

        $feedback = new Feedback;
        $feedback->name = $data['name'];
        $feedback->email = $data['email'];
        $feedback->phone = $data['phone'];
        $feedback->message = $data['message'];
        $feedback->city_id = isset($data['city_id']) ? $data['city_id'] : null;
        $feedback->city_name = isset($data['city_name']) ? $data['city_name'] : null;

        if ($feedback->save()) {
            $support = $this->support();
            $data['text'] = $data['message'];

            Mail::send('kosmo.feedback.mail.support', $data, function($message) use ($data, $support) {
                $message->replyTo($data['email'], $data['name']);
                $message->to($support, 'Support');
            });
        }

        $this->setRestored();
        return;
    }

    private function support()
    {
        if (!$support = env('APP_SUPPORT')) {
            $support = config('mail.support');
        }
        return $support;
    }

    private function getRestored()
    {
        if (env('FEEDBACK_TEST')) {
            return;
        }
        if ($restored = Cookie::get('restored')) {
            return true;
        }
    }

    private function setRestored()
    {
        if (env('FEEDBACK_TEST')) {
            return;
        }
        Cookie::queue('restored', time(), self::RESTORED_PERIOD);
    }
}
