<?php namespace Kosmo\Profile;

use App;
use Mail;
use Route;
use Session;
use Backend;
use Request;
use Redirect;
use BackendAuth;
use System\Classes\PluginBase;
use Kosmo\Profile\Classes\Geoip;
use Kosmo\Profile\Models\User;
use Kosmo\Profile\Classes\SocialManager;

/**
 * Profile Plugin Information File
 */
class Plugin extends PluginBase
{

    public $require = ['RainLab.Translate'];
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Profile',
            'description' => 'Extend RainLab.User plugin',
            'author'      => 'Kosmoport',
            'icon'        => 'icon-folder-o'
        ];
    }

    public function registerSettings()
    {
        return [
            'location' => [
                'label'       => 'Locations',
                'description' => 'Manage available user countries and states.',
                'category'    => 'Locations',
                'icon'        => 'icon-globe',
                'url'         => Backend::url('kosmo/profile/location'),
                'order'       => 500,
                'permissions' => ['kormo.profile.settings']
            ]
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Kosmo\Profile\Components\Account'          => 'account',
            'Kosmo\Profile\Components\ResetPassword'    => 'resetPassword',
            'Kosmo\Profile\Components\Session'          => 'session',
            'Kosmo\Profile\Components\ConfirmEmail'     => 'confirmEmail',
            'Kosmo\Profile\Components\LoadCode'         => 'loadCode',
            'Kosmo\Profile\Components\CabinetTable'     => 'cabinetTable',
        ];
    }

    public function registerReportWidgets()
    {
        return [];
        return [
            'Kosmo\Profile\ReportWidgets\Registration' => [
                'label'   => 'Users',
                'context' => 'dashboard'
            ]
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'kosmo.profile.access_users' => [
                'tab' => 'Profile',
                'label' => 'Access to profiles'
            ],
            'kosmo.profile.export_users' => [
                'tab' => 'Profile',
                'label' => 'Export profiles'
            ],
            'kosmo.profile.edit_users' => [
                'tab' => 'Profile',
                'label' => 'Edit profiles'
            ],
            'kosmo.profile.access_tickets' => [
                'tab' => 'Profile',
                'label' => 'Access to tickets'
            ],
            'kosmo.profile.export_tickets' => [
                'tab' => 'Profile',
                'label' => 'Export tickets'
            ],
            'kosmo.profile.access_raffles' => [
                'tab' => 'Raffle',
                'label' => 'Access to raffles'
            ],
            'kosmo.profile.remove_raffles' => [
                'tab' => 'Raffle',
                'label' => 'Remove raffles'
            ],
        ];
    }

    /**
     * Registers schedule.
     *
     */
    // public function registerSchedule($schedule)
    // {
    //     $schedule->command('profile:mandrill')->weekly()->mondays()->at('10:00');
    // }

    public function register()
    {
        // $this->registerConsoleCommand('profile.mandrill', 'Kosmo\Profile\Console\Mandrill');
        // $this->registerConsoleCommand('profile.closed', 'Kosmo\Profile\Console\Closed');
        // $this->registerConsoleCommand('profile.delivery', 'Kosmo\Profile\Console\Delivery');
        // $this->registerConsoleCommand('profile.massive', 'Kosmo\Profile\Console\Massive');
        // $this->registerConsoleCommand('profile.prepare', 'Kosmo\Profile\Console\Prepare');
        // $this->registerConsoleCommand('profile.miracle', 'Kosmo\Profile\Console\Miracle');
        // $this->registerConsoleCommand('profile.blizzard', 'Kosmo\Profile\Console\Blizzard');
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'profile' => [
                'label'       => 'Управление',
                'url'         => Backend::url('kosmo/profile/user'),
                'icon'        => 'icon-folder-o',
                'permissions' => ['kosmo.profile.*'],
                'order'       => 5,
                'sideMenu'    => [
                    'user' => [
                        'label'     => 'Пользователи',
                        'icon'      => 'icon-users',
                        'url'       => Backend::url('kosmo/profile/user'),
                        'permissions' => ['kosmo.profile.access_users'],
                    ],
                    'ticket' => [
                        'label'     => 'Коды и баллы',
                        'icon'      => 'icon-hashtag',
                        'url'       => Backend::url('kosmo/profile/ticket'),
                        'permissions' => ['kosmo.profile.access_tickets'],
                    ],
                    // 'raffle' => [
                    //     'label'     => 'Розыгрыш',
                    //     'icon'      => 'icon-flag-checkered',
                    //     'url'       => Backend::url('kosmo/profile/raffle'),
                    //     'permissions' => ['kosmo.profile.access_raffles'],
                    // ],
                ]
            ]
        ];
    }

    public function boot()
    {
        // $route = config('social.callback');
        // Route::match(['get', 'post'], $route.'/{social}', function ($social) {
        //     $manager = new SocialManager;
        //     $manager->init();
        //     return $manager->callback($social);
        // })->where('social', '[a-z]+');

        App::before(function ($request) {
            if (!Request::is('backend') && !Request::is('backend/*')) {
                Geoip::instance()->check();
            }
            // if (env('APP_ENV') == 'az') {
            //     if (!Session::get('rainlab.translate.locale')) {
            //         Session::put('rainlab.translate.locale', 'az');
            //     }
            // }
        });
    }
}
