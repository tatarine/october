<?php namespace Kosmo\Profile\Classes;

use Request;
use RainLab\User\Models\User as UserModel;

class VK extends SocialBase
{
    use \October\Rain\Support\Traits\Singleton;

    protected function init()
    {
        $this->code = 'vk';
        parent::init();
    }

    protected function _getLoginUrl()
    {
        $url = 'https://oauth.vk.com/authorize?client_id='.$this->appId.'&display=page&redirect_uri='.$this->redirectUrl.'&scope=email,friends&response_type=code&v=5.42';

        return $url;
    }

    protected function _getUserId($code = null)
    {
        if (!$code) {
            return;
        }

        $url = 'https://oauth.vk.com/access_token?client_id='.$this->appId.'&client_secret='.$this->appSecret.'&redirect_uri='.$this->redirectUrl.'&code='.$code;

        if ($response = $this->_getResponse($url)) {
            $this->accessToken = $response->access_token;
            $this->userId = $response->user_id;
            $this->userEmail = (isset($response->email)) ? $response->email : '';
            return $response->user_id;
        }
        return;
    }

    protected function _getUserProfile($userId = null, $accessToken = null)
    {
        if (!$userId) {
            if (!$userId = $this->userId) {
                return;
            }
        }
        if (!$accessToken) {
            if (!$accessToken = $this->accessToken) {
                return;
            }
        }
        $profile = [];
        $url = 'https://api.vk.com/method/users.get?user_id='.$userId.'&v=5.44';
        if ($response = $this->_getResponse($url)) {
            $profile['id'] = $userId;
            $profile['social_'.$this->code.'_id'] = $userId;
            $profile['name'] = $response->response[0]->first_name.' '.$response->response[0]->last_name;
            $profile['email'] = $this->userEmail;
            return $profile;
        }
        return;
    }

    protected function _getResponse($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);
        $response = curl_getinfo($ch);
        curl_close($ch);

        if ($response['http_code'] == 200) {
            return json_decode($content);
        }
        return;
    }
}
