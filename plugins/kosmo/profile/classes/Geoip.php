<?php namespace Kosmo\Profile\Classes;

use Request;
use Exception;
use PDOException;
use Kosmo\Profile\Models\Geoip as GeoipModel;
use Kosmo\Profile\Models\Location as LocationModel;

class Geoip
{
    use \October\Rain\Support\Traits\Singleton;

    protected $appId = '108031';

    protected $appSecret = 'LRVKA65lLZwD';
    // protected $appUrl = 'https://geoip.maxmind.com/geoip/v2.1/city/';
    protected $appUrl = 'http://geoip.kosmoport.kz:8080/city/';

    public function getBots()
    {
        return [
            'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)',
            'Mozilla/5.0 (compatible; YandexAccessibilityBot/3.0; +http://yandex.com/bots)',
            'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
            'Googlebot/2.1 (+http://www.google.com/bot.html)',
        ];
    }

    public function getExceptions()
    {
        return [
            '127.0.0.1'
        ];
    }

    public function getCountries()
    {
        $country = LocationModel::defaultCountry()->first();
        return [
            $country->slug
        ];
    }

    public function checkBots($agent = null)
    {
        if (!$agent) {
            $agent = Request::header('user_agent');
        }
        $bots = $this->getBots();
        return in_array($agent, $bots);
    }

    public function checkException($ipAddress = null)
    {
        if(($trustedProxies = config('http.trustedProxies')) && is_array($trustedProxies))
            Request::setTrustedProxies($trustedProxies);

        if (!$ipAddress) {
            $ipAddress = Request::ip();
        }
        $exceptions = $this->getExceptions();
        return in_array($ipAddress, $exceptions);
    }

    public function checkCountry($country = null)
    {
        if (!$country){
            return;
        }
        $countries = $this->getCountries();
        return in_array($country, $countries);
    }

    public function check($ipAddress = null, $agent = null)
    {
        if(($trustedProxies = config('http.trustedProxies')) && is_array($trustedProxies))
            Request::setTrustedProxies($trustedProxies);

        if (!$ipAddress) {
            $ipAddress = Request::ip();
        }

        if (!$agent) {
            $agent = Request::header('user_agent');
        }

        if ($this->checkBots($agent) || $this->checkException($ipAddress)) {
            return true;
        }

        if (!$model = GeoipModel::where('ip', $ipAddress)->first()) {
            $response = $this->request($ipAddress);
            if ($response) {
                try {
                    if (!$model = GeoipModel::where('ip', $ipAddress)->first()) {
                        $model = new GeoipModel;
                        $model->ip = $ipAddress;
                        $model->response = $response;
                        $model->save();
                    }
                } catch (PDOException $e) {
                    return;
                } catch (Exception $e) {
                    return;
                }
            } else {
                return true;
            }
        }

        if ($model->country) {
            return $this->checkCountry($model->country->slug);
        }

        return;
    }

    public function request($ipAddress = null)
    {
        if(($trustedProxies = config('http.trustedProxies')) && is_array($trustedProxies))
            Request::setTrustedProxies($trustedProxies);

        if (!$ipAddress) {
            $ipAddress = Request::ip();
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERPWD, $this->appId.':'.$this->appSecret);
        curl_setopt($ch, CURLOPT_URL, $this->appUrl . $ipAddress);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);
        $response = curl_getinfo($ch);
        curl_close($ch);

        if ($response['http_code'] == 200) {
            return json_decode($content);
        }

        return;
    }
}
