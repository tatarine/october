<?php namespace Kosmo\Profile\Classes;

use Log;
use Request;
use Kosmo\Shmobot\Models\Settings;
use RainLab\User\Models\User as UserModel;

class IG extends SocialBase
{
    use \October\Rain\Support\Traits\Singleton;

    protected function init()
    {
        $this->code = 'ig';
        parent::init();
    }

    protected function _getLoginUrl()
    {
        $url = 'https://api.instagram.com/oauth/authorize/?client_id='.$this->appId.'&redirect_uri='.$this->redirectUrl.'&response_type=code&scope=public_content';
        return $url;
    }

    protected function _getUserId($code = null)
    {
        if (!$code) {
            return;
        }
        Settings::set('code', $code);
        $params = array(
            'client_id'     => $this->appId,
            'client_secret' => $this->appSecret,
            'redirect_uri'  => $this->redirectUrl,
            'grant_type'    => 'authorization_code',
            'code'          => $code
        );
    	$url = 'https://api.instagram.com/oauth/access_token';
        if ($response = $this->_getPostResponse($url, $params)) {
            if ($this->accessToken = $response->access_token) {
                Settings::set('accessToken', $this->accessToken);
            }
        }
        return;
    }

    public function search()
    {
        if (($accessToken = Settings::get('accessToken')) && ($tag = Settings::get('tag'))) {
            $url = 'https://api.instagram.com/v1/tags/'.$tag.'/media/recent?access_token='.$accessToken;
            if ($response = $this->_getGetResponse($url)) {
                dd($response);
            }
        }
        return;
    }

    protected function _getUserProfile($userId = null, $accessToken = null)
    {
        if (!$accessToken) {
            if (!$accessToken = $this->accessToken) {
                return;
            }
        }
        $profile = [];
        $url = 'https://www.googleapis.com/oauth2/v1/userinfo?access_token='.$accessToken;
        if (!$response = $this->_getGetResponse($url)) {
            return;
        }
        $profile['id'] = $response->id;
        $profile['social_'.$this->code.'_id'] = $response->id;
        $profile['name'] = $response->name;
        $profile['email'] = $response->email;
        return $profile;
    }

    protected function _getPostResponse($url, $params)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $content = curl_exec($ch);
        $response = curl_getinfo($ch);
        curl_close($ch);
        Log::error($content);
        if ($response['http_code'] == 200) {
            return json_decode($content);
        }
        return;
    }

    protected function _getGetResponse($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);
        $response = curl_getinfo($ch);
        curl_close($ch);
        Log::error($content);
        if ($response['http_code'] == 200) {
            return json_decode($content);
        }
        return;
    }


}
