<?php namespace Kosmo\Profile\Classes;

use ApplicationException;
use October\Rain\Auth\Manager as RainAuthManager;
// use RainLab\User\Models\Settings as UserSettings;

class AuthManager extends RainAuthManager
{
    protected $userModel = 'Kosmo\Profile\Models\User';

    protected $groupModel = 'October\Rain\Auth\Models\Group';

    protected $throttleModel = 'October\Rain\Auth\Models\Throttle';

    protected $useThrottle = false;

    protected $requireActivation = false;

    protected $sessionKey = 'kosmo_auth';

    public $ipAddress = '0.0.0.0';

    /**
     * Getter\setter for protected attribute
     */
    public function getRequireActivation()
    {
        return $this->requireActivation;
    }

    public function setRequireActivation($value)
    {
        $this->requireActivation = $value;
    }

    /**
     * Finds a user by the given credentials.
     */
    public function findUserByCredentials(array $credentials)
    {
        $model = $this->createUserModel();
        $loginName = $model->getLoginName();

        if (!array_key_exists($loginName, $credentials)) {
            throw new ApplicationException('signin');
        }

        $query = $model->newQuery();
        $this->extendUserQuery($query);
        $hashableAttributes = $model->getHashableAttributes();
        $hashedCredentials = [];

        /*
         * Build query from given credentials
         */
        foreach ($credentials as $credential => $value) {
            // All excepted the hashed attributes
            if (in_array($credential, $hashableAttributes)) {
                $hashedCredentials = array_merge($hashedCredentials, [$credential => $value]);
            }
            else {
                $query = $query->where($credential, '=', $value);
            }
        }

        if (!$user = $query->first()) {
            throw new ApplicationException('signin');
        }

        /*
         * Check the hashed credentials match
         */
        foreach ($hashedCredentials as $credential => $value) {

            if (!$user->checkHashValue($credential, $value)) {
                // Incorrect password
                if ($credential == 'password') {
                    throw new ApplicationException('signin');
                }

                // User not found
                throw new ApplicationException('signin');
            }
        }

        return $user;
    }
}
