<?php namespace Kosmo\Profile\Classes;

use Request;

class SocialManager
{
    protected $manager = [];

    public function init()
    {
        $this->manager = [
            'ig' => IG::instance(),
        ];
    }

    public function callback($social)
    {
        if (!isset($this->manager[$social])) {
            return;
        }

        $userId = $this->manager[$social]->getUserId(Request::get('code'));
        if (!$userId) {
            return;
        }
    }
}
