<?php namespace Kosmo\Profile\Classes;

use Request;
use RainLab\User\Models\User as UserModel;

class GG extends SocialBase
{
    use \October\Rain\Support\Traits\Singleton;

    protected function init()
    {
        $this->code = 'gg';
        parent::init();
    }

    protected function _getLoginUrl()
    {
        $url = 'https://accounts.google.com/o/oauth2/auth?client_id='.$this->appId.'&redirect_uri='.$this->redirectUrl.'&response_type=code&scope=https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile';
        return $url;
    }

    protected function _getUserId($code = null)
    {
        if (!$code) {
            return;
        }
    	$result = false;
        $params = array(
            'client_id'     => $this->appId,
            'client_secret' => $this->appSecret,
            'redirect_uri'  => $this->redirectUrl,
            'grant_type'    => 'authorization_code',
            'code'          => $code
        );
    	$url = 'https://accounts.google.com/o/oauth2/token';
        if ($response = $this->_getPostResponse($url, $params)) {
            if ($this->accessToken = $response->access_token) {
                if ($profile = $this->_getUserProfile()){
                    return $profile['id'];
                }
            }
        }
        return;
    }

    protected function _getUserProfile($userId = null, $accessToken = null)
    {
        if (!$accessToken) {
            if (!$accessToken = $this->accessToken) {
                return;
            }
        }
        $profile = [];
        $url = 'https://www.googleapis.com/oauth2/v1/userinfo?access_token='.$accessToken;
        if (!$response = $this->_getGetResponse($url)) {
            return;
        }
        $profile['id'] = $response->id;
        $profile['social_'.$this->code.'_id'] = $response->id;
        $profile['name'] = $response->name;
        $profile['email'] = $response->email;
        return $profile;
    }

    protected function _getPostResponse($url, $params)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $content = curl_exec($ch);
        $response = curl_getinfo($ch);
        curl_close($ch);
        if ($response['http_code'] == 200) {
            return json_decode($content);
        }
        return;
    }

    protected function _getGetResponse($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);
        $response = curl_getinfo($ch);
        curl_close($ch);
        if ($response['http_code'] == 200) {
            return json_decode($content);
        }
        return;
    }
}
