<?php namespace Kosmo\Profile\Classes;

use Request;
use ApplicationException;

class Recaptcha
{
    use \October\Rain\Support\Traits\Singleton;

    public function captchaKey()
    {
        if (env('RECAPTCHA_DISABLED') || config('recaptcha.disabled')) {
            return;
        }
        return config('recaptcha.public');
    }

    public function check($captcha = null)
    {
        if (env('RECAPTCHA_DISABLED') || config('recaptcha.disabled')) {
            return true;
        }

        if (!$captcha) {
            return;
        }

        if (!$secret = config('recaptcha.private')) {
            return;
        }

        if (empty($secret)) {
            return;
        }

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,"secret=".$secret."&response=".$captcha);
        $content = json_decode(curl_exec($ch), true);
        curl_close($ch);

        if (isset($content['success']) && $content['success']) {
            return true;
        };

        return;
    }

}
