<?php namespace Kosmo\Profile\Classes;

use Log;
use Request;
use ApplicationException;

class SocialBase
{
    protected $code;
    protected $appId;
    protected $appSecret;
    protected $redirectUrl;
    protected $accessToken;
    protected $userId;
    protected $initiated = false;

    protected function init()
    {
        try {
            if (empty($this->code)) {
                throw new ApplicationException('Social code is empty.');
            }
            if (empty($config = config('social'))) {
                throw new ApplicationException('Social config is empty.');
            }
            if (!isset($config[$this->code])){
                throw new ApplicationException($this->code.' config is empty.');
            }
            if (empty($social = $config[$this->code])) {
                throw new ApplicationException($this->code.' config is empty.');
            }
            if (!isset($social['appId'])){
                throw new ApplicationException($this->code.' appId is empty.');
            }
            if (empty($this->appId = $social['appId'])) {
                throw new ApplicationException($this->code.' appId is empty.');
            }
            if (!isset($social['appSecret'])) {
                throw new ApplicationException($this->code.' appSecret is empty.');
            }
            if (empty($this->appSecret = $social['appSecret'])) {
                throw new ApplicationException($this->code.' appSecret is empty.');
            }
            if (!isset($config['callback']) && empty($config['callback'])) {
                throw new ApplicationException('Social callback url is empty.');
            }
            $this->redirectUrl = 'http://'.Request::server('HTTP_HOST').$config['callback'].'/'.$this->code;
            $this->initiated = true;
        } catch (ApplicationException $e) {
            Log::error($e);
        }
    }

    public function getLoginUrl()
    {
        if (!$this->_beforeGetLoginUrl())
        {
            return;
        }
        return $this->_getLoginUrl();
    }

    protected function _beforeGetLoginUrl()
    {
        if (!$this->initiated) {
            return;
        }
        return true;
    }

    protected function _getLoginUrl()
    {
        return;
    }

    public function getUserId($code = null)
    {
        if (!$this->_beforeGetUserId($code))
        {
            return;
        }
        return $this->_getUserId($code);
    }

    protected function _beforeGetUserId($code = null)
    {
        if (!$this->initiated) {
            return;
        }
        return true;
    }

    protected function _getUserId($code = null)
    {
        return;
    }

    public function getUserProfile($userId = null, $accessToken = null)
    {
        if (!$this->_beforeUserProfile($userId, $accessToken))
        {
            return;
        }
        return $this->_getUserProfile($userId, $accessToken);
    }

    protected function _beforeUserProfile($userId = null, $accessToken = null)
    {
        if (!$this->initiated) {
            return;
        }
        return true;
    }

    protected function _getUserProfile($userId = null, $accessToken = null)
    {
        return;
    }
}
