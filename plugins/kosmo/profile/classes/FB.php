<?php namespace Kosmo\Profile\Classes;

use Request;
use RainLab\User\Models\User as UserModel;

class FB extends SocialBase
{
    use \October\Rain\Support\Traits\Singleton;

    protected $fb;

    protected function init()
    {
        $this->code = 'fb';
        parent::init();
        // $this->initFacebook();
    }

    public function getFacebook()
    {
        if (!$this->fb) {
            $this->initFacebook();
        }
        return $this->fb;
    }

    public function initFacebook()
    {
        if(empty(session_id())) {
            session_start();
      	}

        if (!$this->fb) {
            $this->fb = new \Facebook\Facebook([
                'app_id' => $this->appId,
                'app_secret' => $this->appSecret,
                'default_graph_version' => 'v2.5',
            ]);
        }

        return $this->fb;
    }

    protected function _getLoginUrl()
    {
        $fb = $this->getFacebook();
        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email', 'user_friends'];
        return $helper->getLoginUrl($this->redirectUrl, $permissions);
    }

    protected function _getUserId($code = null)
    {
        if (!$code) {
            return;
        }

        $fb = $this->getFacebook();
        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        $this->accessToken = $accessToken->getValue();

        $oAuth2Client = $fb->getOAuth2Client();
        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId($this->appId);
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();
        $this->userId = $tokenMetadata->getUserId();

        return $tokenMetadata->getUserId();
    }

    protected function _getUserProfile($userId = null, $accessToken = null)
    {
        if (!$userId) {
            if (!$userId = $this->userId) {
                return;
            }
        }
        if (!$accessToken) {
            if (!$accessToken = $this->accessToken) {
                return;
            }
        }
        $fb = $this->getFacebook();
        $helper = $fb->getRedirectLoginHelper();

        try {
          $response = $fb->get('/me?fields=id,name,email', $accessToken);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          echo 'Graph returned an error: ' . $e->getMessage();
          exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          exit;
        }

        $response = $response->getGraphUser();
        $profile = [];
        if ($response) {
            $profile['id'] = $userId;
            $profile['social_'.$this->code.'_id'] = $userId;
            $profile['name'] = isset($response['name']) ? $response['name'] : '';
            $profile['email'] = isset($response['email']) ? $response['email'] : '';
            return $profile;
        }
        return;
    }
}
