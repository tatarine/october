<?php namespace Kosmo\Profile\Classes;

class FormatHelper
{
    public static function numberWithZeros($value, $zeros = 4)
    {
        return sprintf('%0'.$zeros.'d', $value);
    }

    public static $phoneFormats = [
        'az' => ['+994', 'NN XXX XX NN', '%2s%3s%2s%s', '+994(##)###-##-##', 9],
        'by' => ['+375', 'NN XXX XX NN', '%2s%3s%2s%s', '+375(##)###-##-##', 9],
        'kg' => ['+996', 'NNN XX XX NN', '%3s%2s%2s%s', '+996(###)##-##-##', 9],
        'kz' => ['+7', 'NNN XXX XX NN', '%3s%3s%2s%s', '+7(###)###-##-##', 10],
        // 'default' => ['+996', 'NNN XX XX NN', '%3s%2s%2s%s', '+996(###)##-##-##', 9],
        'default' => ['+7', 'NNN XXX XX NN', '%3s%3s%2s%s', '+7(###)###-##-##', 10]
    ];

    public static function phoneFormat($locale = null)
    {
        if (is_null($locale)) {
            $locale = env('APP_ENV');
        }
        if (!$locale) {
            $locale = 'default';
        }
        if (!isset(self::$phoneFormats[$locale])) {
            $locale = 'default';
        }
        return self::$phoneFormats[$locale];
    }

    public static function phoneMask($locale = null)
    {
        $format = self::phoneFormat($locale);
        return ['mask' => $format[3], 'length' => $format[4]];
    }

    public static function phoneWithPrefix($value, $withPlus = true, $locale = null)
    {
        $phone = self::phoneFormat($locale)[0].$value;
        if (!$withPlus) {
            $phone = str_replace('+', '', $phone);
        }
        return $phone;
    }

    public static function phoneWithMask($value, $withMask = true, $locale = null)
    {
        $format     = self::phoneFormat($locale);
        $prefix     = $format[0];
        $mask       = $format[1];
        $pattern    = $format[2];
        $phone = [];
        $scanValue = sscanf($value, $pattern);
        $scanMask = sscanf($mask, $pattern);
        // dd($scanValue, $scanMask);
        for ($i=0; $i < count($scanValue); $i++) {
            $chunk = $scanValue[$i];
            if ($withMask) {
                if (strstr($scanMask[$i], 'X')) {
                    $chunk = $scanMask[$i];
                }
            }
            $phone[] = $chunk;
        }
        array_unshift($phone, $prefix);
        return implode(' ', $phone);
    }

    public static function stringWithBreak($value)
    {
        return preg_replace('/\s+/', '<br/>', $value);
    }
}
