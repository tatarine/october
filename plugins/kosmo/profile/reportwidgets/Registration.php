<?php namespace Kosmo\Profile\ReportWidgets;

use DB;
use Carbon\Carbon;
use Kosmo\Profile\Models\User;
use Backend\Classes\ReportWidgetBase;

class Registration extends ReportWidgetBase
{

    public function defineProperties()
    {
        return [
            'title' => [
                'title'             => 'Widget title',
                'default'           => 'Пользователи',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'The Widget Title is required.'
            ],
            'days' => [
                'title'             => 'Number of days to display data for',
                'default'           => '7',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$'
            ]
        ];
    }

    public function render()
    {
        $days = ($this->property('days')) ? $this->property('days') : 7;

        $grouped = [];
        $grouped['fb'] = [];
        $grouped['vk'] = [];
        $grouped['web'] = [];
        $grouped['qiwi'] = [];
        $grouped['total'] = [];

        $results = [];
        $results['fb'] = 0;
        $results['vk'] = 0;
        $results['web'] = 0;
        $results['qiwi'] = 0;
        $results['total'] = 0;

        $users = DB::table('kosmo_profile_users as u')
                ->select('u.id', 'u.created_at', 'u.fb_token', 'u.vk_token', 'u.qiwi_token')
                ->orderBy('u.created_at')
                ->get();

        if (count($users)) {
            $date = null;
            foreach ($users as $user) {
                $created_at = Carbon::parse($user->created_at);
                if (!$date || !$created_at->lte($date)) {
                    $date = $created_at->endOfDay();
                    $grouped['fb'][$date->timestamp] = 0;
                    $grouped['vk'][$date->timestamp] = 0;
                    $grouped['web'][$date->timestamp] = 0;
                    $grouped['qiwi'][$date->timestamp] = 0;
                    $grouped['total'][$date->timestamp] = 0;
                }
                if ($user->fb_token) {
                    $grouped['fb'][$date->timestamp] += 1;
                    $results['fb'] += 1;
                } elseif ($user->vk_token) {
                    $grouped['vk'][$date->timestamp] += 1;
                    $results['vk'] += 1;
                } else {
                    $grouped['web'][$date->timestamp] += 1;
                    $results['web'] += 1;
                }

                if ($user->qiwi_token) {
                    $grouped['qiwi'][$date->timestamp] += 1;
                    $results['qiwi'] += 1;
                }

                $grouped['total'][$date->timestamp] += 1;
                $results['total'] += 1;
            }
        }

        $this->vars['items'] = $grouped;
        $this->vars['results'] = $results;
        return $this->makePartial('widget');
    }
}
?>
