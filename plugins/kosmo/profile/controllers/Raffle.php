<?php namespace Kosmo\Profile\Controllers;

use DB;
use Mail;
use Request;
use Redirect;
use BackendMenu;
use Kosmo\Quiz\Classes\RaffleWeekly;
use Kosmo\Quiz\Models\Prize as PrizeModel;
use Kosmo\Quiz\Models\Vote as VoteModel;
use Kosmo\Quiz\Models\Winning as WinningModel;
use Kosmo\Delivery\Models\Delivery as DeliveryModel;
use Kosmo\Profile\Models\Raffle as RaffleModel;
use Kosmo\Profile\Models\User as UserModel;
use Backend\Classes\Controller;

/**
 * Raffle Back-end Controller
 */
class Raffle extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kosmo.Profile', 'profile', 'raffle');
    }

    public function update($recordId = null, $context = null)
    {
        $this->vars['status'] = $this->formFindModelObject($recordId)->status;
        parent::update($recordId, $context);
    }

    public function onSetWinner($recordId = null)
    {
        $model = $this->formFindModelObject($recordId);
        if (!$model) {
            return;
        }
        if (!$prize = $model->prize) {
            return;
        }
        if (!$time = $model->created_at) {
            return;
        }
        if ($ticket = $model->ticket) {
            if (!$winner = $ticket->user) {
                return;
            }
            if ($winner->checkWinningsByType(PrizeModel::TYPE_WEEKLY)) {
                return;
            }
            $winning = RaffleWeekly::instance()->play($winner, ['ticket' => $ticket, 'prize' => $prize], $time);
            if ($winning) {
                $model->winner_id = $winner->id;
                $model->status = RaffleModel::STATUS_ACTIVE;
                $model->save();

                $winner->is_winner = 1;
                $winner->save();
            }
        } else {
            if (!$name = $model->user_fullname) {
                return;
            }
            if (!$number = $model->ticket_number) {
                return;
            }
            if (!$prize->available > 0) {
                return;
            }
            $winning = new WinningModel;
            $winning->prize_id = $prize->id;
            $winning->raffled_at = $time;
            $winning->save();

            $prize->unavailable = $prize->unavailable + 1;
            $prize->save();

            $model->winning_id = $winning->id;
            $model->status = RaffleModel::STATUS_ACTIVE;
            $model->save();
        }
        return Redirect::refresh();
    }
}
