<?php namespace Kosmo\Profile\Models;

use Model;
/**
 * Geoip Model
 */
class Geoip extends Model
{
    public $table = 'kosmo_profile_geoips';

    protected $jsonable = ['response'];

    public $belongsTo = [
        'country' => ['Kosmo\Profile\Models\Location', 'key' => 'country_id'],
        'city' => ['Kosmo\Profile\Models\Location', 'key' => 'city_id'],
    ];

    /**
     * Events
     */
    public function beforeSave()
    {
        if ($this->response) {
            if (isset($this->response['country'])) {
                $country = Location::whereSlug(strtolower($this->response['country']['names']['en']))->first();
                if ($country) {
                    $this->country_id = $country->id;
                }
            }
            if (isset($this->response['city'])) {
              $city = Location::whereSlug(strtolower($this->response['city']['names']['en']))->first();
              if ($city) {
                $this->city_id = $city->id;
              }
            }
        }
    }
}
