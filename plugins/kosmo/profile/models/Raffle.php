<?php namespace Kosmo\Profile\Models;

use Model;
use Kosmo\Quiz\Models\Prize;
use Kosmo\Quiz\Models\Winning;

/**
 * raffle Model
 */
class Raffle extends Model
{
    const STATUS_INACTIVE   = 'inactive';
    const STATUS_ACTIVE     = 'active';

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kosmo_profile_raffles';

    /**
     * @var array Guarded fields
     */
    public $translatable = ['user_city'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'winner' => ['Kosmo\Profile\Models\User', 'key' => 'winner_id'],
        'ticket' => ['Kosmo\Profile\Models\Ticket', 'key' => 'ticket_id'],
        'prize'  => ['Kosmo\Quiz\Models\Prize', 'key' => 'prize_id'],
        'winning' => ['Kosmo\Quiz\Models\Winning', 'key' => 'winning_id'],
    ];

    public function beforeDelete()
    {
        if ($this->status == self::STATUS_ACTIVE) {
            if ($this->winner) {
                $this->winner->is_winner = 0;
                $this->winner->save();
            }

            $this->prize->unavailable = $this->prize->unavailable - 1;
            $this->prize->save();

            $winning = Winning::where('user_id', $this->winner_id)->where('prize_id', $this->prize_id)->first();
            if ($winning) {
                if ($winning->delivery) {
                    $winning->delivery->delete();
                }
                $winning->delete();
            }
        }
    }

    public function getStatusOptions()
    {
        return [
            self::STATUS_INACTIVE => 'Неактивный',
            self::STATUS_ACTIVE   => 'Активный',
        ];
    }

    public function getStatusOption($status = null)
    {
        if (!$status) {
            $status = $this->status;
        }
        $options = $this->getStatusOptions();
        if (!isset($options[$status])) {
            return;
        }

        return $options[$status];
    }

    public function getPrizeOptions($keyValue = null)
    {
        $options = [];
        $prizes = Prize::weekly()->available()->get();
        if (!$prizes->isEmpty()) {
            foreach ($prizes as $prize) {
                $options[$prize->id] = $prize->name.' ('.$prize->comment.')';
            }
        }
        return $options;
    }

    public function getPhoneFormatted($masked = true)
    {
        if (!$this->user_phone) {
            return;
        }
        switch (env('APP_ENV')) {
            case 'by':
                $preffix = '+375';
                break;
            case 'az':
                $preffix = '+994';
                break;
            case 'kz':
                $preffix = '+7';
                break;
            case 'kg':
                $preffix = '0';
                break;
            default:
                $preffix = '+7';
                break;
        }
        $phone = '';

        if (in_array(env('APP_ENV'), ['az', 'by'])) {
            if ($masked) {
                $phone = $preffix.' '.substr($this->user_phone, 0, 2).' XXX XX '.substr($this->user_phone, 7);
            } else {
                $phone = $preffix.' '.substr($this->user_phone, 0, 2).' '.substr($this->user_phone, 2, 3).' '.substr($this->user_phone, 5);
            }
        }

        if (in_array(env('APP_ENV'), ['kz', 'dev'])) {
            if ($masked) {
                $phone = $preffix.' '.substr($this->user_phone, 0, 3).' XXX XX '.substr($this->user_phone, 8);
            } else {
                $phone = $preffix.' '.substr($this->user_phone, 0, 3).' '.substr($this->user_phone, 3, 3).' '.substr($this->user_phone, 6);
            }
        }

        if (in_array(env('APP_ENV'), ['kg'])) {
            if ($masked) {
                $phone = $preffix.' '.substr($this->user_phone, 0, 3).' XXX XX'.substr($this->user_phone, 8);
            } else {
                $phone = $preffix.' '.substr($this->user_phone, 0, 3).' '.substr($this->user_phone, 3, 3).' '.substr($this->user_phone, 6);
            }
        }

        return $phone;
    }
}
