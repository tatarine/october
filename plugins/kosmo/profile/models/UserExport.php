<?php namespace Kosmo\Profile\Models;

use DB;
use Carbon\Carbon;
use Kosmo\Export\Classes\ExportModel;

class UserExport extends ExportModel
{
    protected $fillable = ['begin_at', 'end_at'];

    public function exportData($columns, $sessionKey = null)
    {
        $result = [];
        $query = DB::table('kosmo_profile_users as u')
                ->leftJoin('kosmo_profile_locations as l', 'u.city_id', '=', 'l.id')
                ->select('u.*',
                        'l.name as city');
        if ($this->begin_at) {
            $begin_at = Carbon::createFromFormat('Y-m-d H:i:s', $this->begin_at)->startOfDay()->toDateTimeString();
            $query = $query->where('u.created_at', '>=', $begin_at);
        }
        if ($this->end_at) {
            $end_at = Carbon::createFromFormat('Y-m-d H:i:s', $this->end_at)->endOfDay()->toDateTimeString();
            $query = $query->where('u.created_at', '<=', $end_at);
        }
        $users = $query->get();
        foreach ($users as $i => $user) {
            $result[$i] = [];
            foreach ($user as $key => $value) {
                if (in_array($key, $columns)) {
                    $result[$i][$key] = $value;
                }
            }
        }
        return $result;
    }
}
?>
