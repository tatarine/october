<?php namespace Kosmo\Profile\Models;

use DB;
use Carbon\Carbon;
use Kosmo\Export\Classes\ExportModel;
use Kosmo\Profile\Models\Ticket;

class TicketExport extends ExportModel
{
    protected $fillable = ['begin_at', 'end_at'];

    public function exportData($columns, $sessionKey = null)
    {
        $result = [];
        $query = DB::table('kosmo_profile_tickets as t')
                ->leftJoin('kosmo_profile_users as u', 't.user_id', '=', 'u.id')
                ->select('t.*',
                        'u.name as user_name',
                        'u.email as user_email',
                        'u.phone as user_phone');
        if ($this->begin_at) {
            $begin_at = Carbon::createFromFormat('Y-m-d H:i:s', $this->begin_at)->startOfDay()->toDateTimeString();
            $query = $query->where('t.created_at', '>=', $begin_at);
        }
        if ($this->end_at) {
            $end_at = Carbon::createFromFormat('Y-m-d H:i:s', $this->end_at)->endOfDay()->toDateTimeString();
            $query = $query->where('t.created_at', '<=', $end_at);
        }
        $tickets = $query->get();

        $model = new Ticket;
        foreach ($tickets as $i => $ticket) {
            $result[$i] = [];
            foreach ($ticket as $key => $value) {
                if (in_array($key, $columns)) {
                    if ($key == 'status') {
                        $result[$i][$key] = $model->getStatusOption($value);
                    } elseif ($key == 'type') {
                        $result[$i][$key] = $model->getTypeOption($value);
                    } elseif ($key == 'user_phone') {
                        $result[$i][$key] = '"+7'.$value.'"';
                    } elseif ($key == 'created_at') {
                        $result[$i][$key] = $value;
                    } else {
                        $result[$i][$key] = $value;
                    }
                }
            }
        }
        return $result;
    }
}
?>
