<?php namespace Kosmo\Profile\Models;

use DB;
use Log;
use URL;
use Mail;
use Request;
use October\Rain\Auth\Models\User as UserBase;
// use Kosmo\Quiz\Models\Level;
// use Kosmo\Quiz\Models\LevelPuzzle;
// use Kosmo\Quiz\Models\LevelVote;
// use Kosmo\Quiz\Models\LevelPuzzleVote;
use Kosmo\Quiz\Models\Prize;
use Kosmo\Quiz\Models\Winning;
use Kosmo\Profile\Classes\FormatHelper;
/**
 * user Model
 */
class User extends UserBase
{
    const SCORES_PER    = 1;
    const LIMIT_CODES   = 1;
    const LIMIT_PRIZES  = 15;
    const LIMIT_FRIENDS = 5;
    const LIMIT_IP      = 3;

    protected $table = 'kosmo_profile_users';

    protected $dates = ['activated_at', 'last_login', 'birthday'];

    protected $hidden = ['password', 'reset_password_code', 'activation_code', 'persist_code', 'fb_token', 'vk_token', 'qiwi_token', 'ip'];

    protected $guarded = ['is_superuser'];

    public $rules = [];

    public $attachOne = [
        'avatar' => 'System\Models\File'
    ];

    public $hasMany = [
        'winnings' => ['Kosmo\Quiz\Models\Winning', 'key' => 'user_id'],
        'tickets' => ['Kosmo\Profile\Models\Ticket', 'key' => 'user_id'],
        'tickets_count' => ['Kosmo\Profile\Models\Ticket', 'key' => 'user_id', 'count' =>true],
        'transactions' => ['Kosmo\Profile\Models\Transaction', 'key' => 'user_id'],
    ];

    public $hasOne = [
    ];

    public $belongsTo = [
        'country' => ['Kosmo\Profile\Models\Location', 'key' => 'country_id'],
        'city' => ['Kosmo\Profile\Models\Location', 'key' => 'city_id'],
    ];

    public static $loginAttribute = 'phone';

    public static $loginAttributeLabel = 'Cell phone';

    /**
     * Scopes
     */
    public static function findByEmail($email)
    {
        if (!$email) {
            return;
        }

        return self::where('email', $email)->first();
    }

    public static function findByPhone($phone)
    {
        if (!$phone) {
            return;
        }

        return self::where('phone', $phone)->first();
    }

    public static function findBySocial($social, $value)
    {
        if (!$value) {
            return;
        }

        return self::where($social.'_token', $value)->first();
    }

    /**
     * Events
     */

    /**
     * Mutators
     */
    public function getThumb($width, $height, $mode)
    {
        if (!$this->avatar) {
            return;
        }
        return $this->avatar->getThumb($width, $height, ['mode' => $mode]);
    }

    public function getPhoneWithPreffix($withPlus = true)
    {
        return FormatHelper::phoneWithPrefix($this->phone, $withPlus);
    }

    public function getPhoneFormatted($withMask = true)
    {
        return FormatHelper::phoneWithMask($this->phone, $withMask);
    }

    public function getNameFormatted()
    {
        return FormatHelper::stringWithBreak($this->name);
    }

    /**
     * Attributes
     */
    public function getWinningByType($type)
    {
        foreach ($this->winnings as $winning) {
            if ($winning->prize->type == $type) {
                return $winning;
            }
        }
        return;
    }

    public function getWinningByStatus($status)
    {
        foreach ($this->winnings as $winning) {
            if ($winning->status == $status) {
                return $winning;
            }
        }
        return;
    }

    public function getRaffledWinning()
    {
        return $this->getWinningByStatus(Winning::STATUS_RAFFLED);
    }

    public function hasRaffledWinning()
    {
        return !empty($this->getRaffledWinning());
    }

    public function getAssuredWinning()
    {
        return $this->getWinningByType(Prize::TYPE_ASSURED);
    }

    public function getAchievedWinning()
    {
        return $this->getWinningByType(Prize::TYPE_ACHIEVED);
    }

    public function getPrizeByType($type)
    {
        foreach ($this->winnings as $winning) {
            if ($winning->prize->type == $type) {
                return $winning->prize;
            }
        }
        return;
    }

    public function getAssuredPrize()
    {
        return $this->getPrizeByType(Prize::TYPE_ASSURED);
    }

    public function getPrizesByType($type)
    {
        $prizes = collect([]);
        foreach ($this->winnings as $winning) {
            if ($winning->prize->type == $type) {
                $prizes->push($winning->prize);
            }
        }
        return $prizes;
    }

    public function getAssuredPrizes()
    {
        return $this->getPrizesByType(Prize::TYPE_ASSURED);
    }

    public function hasPrizeById($id)
    {
        foreach ($this->winnings as $winning) {
            if ($winning->prize->id == $id) {
                return true;
            }
        }
        return;
    }
    /*
    * Tickets, scores
    */
    public function getTicketsDesc()
    {
        return $this->tickets()->orderBy('created_at', 'desc')->get();
    }

    public function getTicketsActive()
    {
        return $this->tickets()->active()->count();
    }

    public function getTicketsUsed()
    {
        return $this->tickets()->used()->count();
    }

    public function getScoresActive()
    {
        return $this->getTicketsActive() * self::SCORES_PER;
    }

    public function getScoresUsed()
    {
        return $this->getTicketsUsed() * self::SCORES_PER;
    }

    // public function canSpentScores()
    // {
    //     $prizes = Prize::achieved()->available()->get();
    //     foreach ($prizes as $prize) {
    //         if (!$this->hasPrizeById($prize->id)) {
    //             if ($prize->checkScores($this->getScoresActive())) {
    //                 return true;
    //             }
    //         }
    //     }
    //     return;
    // }
    /*
    * Winnings
    */
    public function checkWinnings()
    {
        return $this->is_winner;
    }

    public function checkWinningsLimit()
    {
        $count = $this->winnings()->count();
        return $count >= self::LIMIT_PRIZES;
    }

    public function checkWinningsById($id = null)
    {
        if (!$id) {
            return;
        }

        foreach ($this->winnings as $winning) {
            if ($winning->prize_id == $id) {
                return true;
            }
        }
        return;
    }

    public function checkWinningsByType($type = null)
    {
        if (!$type) {
            return;
        }

        foreach ($this->winnings as $winning) {
            if ($winning->prize->type == $type) {
                return true;
            }
        }
        return;
    }

    public function checkWinningsByStatus($status = null)
    {
        if (!$status) {
            return;
        }

        foreach ($this->winnings as $winning) {
            if ($winning->status == $status) {
                return $winning;
            }
        }
        return;
    }

    public function checkWinningsByIp($type = null)
    {
        if (!$type) {
            return;
        }

        if (!$ip = $this->ip) {
            return;
        }

        $users = DB::table('kosmo_profile_users as u')
            ->select('u.id', 'u.email', 'u.phone', 'u.ip')
            ->leftJoin('kosmo_quiz_winnings as w', 'u.id', '=', 'w.user_id')
            ->leftJoin('kosmo_quiz_prizes as p', 'p.id', '=', 'w.prize_id')
            ->where('p.type', '=', $type)
            ->where('u.ip', '=', $ip)
            ->get();

        if (count($users) >= self::LIMIT_IP) {
            Log::info('Blocked by ip: '.$this->id);
            return true;
        }

        return;
    }

    public function checkWinningByBlockList()
    {
        if (!$ips = config('block.ips')) {
            return;
        }
        if (empty($ips)) {
            return;
        }
        if (!$ip = $this->ip) {
            return;
        }
        if (in_array($this->ip, $ips)) {
            return true;
        }
        return;
    }

    public function getUnfilledWinning()
    {
        if (!$winnings = $this->winnings) {
            return;
        }
        if ($winnings->isEmpty()) {
            return;
        }
        $filtered = $winnings->filter(function($item) {
            return ($item->status == Winning::STATUS_RAFFLED) && ($item->prize->type == Prize::TYPE_DAILY);
        });
        if ($filtered->isEmpty()) {
            return;
        }
        return $filtered->first();
    }

    /*
    * Location
    */
    public function listCities($keyValue = null, $fieldName = null)
    {
        $list = [];
        $cities = Location::isEnabled()->cities()->get();
        foreach ($cities as $city) {
            $list[$city->id] = $city->name;
        }
        return $list;
    }
}
