<?php namespace Kosmo\Profile\Models;

use Model;
use Request;
use Kosmo\Profile\Classes\AuthManager;
/**
 * location Model
 */
class Location extends Model
{

    public $table = 'kosmo_profile_locations';

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    /**
     * @var array Guarded fields
     */
    public $translatable = ['name'];

    /**
     * @var array Relations
     */
    public $hasMany = [
        'children' => ['Kosmo\Profile\Models\Location', 'key' => 'location_id', 'order' => 'name asc']
    ];

    public $belongsTo = [
        'parent' => ['Kosmo\Profile\Models\Location', 'key' => 'location_id']
    ];

    /**
     * Events
     */
    public function beforeSave()
    {
        if ($this->parent) {
            $this->level = $this->parent->level + 1;
        } else {
            $this->level = 1;
        }

        if (!$this->slug) {
            $this->slug = str_slug($this->name);
        }

        if ($this->isDirty('is_enabled')) {
            if ($this->children) {
                foreach ($this->children as $location) {
                    $location->is_enabled = $this->is_enabled;
                    $location->save();
                }
            }
        }
    }

    /**
     * Scopes
     */
    public function scopeIsEnabled($query)
    {
        return $query->where(['is_enabled' => 1]);
    }

    public function scopeCities($query)
    {
        return $query->where(['type' => 'city']);
    }

    public function scopeDefaultCountry($query)
    {
        return $query->where(['type' => 'country', 'is_default' => 1]);
    }

    public function scopeDefaultCity($query)
    {
        return $query->where(['type' => 'city', 'is_default' => 1]);
    }
    /**
     * Attributes
     */
     public static function getLocationOptions()
     {
        $location = Location::defaultCountry()->firstOrFail();
         return $location->children;
     }

     public static function isLocationDefault($ipAddress = null){ return self::isDefaultLocation($ipAddress); }
     public static function isDefaultLocation($ipAddress = null)
     {
        if(isset($_REQUEST['forcedefault']) && $_REQUEST['forcedefault']) return true;
        if(isset($_REQUEST['forceregion']) && $_REQUEST['forceregion']) return false;
        $location = self::getLocation($ipAddress);
        if ($location) {
            return $location->is_default ? true : false;
        }
        return;
     }

     public static function getDefaultLocation($ipAddress = null)
     {
        $location = self::getLocation($ipAddress);
        if ($location) {
            return $location->id;
        }
        return;
     }

     protected static function getLocation($ipAddress = null)
     {
         $user = AuthManager::instance()->getUser();
         if ($user) {
             return $user->city;
         }

         if (!$ipAddress) {
             $ipAddress = Request::ip();
         }
         $geoip = Geoip::where('ip', $ipAddress)->first();
         if ($geoip) {
             return $geoip->city;
         }

         return;
     }
}
