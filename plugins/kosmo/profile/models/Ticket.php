<?php namespace Kosmo\Profile\Models;

use Hash;
use Model;

/**
 * ticket Model
 */
class Ticket extends Model
{
    const STATUS_INACTIVE = 'inactive';
    const STATUS_ACTIVE   = 'active';
    const STATUS_USED     = 'used';

    const TYPE_WEB = 'web';
    const TYPE_SMS = 'sms';
    const TYPE_QIWI = 'qiwi';

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kosmo_profile_tickets';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'user' => ['Kosmo\Profile\Models\User', 'key' => 'user_id'],
    ];

    public $hasOne = [
        'winning' => ['Kosmo\Quiz\Models\Winning', 'key' => 'ticket_id'],
        // 'levelVote' => ['Kosmo\Quiz\Models\LevelVote', 'key' => 'ticket_id']
    ];
    //
    // public $hasMany = [
    //     'puzzleVotes' => ['Kosmo\Quiz\Models\LevelPuzzleVote', 'key' => 'ticket_id']
    // ];

    /**
     * Events
     */
    public function scopeActive($query)
    {
        $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeInactive($query)
    {
        $query->where('status', self::STATUS_INACTIVE);
    }

    public function scopeUsed($query)
    {
        $query->where('status', self::STATUS_USED);
    }
    /**
     * Attributes
     */
    public function hasWinning()
    {
        return !is_null($this->winning);
    }

    public function getStatusOptions()
    {
        return [
            self::STATUS_INACTIVE => 'Неактивный',
            self::STATUS_ACTIVE => 'Активный'
        ];
    }

    public function getStatusOption($status = null)
    {
        if (is_null($status)) {
            $status = $this->status;
        }
        $options = $this->getStatusOptions();
        if (!isset($options[$status])) {
            return;
        }

        return $options[$status];
    }

    public function getTypeOptions()
    {
        return [
            self::TYPE_WEB  => 'Сайт',
            self::TYPE_SMS  => 'SMS',
            self::TYPE_QIWI => 'QIWI',
        ];
    }

    public function getTypeOption($type = null)
    {
        if (is_null($type)) {
            $type = $this->type;
        }
        $options = $this->getTypeOptions();
        if (!isset($options[$type])) {
            return;
        }

        return $options[$type];
    }

    public function setUsed()
    {
        $this->status = self::STATUS_USED;
        $this->save();
    }

    public function setInactive()
    {
        $this->status = self::STATUS_INACTIVE;
        $this->save();
    }

    /**
     * Events
     */
    public function beforeCreate()
    {
        if ($this->user_id) {
            if ($this->user->is_winner) {
                $this->status = self::STATUS_INACTIVE;
            } else {
                $this->status = self::STATUS_ACTIVE;
            }
        }
    }

    /**
     * Attributes
     */
    //  public function isAvailable()
    //  {
    //     return ($this->levelVote) ? false : true;
    //  }


}
