<?php namespace Kosmo\Profile\Models;

use Model;

/**
 * Transaction Model
 */
class Transaction extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'kosmo_profile_transactions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'user' => ['Kosmo\Profile\Models\User', 'key' => 'user_id']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * Events
     */
    public function beforeCreate()
    {
        $this->hash = uniqid();
    }
    /**
     * Scopes
     */
    public function scopeByUser($query, $value)
    {
        return $query->where('user_id', $value);
    }

    public function scopeByCode($query, $value)
    {
        return $query->where('code', $value);
    }

    public function scopeByHash($query, $value)
    {
        return $query->where('hash', $value);
    }
}
