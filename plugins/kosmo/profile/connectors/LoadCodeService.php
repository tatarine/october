<?php namespace Kosmo\Profile\Connectors;

use ApplicationException;
use October\Rain\Extension\Extendable;

class LoadCodeService extends Extendable
{
    use \October\Rain\Support\Traits\Singleton;

	public $implement = [];

    public function __construct()
    {
        if (in_array(env('APP_ENV'), ['kz', 'az', 'kg', 'dev', 'prod'])) {
            $this->implement[] = 'Kosmo.Profile.Connectors.RglExtension';
        }
        if (in_array(env('APP_ENV'), ['by'])) {
            $this->implement[] = 'Kosmo.Profile.Connectors.BambooExtension';
        }
        parent::__construct();
    }
}
