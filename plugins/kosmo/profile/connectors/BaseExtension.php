<?php namespace Kosmo\Profile\Connectors;

use ApplicationException;
use October\Rain\Extension\ExtensionBase;
use Kosmo\Profile\Connectors\LoadCodeHelper as Helper;

class BaseExtension extends ExtensionBase
{

    public function headers($headers = [])
    {
        $flat = [];
        if (!empty($headers)) {
            foreach ($headers as $key => $value) {
                $flat[] = $key.': '.$value;
            }
        }
        return $flat;
    }

    public function params($params = [])
    {
        $params = $this->sign($params);
        $params = http_build_query($params);
        return $params;
    }

    public function sign($params = [])
    {
        return $params;
    }

    public function url($routes = [], $url = null)
    {
        if (!$url) {
            $url = '';
        }
        if (!empty($routes)) {
            $url .= '/'.implode('/', $routes);
        }
        return $url;
    }

    public function response($response)
    {
        return $response;
    }

	protected function request($url, $headers = [])
    {
		$request = curl_init();
        curl_setopt($request, CURLOPT_URL, $url);
        curl_setopt($request, CURLOPT_HEADER, false);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
        return $request;
	}

    protected function post($routes = [], $params = [], $headers = [])
    {
        $url        = $this->url($routes);
        $headers    = $this->headers($headers);
        $params     = $this->params($params);
        $request    = $this->request($url, $headers);
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, $params);
        $response   = $this->response($this->exec($request));
        return $response;
    }

    protected function get($routes = [], $params = [], $headers = [])
    {
        $url        = $this->url($routes);
        $headers    = $this->headers($headers);
        $params     = $this->params($params);
        $request    = $this->request($url.'?'.$params, $headers);
        $response   = $this->response($this->exec($request));
        return $response;
    }

    protected function exec($request)
    {
        try {
            $response = curl_exec($request);
        } catch (Exception $e) {
            return;
        } catch (ApplicationException $e) {
            return;
        }
        return $response;
    }
}
