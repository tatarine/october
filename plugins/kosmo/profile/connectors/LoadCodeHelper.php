<?php namespace Kosmo\Profile\Connectors;


class LoadCodeHelper
{
    const STATUS_OK       = 1;
    const STATUS_FAIL     = 0;

    const USER_EXISTS     = 100;

    const CODE_USED_OK    = 201;
    const CODE_AVAILABLE  = 202;
    const CODE_ALREADY    = 203;
    const CODE_NX         = 204;

    const UNKNOWN         = 500;
}
