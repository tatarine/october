<?php namespace Kosmo\Profile\Connectors;

use Log;
use ValidationException;
use ApplicationException;
use Kosmo\Profile\Connectors\LoadCodeHelper as Helper;

class BambooExtension extends BaseExtension
{
    const STATUS_CODE_SUCCESS           = 1;
    const STATUS_CODE_VALID             = 2;
    const STATUS_CODE_REGISTRED         = 3;
    const STATUS_CODE_INVALID           = 4;
    const STATUS_CODE_WAITING_PROCESS   = 5;

    //Set default headers
    public function headers($headers = [])
    {
        return parent::headers($headers);
    }

    //Set default params
    public function params($params = [])
    {
        return parent::params($params);
    }

    //Set default url
    public function url($routes = [], $url = null)
    {
        $url = config('connectors.url');
        Log::error(json_encode($routes, JSON_UNESCAPED_UNICODE));
        return parent::url($routes, $url);
    }

    //Set default sign
    public function sign($params = [])
    {
        $sorted = $params;
        ksort($sorted);
        $prehash = '';
        foreach ($sorted as $key => $value) {
            if (!is_numeric($value) && empty($value)) continue;
            $prehash .= $key.$value;
        }
        $prehash = $prehash.config('connectors.key');
		$params['signature'] = base64_encode(hash('sha256', $prehash));
        return parent::sign($params);
    }

    //Set response handler
    public function response($response)
    {
        $response = parent::response($response);
        $response = json_decode($response, true);
        if ($response['success'] === false) {
            $response['exception'] = 'load_code_error';
            $response['message'] = $response['message']['error'];
            $json = json_encode($response, JSON_UNESCAPED_UNICODE);
            Log::error($json);
            throw new ApplicationException($json);
        }
        if (!isset($response['message'])) {
            throw new ApplicationException('Invalid response');
        }
        return $response['message'];
    }

    /*
    *  Requests to API
    */
    public function checkCode($code)
    {
        $response = $this->get(
            ['check'],
            ['code' => $code]
        );
        return $response;
    }

    public function checkRegister($phone, $code)
    {
        $response = $this->get(
            ['register', 'check'],
            [
                'msisdn' => $phone,
                'code'   => $code
            ]
        );
        return $response;
    }

    public function loadCode($phone, $data)
    {
        $response = $this->checkRegister($phone, $data['promo_code']);
        if ($response['status'] == self::STATUS_CODE_REGISTRED) {
            throw new ApplicationException('code_activated');
        }
        if ($response['status'] == self::STATUS_CODE_INVALID) {
            throw new ApplicationException('code_invalid');
        }
        if ($response['status'] == self::STATUS_CODE_SUCCESS) {
            return $response;
        }
        if ($response['status'] == self::STATUS_CODE_VALID || $response['status'] == self::STATUS_CODE_WAITING_PROCESS) {
            $response = $this->registerProfile($phone, $data);
        }
        return $response;
    }

    public function registerProfile($phone, $data)
    {
        $response = $this->get(
            ['register', 'process'],
            [
                'msisdn'        => $phone,
                'code'          => $data['promo_code'],
                'email'         => $data['user_email'],
                'name'          => $data['user_firstname'],
                'surname'       => $data['user_lastname'],
                'middle_name'   => $data['user_middlename'],
                'city'          => $data['user_city'],
                'street'        => $data['user_street'],
                'building'      => $data['user_house'],
                'flat'          => $data['user_flat']
            ]
        );
        return $response;
    }

    public function requestList($phone)
    {
        $list = [];
        $response = $this->get(
            ['get'],
            ['msisdn' => $phone]
        );
        if (isset($response['codes'])) {
            foreach ($response['codes'] as $code) {
                $list[] = [
                    'code' => $code,
                    'datetime' => date('d.m.Y H:i:s'),
                    'type' => 'sms'
                ];
            }
        }
        return $list;
    }
}
