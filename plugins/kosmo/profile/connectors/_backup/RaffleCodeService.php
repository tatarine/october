<?php namespace Kosmo\Profile\Connectors;

use ApplicationException;

/*
 *	USAGE:
 		RaffleCodeService::instance()->tryRaffleCode($userphone, $code, $creds);

		RaffleCodeService::instance()->getAllRaffleCodes($userphone);
*/

class RaffleCodeService extends \October\Rain\Extension\Extendable
{
	const RCS_OK = 1;
	const RCS_FAIL = 0;
	const RCS_USER_EXISTS = 100;

	const RCS_CODE_USED_OK = 201;
	const RCS_CODE_AVAILABLE = 202;
	const RCS_CODE_ALREADY = 203;
	const RCS_CODE_NX = 204;

	const RCS_UNKNOWN = 500;
	public $implement = [];
	private static $_instance = null;

    public function __construct()
    {
        switch(env('APP_ENV'))
		{
			case 'by':
            	$this->implement[] = 'Kosmo.Profile.Connectors.RaffleCodeBamboo';
				break;
			default:
				$this->implement[] = 'Kosmo.Profile.Connectors.RaffleCodeRGL';
        }

        parent::__construct();
    }

	public static function instance(){
		if(self::$_instance) return self::$_instance;
		return self::$_instance = new RaffleCodeService();
	}
}
