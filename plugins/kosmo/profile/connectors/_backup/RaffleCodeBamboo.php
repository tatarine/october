<?php namespace Kosmo\Profile\Connectors;

use ApplicationException;
use October\Rain\Extension\Extendable;
use Kosmo\Profile\Connectors\LoadCodeHelper as Helper;

class RaffleCodeBamboo extends ExtensionBase
{
	public function whoami(){ return __class__; } //testing only

	const API_URL = 'http://foo.bar/';
	const API_PW = 'fill-me';

	public function checkRaffleCodeAvailable($code){
		$rsp = self::_request('check', [
			'code' => $code
		]);
		if(!isset($rsp["is_registered"]))
			return Helper::RCS_UNKNOWN;

		return $rsp["is_registered"] ? Helper::RCS_CODE_ALREADY: Helper::RCS_CODE_AVAILABLE;
	}

	public function tryRaffleCode($userid, $code, $credentials = []){
		$rsp = self::_request('register/check', [
			'msisdn' => $userid,
			'code' => $code
		]);

		if($rsp['next'] == 'process' && !empty($credentials))
			return $this->registerParticipantWithCode($userid, $code, $credentials);

		if(isset($rsp['status']))
			switch($rsp['status']){
				case 1: return Helper::RCS_CODE_USED_OK;
				case 2: return Helper::RCS_CODE_AVAILABLE;
				case 3: return Helper::RCS_CODE_ALREADY;
				case 4: return Helper::RCS_CODE_NX;
				default: return Helper::RCS_UNKNOWN;
			}
		return Helper::RCS_FAIL;
	}

	public function registerParticipantWithCode($userid, $code, $credentials = []){
		$data = [
			'msisdn' => $userid,
			'code' => $code
		];

		$rsp = self::_request('register/process', array_merge($data,$credentials));

		if(isset($rsp['status']))
			switch($rsp['status']){
				case 1: return Helper::RCS_CODE_USED_OK;
				case 2: return Helper::RCS_CODE_AVAILABLE;
				case 3: return Helper::RCS_CODE_ALREADY;
				case 4: return Helper::RCS_CODE_NX;
				default: return Helper::RCS_UNKNOWN;
			}
		return Helper::RCS_FAIL;
	}

	public function getAllRaffleCodes($userid){
		$rsp = self::_request('get', ['msisdn' => $userid]);
		if(!isset($rsp['codes']) || !is_array($rsp['codes']))
			return [];
		return $rsp['codes'];
	}

	private static function _sign_prepare($params)
	{
		$_tmp = $params;
		ksort($params);
		$prehash = '';
		foreach ($params as $key => $value)
			$prehash .= $key.$value;

		$_tmp['signature'] = hash ( 'sha256' , $prehash );
		return http_build_query($_tmp);
	}

	private static function _request($type, $params){
        $url = self::_url();
		$ch = curl_init($url.$type.'?'.self::_sign_prepare($params));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

		//curl_setopt($ch, CURLOPT_POST, true);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $cData);

        $rawResp = curl_exec($ch);

		$jsonResp = json_decode($rawResp,true);
		if($jsonResp['message'])
			return $jsonResp['message'];

		return [];
	}

    private static function _url()
    {
        return config('connectors.url').'/'.config('connectors.promo');
    }
}
