<?php namespace Kosmo\Profile\Connectors;

use Log;
use ValidationException;
use ApplicationException;
use Kosmo\Profile\Models\Transaction;
use Kosmo\Profile\Connectors\LoadCodeHelper as Helper;

class RglExtension extends BaseExtension
{
    //Set default headers
    public function headers($headers = [])
    {
        // Log::error(json_encode($headers, JSON_UNESCAPED_UNICODE));
        return parent::headers($headers);
    }

    //Set default params
    public function params($params = [])
    {
        // Log::error(json_encode($params, JSON_UNESCAPED_UNICODE));
        if (isset($params['json'])) {
            $params = $this->sign($params);
            $params = $params['json'];
            return $params;
        } else {
            return parent::params($params);
        }
    }

    //Set default url
    public function url($routes = [], $url = null)
    {
        $url = config('connectors.url').'/'.config('connectors.id');
        // Log::error(json_encode($routes, JSON_UNESCAPED_UNICODE));
        return parent::url($routes, $url);
    }

    //Set response handler
    public function response($response)
    {
        $response = parent::response($response);
        Log::error($response);
        $response = json_decode($response, true);
        if ($response['code'] != 200) {
            if(isset($response['data'])){
                if (isset($response['data']['transaction_id'])) {
                    if ($transaction = Transaction::byHash($response['data']['transaction_id'])->first()) {
                        $transaction->delete();
                    }
                }
            }
            $response['exception'] = 'load_code_error';
            $response = json_encode($response, JSON_UNESCAPED_UNICODE);
            throw new ApplicationException($response);
        }
        if (!isset($response['data'])) {
            Log::error('Invalid RGL data in response');
            throw new ApplicationException('Invalid data in response');
        }
        return $response;
    }

    /*
    *  Requests to API
    */
    public function loadCode($phone, $data)
    {
        if (!isset($data['promo_language'])) {
            $data['promo_language'] = 'ru';
        }
        if (!isset($data['promo_app_type'])) {
            $data['promo_app_type'] = 'web';
        }

        $response = $this->post(
            ['w', 'users', $phone, 'codes', 'redeem'],
            [
                'transaction_id'    => $data['transaction_id'],
                'promo_app_type'    => $data['promo_app_type'],
                'promo_language'    => $data['promo_language'],
                'promo_code'        => $data['promo_code'],
                'user_email'        => $data['user_email'],
                'user_firstname'    => $data['user_firstname'],
                'user_lastname'     => $data['user_lastname'],
                'user_middlename'   => $data['user_middlename'],
                'user_city'         => $data['user_city'],
                'user_age'          => $data['user_age'],
                'user_gender'       => $data['user_gender'],
                'promo_time'        => $data['promo_time'],
            ],
            [
                'api-key' => config('connectors.key'),
                'User-Language' => $data['promo_language'],
                'Content-Type'  => 'application/x-www-form-urlencoded'
            ]
        );
        return $response;
    }

    // public function addPrize($phone, $data)
    // {
    //     if (!isset($data['promo_language'])) {
    //         $data['promo_language'] = 'ru';
    //     }
    //     if (!isset($data['promo_app_type'])) {
    //         $data['promo_app_type'] = 'web';
    //     }
    //     if (!isset($data['method'])) {
    //         $data['method'] = 'shipping';
    //     }
    //     $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    //     $response = $this->post(
    //         ['w', 'users', $phone, 'prizes', 'add'],
    //         [
    //             'json' => $json
    //         ],
    //         [
    //             'api-key' => config('connectors.key'),
    //             'User-Language' => $data['promo_language'],
    //             'Content-Type'  => 'application/json',
    //             'Content-Length' => strlen($json)
    //         ]
    //     );
    //     return $response;
    // }
    //
    // public function updatePrize($phone, $data)
    // {
    //     if (!isset($data['promo_language'])) {
    //         $data['promo_language'] = 'ru';
    //     }
    //     if (!isset($data['promo_app_type'])) {
    //         $data['promo_app_type'] = 'web';
    //     }
    //     $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    //     $response = $this->post(
    //         ['w', 'users', $phone, 'prizes', 'update'],
    //         [
    //             'json' => $json
    //         ],
    //         [
    //             'api-key' => config('connectors.key'),
    //             'User-Language' => $data['promo_language'],
    //             'Content-Type'  => 'application/json',
    //             'Content-Length' => strlen($json)
    //         ]
    //     );
    //     return $response;
    // }

    public function requestUserProfile($phone)
    {
        $response = $this->get(
            ['r', 'users', $phone, 'info'],
            [],
            ['api-key' => config('connectors.key')]
        );
        return $response;
    }

    public function requestUserCodes($phone)
    {
        $list = [];
        $response = $this->get(
            ['r', 'users', $phone, 'codes', 'list'],
            [
                'page' => 1,
                'pageSize' => 1000
            ],
            ['api-key' => config('connectors.key')]
        );
        foreach ($response['data'] as $field) {
            if (isset($field['promo_code'])) {
                $datetime   = isset($field['promo_datetime']) ? $field['promo_datetime'] : date('d.m.Y H:i:s');
                $type       = isset($field['promo_app_type']) ? $field['promo_app_type'] : 'sms';
                $prize_id   = isset($field['prize_id']) ? $field['prize_id'] : null;
                $list[] = [
                    'code'      => $field['promo_code'],
                    'datetime'  => $datetime,
                    'type'      => $type,
                    'prize_id'  => $prize_id
                ];
            }
        }
        return $list;
    }

    public function requestPrizes()
    {
        $response = $this->get(
            ['r', 'info', 'stats', 'prizes'],
            [],
            ['api-key' => config('connectors.key')]
        );
        foreach ($response['data'] as $field) {
            $list[] = [
                'id' => $field['id'],
                'name' => $field['name'],
                'count' => $field['count']
            ];
        }

        return $list;
    }

    public function checkPhone($data)
    {
        if (!isset($data['promo_language'])) {
            $data['promo_language'] = 'ru';
        }
        if (!isset($data['promo_app_type'])) {
            $data['promo_app_type'] = 'web';
        }

        $response = $this->post(
            ['w', 'users', 'signup'],
            [
                'promo_app_type'    => $data['promo_app_type'],
                'promo_language'    => $data['promo_language'],
                'user_phone'        => $data['user_phone'],
                'user_email'        => $data['user_email'],
                'user_firstname'    => $data['user_firstname'],
                'user_lastname'     => $data['user_lastname'],
                'user_middlename'   => $data['user_middlename'],
                'user_city'         => $data['user_city'],
                'user_age'          => $data['user_age'],
                'user_gender'       => $data['user_gender']
            ],
            [
                'api-key' => config('connectors.key'),
                'User-Language' => $data['promo_language'],
            ]
        );
        return $response;
    }
}
