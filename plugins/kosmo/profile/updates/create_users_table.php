<?php namespace Kosmo\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('kosmo_profile_users', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            //User fields
            $table->string('phone')->unique();
            $table->string('email')->unique();
            $table->string('name')->nullable();
            $table->string('shortname')->nullable();
            $table->string('password');
            $table->string('activation_code')->nullable()->index();
            $table->string('persist_code')->nullable();
            $table->string('reset_password_code')->nullable()->index();
            $table->text('permissions')->nullable();
            $table->boolean('is_activated')->default(0);
            $table->timestamp('activated_at')->nullable();
            $table->timestamp('last_login')->nullable();
            //GeoIP fields
            $table->string('ip')->nullable();
            $table->integer('country_id')->nullable()->unsigned();
            $table->integer('city_id')->nullable()->unsigned();
            //Quiz fields
            $table->timestamp('birthday')->nullable();
            $table->integer('scores')->nullable();
            $table->boolean('is_suitable')->default(0);
            $table->boolean('is_agree')->default(0);
            $table->boolean('is_winner')->default(0);
            //Socials
            $table->string('fb_token')->nullable()->index();
            $table->string('vk_token')->nullable()->index();
            //QIWI
            $table->string('qiwi_token')->nullable()->index();
            $table->boolean('is_qiwi_visited')->default(0);
            //Referral
            $table->integer('referrer_id')->nullable()->unsigned();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kosmo_profile_users');
    }

}
