<?php namespace Kosmo\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ChangeFieldsRafflesTable extends Migration
{

    public function up()
    {
        Schema::table('kosmo_profile_raffles', function ($table) {
            $table->dropColumn('status');
        });
        Schema::table('kosmo_profile_raffles', function ($table) {
            $table->string('ticket_number')->nullable();
            $table->string('user_fullname')->nullable();
            $table->enum('status', ['inactive', 'active'])->default('inactive');
        });
    }

    public function down()
    {
        Schema::table('kosmo_profile_raffles', function ($table) {
            $table->dropColumn('ticket_number');
            $table->dropColumn('user_fullname');
            $table->dropColumn('status');
        });
        Schema::table('kosmo_profile_raffles', function ($table) {
            $table->enum('status', ['empty', 'prepared', 'assigned']);
        });
    }

}
