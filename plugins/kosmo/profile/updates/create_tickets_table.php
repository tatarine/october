<?php namespace Kosmo\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTicketsTable extends Migration
{

    public function up()
    {
        Schema::create('kosmo_profile_tickets', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('number')->unique();
            $table->integer('user_id')->unsigned();
            $table->enum('status', ['inactive', 'active', 'used']);
            $table->enum('type', ['web', 'sms', 'qiwi']);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kosmo_profile_tickets');
    }

}
