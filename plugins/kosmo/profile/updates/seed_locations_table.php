<?php namespace Kosmo\Profile\Updates;

use October\Rain\Database\Updates\Seeder;
use Kosmo\Profile\Models\Location;

class SeedLocationsTable extends Seeder
{
    public function run()
    {
        /*
         * The countries and states table were previously seeded
         * by RainLab.User so this occurance is detected and halt.
         */
        if (Location::count() > 0) {
            return;
        }

        Location::insert([
            ['slug' => 'azerbaijan', 'name' => 'Азербайджан', 'level' => 1, 'type' => 'country', 'is_default' => 0],
            ['slug' => 'belarus', 'name' => 'Беларусь', 'level' => 1, 'type' => 'country', 'is_default' => 0],
            ['slug' => 'kazakhstan', 'name' => 'Казахстан', 'level' => 1, 'type' => 'country', 'is_default' => 1]
        ]);

        $az = Location::whereSlug('azerbaijan')->first();
        $az->setAttributeTranslated('name', 'Азәрбајҹан', 'az');
        $az->save();

        $cities = [
            ['name' => 'Агдам', 'translation' => 'Ağdam', 'level' => 2, 'type' => 'city'],
            ['name' => 'Агдаш', 'translation' => 'Ağdaş', 'level' => 2, 'type' => 'city'],
            ['name' => 'Агджабеди', 'translation' => 'Ağcabədi', 'level' => 2, 'type' => 'city'],
            ['name' => 'Аджигабул', 'translation' => 'Hacıqabul', 'level' => 2, 'type' => 'city'],
            ['name' => 'Акстафа', 'translation' => 'Ağstafa', 'level' => 2, 'type' => 'city'],
            ['name' => 'Астара', 'translation' => 'Astara', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ахсу', 'translation' => 'Ağsu', 'level' => 2, 'type' => 'city'],
            ['name' => 'Бабек', 'translation' => 'Babək', 'level' => 2, 'type' => 'city'],
            ['name' => 'Баку', 'translation' => 'Bakı', 'level' => 2, 'type' => 'city', 'is_default' => 1],
            ['name' => 'Барда', 'translation' => 'Bərdə', 'level' => 2, 'type' => 'city'],
            ['name' => 'Бейлаган', 'translation' => 'Beyləqan', 'level' => 2, 'type' => 'city'],
            ['name' => 'Белоканы', 'translation' => 'Balakən', 'level' => 2, 'type' => 'city'],
            ['name' => 'Билясувар', 'translation' => 'Biləsuvar', 'level' => 2, 'type' => 'city'],
            ['name' => 'Габала', 'translation' => 'Qəbələ', 'level' => 2, 'type' => 'city'],
            ['name' => 'Гёйгёль', 'translation' => 'Göygöl', 'level' => 2, 'type' => 'city'],
            ['name' => 'Геокчай', 'translation' => 'Göyçay', 'level' => 2, 'type' => 'city'],
            ['name' => 'Геранбой', 'translation' => 'Goranboy', 'level' => 2, 'type' => 'city'],
            ['name' => 'Гывраг', 'translation' => 'Gyvrag', 'level' => 2, 'type' => 'city'],
            ['name' => 'Гянджа', 'translation' => 'Gəncə', 'level' => 2, 'type' => 'city'],
            ['name' => 'Дашкесан', 'translation' => 'Daşkəsən', 'level' => 2, 'type' => 'city'],
            ['name' => 'Джалилабад', 'translation' => 'Cəlilabad', 'level' => 2, 'type' => 'city'],
            ['name' => 'Джебраил', 'translation' => 'Cəbrayıl', 'level' => 2, 'type' => 'city'],
            ['name' => 'Джульфа', 'translation' => 'Culfa', 'level' => 2, 'type' => 'city'],
            ['name' => 'Евлах', 'translation' => 'Yevlax', 'level' => 2, 'type' => 'city'],
            ['name' => 'Закаталы', 'translation' => 'Zaqatala', 'level' => 2, 'type' => 'city'],
            ['name' => 'Зангелан', 'translation' => 'Zəngilan', 'level' => 2, 'type' => 'city'],
            ['name' => 'Зердаб', 'translation' => 'Zərdab', 'level' => 2, 'type' => 'city'],
            ['name' => 'Имишли', 'translation' => 'İmişli', 'level' => 2, 'type' => 'city'],
            ['name' => 'Исмаиллы', 'translation' => 'İsmayıllı', 'level' => 2, 'type' => 'city'],
            ['name' => 'Казах', 'translation' => 'Qazax', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кахи', 'translation' => 'Qax', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кедабек', 'translation' => 'Gədəbəy', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кельбаджар', 'translation' => 'Kəlbəcər', 'level' => 2, 'type' => 'city'],
            ['name' => 'Куба', 'translation' => 'Quba', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кубатлы', 'translation' => 'Qubadlı', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кусары', 'translation' => 'Qusar', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кюрдамир', 'translation' => 'Kürdəmir', 'level' => 2, 'type' => 'city'],
            ['name' => 'Лачин', 'translation' => 'Laçın', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ленкорань', 'translation' => 'Lənkəran', 'level' => 2, 'type' => 'city'],
            ['name' => 'Лерик', 'translation' => 'Lerik', 'level' => 2, 'type' => 'city'],
            ['name' => 'Мараза', 'translation' => 'Mərəzə', 'level' => 2, 'type' => 'city'],
            ['name' => 'Масаллы', 'translation' => 'Masallı', 'level' => 2, 'type' => 'city'],
            ['name' => 'Нафталан', 'translation' => 'Naftalan', 'level' => 2, 'type' => 'city'],
            ['name' => 'Нахичевань', 'translation' => 'Naxçıvan', 'level' => 2, 'type' => 'city'],
            ['name' => 'Нефтечала', 'translation' => 'Neftçala', 'level' => 2, 'type' => 'city'],
            ['name' => 'Огуз', 'translation' => 'Oğuz', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ордубад', 'translation' => 'Ordubad', 'level' => 2, 'type' => 'city'],
            ['name' => 'Саатлы', 'translation' => 'Saatlı', 'level' => 2, 'type' => 'city'],
            ['name' => 'Сабирабад', 'translation' => 'Sabirabad', 'level' => 2, 'type' => 'city'],
            ['name' => 'Садарак', 'translation' => 'Sədərək', 'level' => 2, 'type' => 'city'],
            ['name' => 'Сальяны', 'translation' => 'Salyan', 'level' => 2, 'type' => 'city'],
            ['name' => 'Самух', 'translation' => 'Samux', 'level' => 2, 'type' => 'city'],
            ['name' => 'Сиазань', 'translation' => 'Siyəzən', 'level' => 2, 'type' => 'city'],
            ['name' => 'Тертер', 'translation' => 'Tərtər', 'level' => 2, 'type' => 'city'],
            ['name' => 'Товуз', 'translation' => 'Tovuz', 'level' => 2, 'type' => 'city'],
            ['name' => 'Уджары', 'translation' => 'Ucar', 'level' => 2, 'type' => 'city'],
            ['name' => 'Физули', 'translation' => 'Füzuli', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ханкенди', 'translation' => 'Xankəndi', 'level' => 2, 'type' => 'city'],
            ['name' => 'Хачмас', 'translation' => 'Xaçmaz', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ходжавенд', 'translation' => 'Xocavənd ', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ходжалы', 'translation' => 'Xocalı', 'level' => 2, 'type' => 'city'],
            ['name' => 'Хызы', 'translation' => 'Xızı', 'level' => 2, 'type' => 'city'],
            ['name' => 'Хырдалан', 'translation' => 'Xırdalan', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шабран', 'translation' => 'Şabran', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шамкир', 'translation' => 'Şəmkir', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шарур', 'translation' => 'Şərur', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шахбуз', 'translation' => 'Şahbuz', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шеки', 'translation' => 'Şəki', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шемаха', 'translation' => 'Şamaxı', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ширван', 'translation' => 'Şirvan', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шуша', 'translation' => 'Şuşa', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ярдымлы', 'translation' => 'Yardımlı', 'level' => 2, 'type' => 'city'],
            ['name' => 'Сумгаит', 'translation' => 'SUMQAYIT', 'level' => 2, 'type' => 'city'],
            ['name' => 'Мингечевир', 'translation' => 'MINGƏÇEVİR', 'level' => 2, 'type' => 'city'],
        ];

        foreach ($cities as $city) {
            $location = new Location;
            $location->name = $city['name'];
            $location->level = $city['level'];
            $location->type = $city['type'];
            if (isset($city['is_default']))
                $location->is_default = $city['is_default'];
            $location['location_id'] = $az->id;
            $location->save();

            $location->setAttributeTranslated('name', $city['translation'], 'az');
            $location->save();
        }

        $by = Location::whereSlug('belarus')->first();
        $by->children()->createMany([
            ['name' => 'Барановичи', 'level' => 2, 'type' => 'city'],
            ['name' => 'Белыничи', 'level' => 2, 'type' => 'city'],
            ['name' => 'Береза', 'level' => 2, 'type' => 'city'],
            ['name' => 'Березино', 'level' => 2, 'type' => 'city'],
            ['name' => 'Берестовица', 'level' => 2, 'type' => 'city'],
            ['name' => 'Бешенковичи', 'level' => 2, 'type' => 'city'],
            ['name' => 'Бобруйск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Борисов', 'level' => 2, 'type' => 'city'],
            ['name' => 'Брагин', 'level' => 2, 'type' => 'city'],
            ['name' => 'Браслав', 'level' => 2, 'type' => 'city'],
            ['name' => 'Брест', 'level' => 2, 'type' => 'city'],
            ['name' => 'Буда-Кошелево', 'level' => 2, 'type' => 'city'],
            ['name' => 'Быхов', 'level' => 2, 'type' => 'city'],
            ['name' => 'Верхнедвинск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ветка', 'level' => 2, 'type' => 'city'],
            ['name' => 'Вилейка', 'level' => 2, 'type' => 'city'],
            ['name' => 'Витебск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Волковыск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Воложин', 'level' => 2, 'type' => 'city'],
            ['name' => 'Вороново', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ганцевичи', 'level' => 2, 'type' => 'city'],
            ['name' => 'Глубокое', 'level' => 2, 'type' => 'city'],
            ['name' => 'Глуск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Гомель', 'level' => 2, 'type' => 'city'],
            ['name' => 'Горки', 'level' => 2, 'type' => 'city'],
            ['name' => 'Городок', 'level' => 2, 'type' => 'city'],
            ['name' => 'Гродно', 'level' => 2, 'type' => 'city'],
            ['name' => 'Дзержинск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Добруш', 'level' => 2, 'type' => 'city'],
            ['name' => 'Докшицы', 'level' => 2, 'type' => 'city'],
            ['name' => 'Дрибин', 'level' => 2, 'type' => 'city'],
            ['name' => 'Дрогичин', 'level' => 2, 'type' => 'city'],
            ['name' => 'Дубровно', 'level' => 2, 'type' => 'city'],
            ['name' => 'Дятлово', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ельск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Жабинка', 'level' => 2, 'type' => 'city'],
            ['name' => 'Житковичи', 'level' => 2, 'type' => 'city'],
            ['name' => 'Жлобин', 'level' => 2, 'type' => 'city'],
            ['name' => 'Жодино', 'level' => 2, 'type' => 'city'],
            ['name' => 'Зельва', 'level' => 2, 'type' => 'city'],
            ['name' => 'Иваново', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ивацевичи', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ивье', 'level' => 2, 'type' => 'city'],
            ['name' => 'Калинковичи', 'level' => 2, 'type' => 'city'],
            ['name' => 'Каменец', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кировск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Клецк', 'level' => 2, 'type' => 'city'],
            ['name' => 'Климовичи', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кличев', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кобрин', 'level' => 2, 'type' => 'city'],
            ['name' => 'Копыль', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кореличи', 'level' => 2, 'type' => 'city'],
            ['name' => 'Корма', 'level' => 2, 'type' => 'city'],
            ['name' => 'Костюковичи', 'level' => 2, 'type' => 'city'],
            ['name' => 'Краснополье', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кричев', 'level' => 2, 'type' => 'city'],
            ['name' => 'Круглое', 'level' => 2, 'type' => 'city'],
            ['name' => 'Крупки', 'level' => 2, 'type' => 'city'],
            ['name' => 'Лельчицы', 'level' => 2, 'type' => 'city'],
            ['name' => 'Лепель', 'level' => 2, 'type' => 'city'],
            ['name' => 'Лида', 'level' => 2, 'type' => 'city'],
            ['name' => 'Лиозно', 'level' => 2, 'type' => 'city'],
            ['name' => 'Логойск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Лоев', 'level' => 2, 'type' => 'city'],
            ['name' => 'Лунинец', 'level' => 2, 'type' => 'city'],
            ['name' => 'Любань', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ляховичи', 'level' => 2, 'type' => 'city'],
            ['name' => 'Малорита', 'level' => 2, 'type' => 'city'],
            ['name' => 'Марьина Горка', 'level' => 2, 'type' => 'city'],
            ['name' => 'Минск', 'level' => 2, 'type' => 'city', 'is_default' => 1],
            ['name' => 'Минский район', 'level' => 2, 'type' => 'city'],
            ['name' => 'Миоры', 'level' => 2, 'type' => 'city'],
            ['name' => 'Могилев', 'level' => 2, 'type' => 'city'],
            ['name' => 'Мозырь', 'level' => 2, 'type' => 'city'],
            ['name' => 'Молодечно', 'level' => 2, 'type' => 'city'],
            ['name' => 'Мосты', 'level' => 2, 'type' => 'city'],
            ['name' => 'Мстиславль', 'level' => 2, 'type' => 'city'],
            ['name' => 'Мядель', 'level' => 2, 'type' => 'city'],
            ['name' => 'Наровля', 'level' => 2, 'type' => 'city'],
            ['name' => 'Несвиж', 'level' => 2, 'type' => 'city'],
            ['name' => 'Новогрудок', 'level' => 2, 'type' => 'city'],
            ['name' => 'Новополоцк', 'level' => 2, 'type' => 'city'],
            ['name' => 'Октябрьский', 'level' => 2, 'type' => 'city'],
            ['name' => 'Орша', 'level' => 2, 'type' => 'city'],
            ['name' => 'Осиповичи', 'level' => 2, 'type' => 'city'],
            ['name' => 'Островец', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ошмяны', 'level' => 2, 'type' => 'city'],
            ['name' => 'Петриков', 'level' => 2, 'type' => 'city'],
            ['name' => 'Пинск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Плещеницы', 'level' => 2, 'type' => 'city'],
            ['name' => 'Полоцк', 'level' => 2, 'type' => 'city'],
            ['name' => 'Поставы', 'level' => 2, 'type' => 'city'],
            ['name' => 'Пружаны', 'level' => 2, 'type' => 'city'],
            ['name' => 'Пуховичи', 'level' => 2, 'type' => 'city'],
            ['name' => 'Речица', 'level' => 2, 'type' => 'city'],
            ['name' => 'Рогачев', 'level' => 2, 'type' => 'city'],
            ['name' => 'Россоны', 'level' => 2, 'type' => 'city'],
            ['name' => 'Светлогорск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Свислочь', 'level' => 2, 'type' => 'city'],
            ['name' => 'Сенно', 'level' => 2, 'type' => 'city'],
            ['name' => 'Славгород', 'level' => 2, 'type' => 'city'],
            ['name' => 'Слоним', 'level' => 2, 'type' => 'city'],
            ['name' => 'Слуцк', 'level' => 2, 'type' => 'city'],
            ['name' => 'Смолевичи', 'level' => 2, 'type' => 'city'],
            ['name' => 'Сморгонь', 'level' => 2, 'type' => 'city'],
            ['name' => 'Солигорск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Старые Дороги', 'level' => 2, 'type' => 'city'],
            ['name' => 'Столбцы', 'level' => 2, 'type' => 'city'],
            ['name' => 'Столин', 'level' => 2, 'type' => 'city'],
            ['name' => 'Толочин', 'level' => 2, 'type' => 'city'],
            ['name' => 'Узда', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ушачи', 'level' => 2, 'type' => 'city'],
            ['name' => 'Хойники', 'level' => 2, 'type' => 'city'],
            ['name' => 'Хотимск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Чаусы', 'level' => 2, 'type' => 'city'],
            ['name' => 'Чашники', 'level' => 2, 'type' => 'city'],
            ['name' => 'Червень', 'level' => 2, 'type' => 'city'],
            ['name' => 'Чериков', 'level' => 2, 'type' => 'city'],
            ['name' => 'Чечерск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шарковщина', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шклов', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шумилино', 'level' => 2, 'type' => 'city'],
            ['name' => 'Щучин', 'level' => 2, 'type' => 'city']
        ]);

        $kz = Location::whereSlug('kazakhstan')->first();
        $kz->setAttributeTranslated('name', 'Қазақстан', 'kz');
        $kz->save();

        $cities = [
            ['name' => 'Абай', 'translation' => 'Абай', 'level' => 2, 'type' => 'city'],
            ['name' => 'Акколь', 'translation' => 'Ақкөл', 'level' => 2, 'type' => 'city'],
            ['name' => 'Аксай', 'translation' => 'Ақсай', 'level' => 2, 'type' => 'city'],
            ['name' => 'Аксу', 'translation' => 'Ақсу', 'level' => 2, 'type' => 'city'],
            ['name' => 'Актау', 'translation' => 'Ақтау', 'level' => 2, 'type' => 'city'],
            ['name' => 'Актобе', 'translation' => 'Ақтөбе', 'level' => 2, 'type' => 'city'],
            ['name' => 'Алга', 'translation' => 'Алға', 'level' => 2, 'type' => 'city'],
            ['name' => 'Алматы', 'translation' => 'Алматы', 'level' => 2, 'type' => 'city', 'is_default' => 1],
            ['name' => 'Аральск', 'translation' => 'Арал', 'level' => 2, 'type' => 'city'],
            ['name' => 'Аркалык', 'translation' => 'Арқалық', 'level' => 2, 'type' => 'city'],
            ['name' => 'Арысь', 'translation' => 'Арыс', 'level' => 2, 'type' => 'city'],
            ['name' => 'Астана', 'translation' => 'Астана', 'level' => 2, 'type' => 'city'],
            ['name' => 'Атбасар', 'translation' => 'Атбасар', 'level' => 2, 'type' => 'city'],
            ['name' => 'Атырау', 'translation' => 'Атырау', 'level' => 2, 'type' => 'city'],
            ['name' => 'Аягоз', 'translation' => 'Аягөз', 'level' => 2, 'type' => 'city'],
            ['name' => 'Байконыр', 'translation' => 'Байқоңыр', 'level' => 2, 'type' => 'city'],
            ['name' => 'Балхаш', 'translation' => 'Балқаш', 'level' => 2, 'type' => 'city'],
            ['name' => 'Булаево', 'translation' => 'Булаев', 'level' => 2, 'type' => 'city'],
            ['name' => 'Державинск', 'translation' => 'Державин', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ерейментау', 'translation' => 'Ерейментау', 'level' => 2, 'type' => 'city'],
            ['name' => 'Есик', 'translation' => 'Есік', 'level' => 2, 'type' => 'city'],
            ['name' => 'Есиль', 'translation' => 'Есіл', 'level' => 2, 'type' => 'city'],
            ['name' => 'Жанаозен', 'translation' => 'Жаңаөзен', 'level' => 2, 'type' => 'city'],
            ['name' => 'Жанатас', 'translation' => 'Жаңатас', 'level' => 2, 'type' => 'city'],
            ['name' => 'Жаркент', 'translation' => 'Жаркент', 'level' => 2, 'type' => 'city'],
            ['name' => 'Жезказган', 'translation' => 'Жезқазған', 'level' => 2, 'type' => 'city'],
            ['name' => 'Жем', 'translation' => 'Жем', 'level' => 2, 'type' => 'city'],
            ['name' => 'Жетысай', 'translation' => 'Жетісай', 'level' => 2, 'type' => 'city'],
            ['name' => 'Житикара', 'translation' => 'Жетіқара', 'level' => 2, 'type' => 'city'],
            ['name' => 'Зайсан', 'translation' => 'Зайсаң', 'level' => 2, 'type' => 'city'],
            ['name' => 'Зыряновск', 'translation' => 'Зыряновск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Казалинск', 'translation' => 'Қазалы', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кандыагаш', 'translation' => 'Қандыағаш', 'level' => 2, 'type' => 'city'],
            ['name' => 'Капчагай', 'translation' => 'Қапшағай', 'level' => 2, 'type' => 'city'],
            ['name' => 'Караганда', 'translation' => 'Қарағанды', 'level' => 2, 'type' => 'city'],
            ['name' => 'Каражал', 'translation' => 'Қаражал', 'level' => 2, 'type' => 'city'],
            ['name' => 'Каратау', 'translation' => 'Қаратау', 'level' => 2, 'type' => 'city'],
            ['name' => 'Каркаралинск', 'translation' => 'Қарқаралы', 'level' => 2, 'type' => 'city'],
            ['name' => 'Каскелен', 'translation' => 'Қаскелең', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кентау', 'translation' => 'Кентау', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кокшетау', 'translation' => 'Көкшетау', 'level' => 2, 'type' => 'city'],
            ['name' => 'Костанай', 'translation' => 'Қостанай', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кульсары', 'translation' => 'Құлсары', 'level' => 2, 'type' => 'city'],
            ['name' => 'Курчатов', 'translation' => 'Курчатов', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кызылорда', 'translation' => 'Қызылорда', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ленгер', 'translation' => 'Ленгер', 'level' => 2, 'type' => 'city'],
            ['name' => 'Лисаковск', 'translation' => 'Лисаковск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Макинск', 'translation' => 'Макинск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Мамлютка', 'translation' => 'Мамлют', 'level' => 2, 'type' => 'city'],
            ['name' => 'Павлодар', 'translation' => 'Павлодар', 'level' => 2, 'type' => 'city'],
            ['name' => 'Петропавловск', 'translation' => 'Петропавл', 'level' => 2, 'type' => 'city'],
            ['name' => 'Приозёрск', 'translation' => 'Приозер', 'level' => 2, 'type' => 'city'],
            ['name' => 'Риддер', 'translation' => 'Риддер', 'level' => 2, 'type' => 'city'],
            ['name' => 'Рудный', 'translation' => 'Рудный', 'level' => 2, 'type' => 'city'],
            ['name' => 'Сарань', 'translation' => 'Саран', 'level' => 2, 'type' => 'city'],
            ['name' => 'Сарканд', 'translation' => 'Сарқант', 'level' => 2, 'type' => 'city'],
            ['name' => 'Сарыагаш', 'translation' => 'Сарыағаш', 'level' => 2, 'type' => 'city'],
            ['name' => 'Сатпаев', 'translation' => 'Сәтбаев', 'level' => 2, 'type' => 'city'],
            ['name' => 'Семей', 'translation' => 'Семей', 'level' => 2, 'type' => 'city'],
            ['name' => 'Сергеевка', 'translation' => 'Сергеев', 'level' => 2, 'type' => 'city'],
            ['name' => 'Серебрянск', 'translation' => 'Серебрянск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Степногорск', 'translation' => 'Степногор', 'level' => 2, 'type' => 'city'],
            ['name' => 'Степняк', 'translation' => 'Степняк', 'level' => 2, 'type' => 'city'],
            ['name' => 'Тайынша', 'translation' => 'Тайынша', 'level' => 2, 'type' => 'city'],
            ['name' => 'Талгар', 'translation' => 'Талғар', 'level' => 2, 'type' => 'city'],
            ['name' => 'Талдыкорган', 'translation' => 'Талдықорған', 'level' => 2, 'type' => 'city'],
            ['name' => 'Тараз', 'translation' => 'Тараз', 'level' => 2, 'type' => 'city'],
            ['name' => 'Текели', 'translation' => 'Текелі', 'level' => 2, 'type' => 'city'],
            ['name' => 'Темир', 'translation' => 'Темір', 'level' => 2, 'type' => 'city'],
            ['name' => 'Темиртау', 'translation' => 'Теміртау', 'level' => 2, 'type' => 'city'],
            ['name' => 'Туркестан', 'translation' => 'Түркістан', 'level' => 2, 'type' => 'city'],
            ['name' => 'Уральск', 'translation' => 'Орал', 'level' => 2, 'type' => 'city'],
            ['name' => 'Усть-Каменогорск', 'translation' => 'Өскемен', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ушарал', 'translation' => 'Үшарал', 'level' => 2, 'type' => 'city'],
            ['name' => 'Уштобе', 'translation' => 'Үштөбе', 'level' => 2, 'type' => 'city'],
            ['name' => 'Форт-Шевченко', 'translation' => 'Форт-Шевченко', 'level' => 2, 'type' => 'city'],
            ['name' => 'Хромтау', 'translation' => 'Хромтау', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шардара', 'translation' => 'Шардара', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шалкар', 'translation' => 'Шалқар', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шар', 'translation' => 'Шар', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шахтинск', 'translation' => 'Шахтинск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шемонаиха', 'translation' => 'Шемонаиха', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шу', 'translation' => 'Шу', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шымкент', 'translation' => 'Шымкент', 'level' => 2, 'type' => 'city'],
            ['name' => 'Щучинск', 'translation' => 'Щучинск', 'level' => 2, 'type' => 'city'],
            ['name' => 'Экибастуз', 'translation' => 'Екібастұз', 'level' => 2, 'type' => 'city'],
            ['name' => 'Эмба', 'translation' => 'Эмбі', 'level' => 2, 'type' => 'city']
        ];

        foreach ($cities as $city) {
            $location = new Location;
            $location->name = $city['name'];
            $location->level = $city['level'];
            $location->type = $city['type'];
            if (isset($city['is_default']))
                $location->is_default = $city['is_default'];
            $location['location_id'] = $kz->id;
            $location->save();

            $location->setAttributeTranslated('name', $city['translation'], 'kz');
            $location->save();
        }
    }
}
