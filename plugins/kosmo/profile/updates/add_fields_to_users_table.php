<?php namespace Kosmo\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddFieldsToUsersTable extends Migration
{

    public function up()
    {
        Schema::table('kosmo_profile_users', function ($table) {
            $table->string('surname')->nullable();
            $table->string('middlename')->nullable();
            $table->integer('state_id')->nullable()->unsigned();
            $table->string('state_name')->nullable();
            $table->integer('district_id')->nullable()->unsigned();
            $table->string('district_name')->nullable();
            $table->string('street')->nullable();
            $table->string('zip')->nullable();
            $table->string('house')->nullable();
            $table->string('apartment')->nullable();
            $table->integer('age')->nullable();
            $table->string('gender')->nullable();
        });
    }

    public function down()
    {
        Schema::table('kosmo_profile_users', function ($table) {
            $table->dropColumn('surname');
            $table->dropColumn('middlename');
            $table->dropColumn('state_id');
            $table->dropColumn('state_name');
            $table->dropColumn('district_id');
            $table->dropColumn('district_name');
            $table->dropColumn('street');
            $table->dropColumn('zip');
            $table->dropColumn('house');
            $table->dropColumn('apartment');
            $table->dropColumn('age');
            $table->dropColumn('gender');
        });
    }

}
