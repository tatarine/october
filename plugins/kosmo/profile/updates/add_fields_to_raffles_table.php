<?php namespace Kosmo\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddFieldsToRafflesTable extends Migration
{

    public function up()
    {
        Schema::table('kosmo_profile_raffles', function ($table) {
            $table->string('user_email')->nullable()->after('user_fullname');
            $table->string('user_phone')->nullable()->after('user_fullname');
            $table->text('user_city')->nullable()->after('user_fullname');
            $table->integer('winning_id')->nullable()->unsigned()->index()->after('prize_id');
        });
    }

    public function down()
    {
        Schema::table('kosmo_profile_raffles', function ($table) {
            $table->dropColumn('user_email');
            $table->dropColumn('user_phone');
            $table->dropColumn('user_city');
            $table->dropColumn('winning_id');
        });
    }

}
