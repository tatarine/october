<?php namespace Kosmo\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateRafflesTable extends Migration
{

    public function up()
    {
        Schema::create('kosmo_profile_raffles', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('winner_id')->unsigned()->nullable()->index();
            $table->integer('ticket_id')->unsigned()->nullable()->index();
            $table->integer('prize_id')->unsigned()->nullable()->index();
            $table->enum('status', ['empty', 'prepared', 'assigned']);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kosmo_profile_raffles');
    }

}
