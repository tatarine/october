<?php namespace Kosmo\Profile\Updates;

use October\Rain\Database\Updates\Seeder;
use Kosmo\Profile\Models\Location;

class SeedKgLocationsTable extends Seeder
{
    public function run()
    {

        Location::insert([
            ['slug' => 'kyrgyzstan', 'name' => 'Киргизия', 'level' => 1, 'type' => 'country', 'is_default' => 0],
        ]);

        $kg = Location::whereSlug('kyrgyzstan')->first();
        $kg->setAttributeTranslated('name', 'Кыргызстан', 'kg');
        $kg->save();

        $cities = [
            ['name' => 'Айдаркен', 'translation' => 'Айдаркан', 'level' => 2, 'type' => 'city'],
            ['name' => 'Балыкчы', 'translation' => 'Балыкчы', 'level' => 2, 'type' => 'city'],
            ['name' => 'Баткен', 'translation' => 'Баткен', 'level' => 2, 'type' => 'city'],
            ['name' => 'Бишкек', 'translation' => 'Бишкек', 'level' => 2, 'type' => 'city', 'is_default' => 1],
            ['name' => 'Джалал-Абад', 'translation' => 'Жалалабат', 'level' => 2, 'type' => 'city'],
            ['name' => 'Исфана', 'translation' => 'Исфана', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кадамжай', 'translation' => 'Кадамжай', 'level' => 2, 'type' => 'city'],
            ['name' => 'Каинды', 'translation' => 'Кайыңды', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кант', 'translation' => 'Кант', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кара-Балта', 'translation' => 'Карабалта', 'level' => 2, 'type' => 'city'],
            ['name' => 'Каракол', 'translation' => 'Каракол', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кара-Куль', 'translation' => 'Каракөл', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кара-Суу', 'translation' => 'Карасуу', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кемин', 'translation' => 'Кемин', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кербен', 'translation' => 'Кербен', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кок-Джангак', 'translation' => 'Кок-Жангак', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кочкор-Ата', 'translation' => 'Кочкор-Ата', 'level' => 2, 'type' => 'city'],
            ['name' => 'Кызыл-Кия', 'translation' => 'Кызылкыя', 'level' => 2, 'type' => 'city'],
            ['name' => 'Майлуу-Суу', 'translation' => 'Майлуу-Суу', 'level' => 2, 'type' => 'city'],
            ['name' => 'Нарын', 'translation' => 'Нарын', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ноокат', 'translation' => 'Ноокат', 'level' => 2, 'type' => 'city'],
            ['name' => 'Орловка', 'translation' => 'Орловка', 'level' => 2, 'type' => 'city'],
            ['name' => 'Ош', 'translation' => 'Ош', 'level' => 2, 'type' => 'city'],
            ['name' => 'Сулюкта', 'translation' => 'Сүлүктү', 'level' => 2, 'type' => 'city'],
            ['name' => 'Талас', 'translation' => 'Талас', 'level' => 2, 'type' => 'city'],
            ['name' => 'Таш-Кумыр', 'translation' => 'Ташкөмүр', 'level' => 2, 'type' => 'city'],
            ['name' => 'Токмок', 'translation' => 'Токмок', 'level' => 2, 'type' => 'city'],
            ['name' => 'Токтогул', 'translation' => 'Токтогул', 'level' => 2, 'type' => 'city'],
            ['name' => 'Узген', 'translation' => 'Өзгөн', 'level' => 2, 'type' => 'city'],
            ['name' => 'Чолпон-Ата', 'translation' => 'Чолпоната', 'level' => 2, 'type' => 'city'],
            ['name' => 'Шопоков', 'translation' => 'Шопоков', 'level' => 2, 'type' => 'city']
        ];

        foreach ($cities as $city) {
            $location = new Location;
            $location->name = $city['name'];
            $location->level = $city['level'];
            $location->type = $city['type'];
            if (isset($city['is_default']))
                $location->is_default = $city['is_default'];
            $location['location_id'] = $kg->id;
            $location->save();

            $location->setAttributeTranslated('name', $city['translation'], 'kg');
            $location->save();
        }
    }
}
