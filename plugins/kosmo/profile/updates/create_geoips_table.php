<?php namespace Kosmo\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateGeoipsTable extends Migration
{

    public function up()
    {
        Schema::create('kosmo_profile_geoips', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('ip')->unique();
            $table->integer('country_id')->nullable()->unsigned();
            $table->integer('city_id')->nullable()->unsigned();
            $table->json('response')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kosmo_profile_geoips');
    }

}
