<?php namespace Kosmo\Profile\Updates;

use DB;
use October\Rain\Database\Updates\Seeder;

class SeedProfileMailTemplatesTable extends Seeder
{
    public function run()
    {
        DB::table('system_mail_templates')->where('code', 'like', 'kosmo.profile.mail.restore')
            ->delete();
        DB::table('system_mail_templates')->where('code', 'like', 'kosmo.profile.mail.confirm')
            ->delete();
        DB::table('system_mail_templates')->where('code', 'like', 'kosmo.profile.mail.mailing')
            ->delete();

        DB::table('system_mail_templates')->insert([
            [
                'code' => 'kosmo.profile.mail.restore',
                'subject' => 'Restore access',
                'description' => 'Restore access template',
                'content_html' => '<p>​Слышали, ты забыл пароль. Перейди по ссылке {{link}} и измени пароль.</p>',
                'content_text' => '​Слышали, ты забыл пароль. Перейди по ссылке {{link}} и измени пароль.',
                'layout_id' => 1,
                'is_custom' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],[
                'code' => 'kosmo.profile.mail.confirm',
                'subject' => 'Confirm email',
                'description' => 'Email confirmation template',
                'content_html' => '<p>Пожалуйста, пройди по следующей ссылке, чтобы подтвердить свой ящик.</p><p>{{link}}</p><p>{{code}}</p>',
                'content_text' => '​<p>Пожалуйста, пройди по следующей ссылке, чтобы подтвердить свой ящик. {{link}} {{code}}',
                'layout_id' => 1,
                'is_custom' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],[
                'code' => 'kosmo.profile.mail.mailing',
                'subject' => 'Mailing',
                'description' => 'Mailing template',
                'content_html' => '<p>Переходи по ссылке {{link}}</p>',
                'content_text' => '​<p>Переходи по ссылке {{link}}',
                'layout_id' => 1,
                'is_custom' => 1,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]
        ]);

    }
}
