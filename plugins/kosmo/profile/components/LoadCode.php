<?php namespace Kosmo\Profile\Components;

use Log;
use Redirect;
use Validator;
use Carbon\Carbon;
use ValidationException;
use ApplicationException;
use Illuminate\Database\QueryException;

use Cms\Classes\ComponentBase;
use RainLab\Translate\Classes\Translator;

use Kosmo\Quiz\Models\Prize;
use Kosmo\Quiz\Classes\ScheduleHelper;
use Kosmo\Profile\Models\User;
use Kosmo\Profile\Models\Ticket;
use Kosmo\Profile\Models\Transaction;
use Kosmo\Profile\Classes\Recaptcha;
use Kosmo\Profile\Classes\AuthManager;
use Kosmo\Quiz\Classes\RaffleAchieved as Raffle;
use Kosmo\Profile\Connectors\LoadCodeService as LCS;

class LoadCode extends ComponentBase
{
    protected $authManager;

    public function componentDetails()
    {
        return [
            'name'        => 'Load Code Component',
            'description' => 'Load code component'
        ];
    }

    public function defineProperties()
    {
        return [
            'autoload' => [
                'title'       => 'Autoload',
                'description' => 'Autoload user codes',
                'type'        => 'checkbox',
                'default'     => 0
            ]
        ];
    }

    public function init()
    {
        $this->authManager = AuthManager::instance();
    }

    public function onTest()
    {}

    public function onRun()
    {
        if ($this->property('autoload')) {
            if (env('USE_RGL')) {
                $this->onRequestUserCodes();
                if ($user = $this->user()) {
                    $this->page['user'] = $user;
                }
            }
        }
        $this->page['prizes'] = Prize::achieved()->get();
        $this->page['captchaKey'] = Recaptcha::instance()->captchaKey();
    }

    protected function pushPartial()
    {
        $post = post();
        $result = [];
        $result['#'.$post['target']] = $this->renderPartial($post['partial']);
        return $result;
    }

    public function onCheckPrizes()
    {
        if (env('USE_RGL')) {
            $this->onRequestPrizes();
        }
        $this->page['prizes'] = Prize::achieved()->get();
        return $this->pushPartial();
    }

    // public function onRequestAddPrize()
    // {
    //     if (!$user = $this->user()) {
    //         return Redirect::to('/');
    //         // throw new ApplicationException('Invalid request');
    //     }
    //     $post = post();
    //     if(!$prize = Prize::find($post['prize'])) {
    //         throw new ApplicationException('Invalid request');
    //     }
    //
    //     $data = [];
    //     $data['prizes'] = [
    //         [
    //             'id' => $prize->rgl_id,
    //             'quantity' => 1
    //         ]
    //     ];
    //     $data['promo_language'] = Translator::instance()->getLocale();
    //
    //     if (env('USE_RGL')) {
    //         $phone    = $user->getPhoneWithPreffix(false);
    //         $response = LCS::instance()->addPrize($phone, $data);
    //     } else {
    //         $response = [
    //             'code' => 200,
    //             'data' => [
    //                 'prize_register_id' => '8',
    //                 'points_before' => 4,
    //                 'points_after' => 0,
    //                 'rating' => '4',
    //                 'points' => 0,
    //                 'message_id' => ['PRIZE_SHIPPING_ACCEPTED'],
    //                 'transaction_id' => '58c5513a9ffad'
    //             ]
    //         ];
    //     }
    //     if (isset($response['code']) && $response['code'] == 200) {
            // if(isset($response['data'])) {
            //     if(isset($response['data']['prize_register_id'])) {
            //         $data = [];
            //         $data['prize'] = $prize;
            //         $data['tickets_count'] = $prize->scores;
            //         $data['rgl_id'] = $response['data']['prize_register_id'];
            //
            //         if ($tickets = $user->tickets()->active()->limit($prize->scores)->get()) {
            //             $data['ticket'] = $tickets->last();
            //             if ($winning = Raffle::instance()->play($user, $data)) {
            //                 if ($tickets->count() == $prize->scores) {
            //                     foreach ($tickets as $ticket) {
            //                         $ticket->setUsed();
            //                     }
            //                 }
            //             }
            //         }
            //
            //         $this->page['user'] = $user;
            //         $this->page['prize'] = $prize;
            //         $this->page['winning'] = $winning;
            //         $this->page['showOnLoad'] = true;
            //         return [
            //             '#modals' => $this->renderPartial('modals/prize-confirm-success')
            //         ];
            //     }
            // }
    //     }
    //     $this->page['showOnLoad'] = true;
    //     return [
    //         '#modals' => $this->renderPartial('modals/prize-confirm-error')
    //     ];
    // }

    public function onLoadCode()
    {
        if (!$user = $this->user()) {
            return Redirect::to('/');
            // throw new ApplicationException('Invalid request');
        }
        if ($main = ScheduleHelper::instance()->getMainSchedule()) {
            if ($main->isPast()) {
                return Redirect::to('/');
            }
        }
        $post = post();
        $rules = [
            'code' => 'required|unique:kosmo_profile_tickets,number|size:9',
            'g-recaptcha-response' => 'required',
        ];

        if (!Recaptcha::instance()->check($post['g-recaptcha-response'])) {
            throw new ApplicationException('captcha');
        }

        $validation = Validator::make($post, $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        if (!$transaction = Transaction::byUser($user->id)->byCode($post['code'])->first()) {
            try {
                $transaction = new Transaction;
                $transaction->code = strtoupper($post['code']);
                $transaction->user_id = $user->id;
                $transaction->save();
            } catch (QueryException $e) {
                throw new ApplicationException('Invalid transaction');
            }
        }
        $now = Carbon::now();

        $data = [];
        $data['transaction_id']     = $transaction->hash;
        $data['promo_code']         = strtoupper($post['code']);
        $data['user_email']         = $user->email;
        $data['user_firstname']     = $user->name;
        $data['user_lastname']      = ($user->surname?$user->surname:'Фамилия');
        $data['user_middlename']    = ($user->middlename?$user->middlename:'');
        $data['user_city']          = $user->city->name;
        $data['user_age']           = $user->age;
        $data['user_gender']        = $user->gender;
        $data['promo_language']     = Translator::instance()->getLocale();
        $data['promo_time']         = env('USE_REAL_TIME')? $now->format('d.m.Y H:i:s'): '21.06.2017 10:00:00';
        // Log::error(json_encode($data, JSON_UNESCAPED_UNICODE));

        if (env('USE_RGL')) {
            $phone    = $user->getPhoneWithPreffix(false);
            $response = LCS::instance()->loadCode($phone, $data);
        } else {
            $response = [
                'code' => 200,
                'data' => [
                    'transaction_id' => $transaction->hash,
                    'code' => strtoupper($post['code'])
                ]
            ];
        }

        if (isset($response['code']) && $response['code'] == 200) {
            try {
                $ticket = new Ticket;
                $ticket->number = strtoupper($response['data']['code']);
                $ticket->user_id = $user->id;
                $ticket->created_at = $now;
                $ticket->save();
                $this->page['showModal'] = 'thanks';
            } catch (QueryException $e) {
                throw new ApplicationException('Invalid query');
            }
            if (isset($response['data']['prizes']) && !empty($response['data']['prizes'])) {
                foreach ($response['data']['prizes'] as $dataPrize) {
                    if (!$prize = Prize::byRglId($dataPrize['id'])->first()) {
                        continue;
                    }
                    $data                   = [];
                    $data['prize']          = $prize;
                    $data['rgl_id']         = $dataPrize['id'];
                    $data['ticket']         = $ticket;
                    $data['tickets_count']  = 1;
                    if ($winning = Raffle::instance()->play($user, $data)) {
                        $ticket->setUsed();
                        $this->page['showModal'] = 'congrats';
                    }
                }
            }
            if(isset($response['data'])){
                if (isset($response['data']['transaction_id'])) {
                    if ($transaction = Transaction::byHash($response['data']['transaction_id'])->first()) {
                        $transaction->delete();
                    }
                }
            }
        }

        return;
    }

    public function onRequestProfile()
    {
        if (!$user = $this->user()) {
            return Redirect::to('/');
        }
        try {
            $response = LCS::instance()->requestUserProfile($user->getPhoneWithPreffix(false));
        } catch (ApplicationException $e) {
            return;
        }
        return;
    }

    public function onRequestUserCodes()
    {
        if (!$user = $this->user()) {
            return Redirect::to('/');
        }
        try {
            $list = LCS::instance()->requestUserCodes($user->getPhoneWithPreffix(false));
        } catch (ApplicationException $e) {
            return;
        }
        $tickets = $user->tickets->lists('number');
        if (!is_null($tickets) && !is_array($tickets)) {
            $tickets = [$tickets];
        }
        foreach ($list as $field) {
            if (!is_null($tickets)) {
                if (isset($field['code']) && in_array($field['code'], $tickets)) {
                    continue;
                }
            }
            try {
                $ticket = new Ticket;
                $ticket->number = $field['code'];
                $ticket->user_id = $user->id;
                $ticket->type = $field['type'];
                $ticket->created_at = Carbon::createFromFormat('d.m.Y H:i:s', $field['datetime']);
                $ticket->save();
            } catch (QueryException $e) {
                continue;
            }
            if (!$prize = Prize::byRglId($field['prize_id'])->first()) {
                continue;
            }
            $data                   = [];
            $data['prize']          = $prize;
            $data['rgl_id']         = $field['prize_id'];
            $data['ticket']         = $ticket;
            $data['tickets_count']  = 1;
            if ($winning = Raffle::instance()->play($user, $data)) {
                $ticket->setUsed();
            }
        }
        return;
    }

    public function onRequestPrizes()
    {
        try{
            $list = LCS::instance()->requestPrizes();
        }catch(ApplicationException $e){
            return;
        }
        foreach ($list as $field) {
            if ($prize = Prize::byRglId($field['id'])->first()) {
                $prize->unavailable = $field['count'];
                $prize->save();
            }
        }
    }
    /**
     * Returns the logged in user, if available
     */
    public function user()
    {
        if (!$this->authManager->check()) {
            return null;
        }

        return $this->authManager->getUser();
    }
}
