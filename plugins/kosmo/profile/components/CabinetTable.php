<?php namespace Kosmo\Profile\Components;

use Cms\Classes\ComponentBase;
use Kosmo\Profile\Classes\AuthManager;

class CabinetTable extends ComponentBase
{
    protected $authManager;

    public function componentDetails()
    {
        return [
            'name'        => 'CabinetTable Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'size' => [
                'title'       => 'Page Size',
                'description' => 'Page size property',
                'type'        => 'string',
                'default'     => '10'
            ],
            'page' => [
                'title'       => 'Page Number',
                'description' => 'Page number property',
                'type'        => 'string',
                'default'     => '1'
            ],
        ];
    }

    public function init()
    {
        $this->authManager = AuthManager::instance();
    }

    public function onRun()
    {
        $this->page['wtickets'] = $this->getTickets()->get();
        $this->page['tickets'] = $this->getTicketsPerPage();
        $this->page['pagination'] = $this->getTicketsPerPage();
    }

    public function onPaginateTickets()
    {
        $post = post();
        if (!$page = $post['page']) {
            return;
        }
        if ($page == 1) {
            $this->page['wtickets'] = $this->getTickets()->get();
        }
        $this->page['tickets'] = $this->getTicketsPerPage($page);
        $this->page['pagination'] = $this->getTicketsPerPage($page);

        $result = [];
        $result['#'.$post['paginateTarget']] = $this->renderPartial($post['paginatePartial']);
        $result['#'.$post['paginateId']] = $this->renderPartial($post['paginateTemplate']);
        return $result;
    }

    protected function getTicketsPerPage($page = null, $size = null)
    {
        if (!$query = $this->getUserTickets()) {
            return;
        }
        if (!$page) {
            $page = $this->property('page');
        }
        if (!$size) {
            $size = $this->property('size');
        }
        $winners = $query->paginate($size, $page);
        return $winners;
    }

    protected function getUserTickets()
    {
        if (!$user = $this->user()) {
            return;
        }
        return $user->tickets()->doesntHave('winning')->orderBy('created_at', 'desc');
    }

    protected function getTickets()
    {
        if (!$user = $this->user()) {
            return;
        }
        return $user->tickets()->has('winning')->orderBy('created_at', 'desc');
    }

    /**
     * Returns the logged in user, if available
     */
    public function user()
    {
        if (!$this->authManager->check()) {
            return null;
        }

        return $this->authManager->getUser();
    }
}
