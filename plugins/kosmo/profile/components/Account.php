<?php namespace Kosmo\Profile\Components;

use Lang;
use Mail;
use Flash;
use Input;
use Request;
use Redirect;
use Session as SessionService;
use Validator;
use Carbon\Carbon;
use ValidationException;
use ApplicationException;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Kosmo\Profile\Models\User as UserModel;
use Kosmo\Profile\Models\Geoip as GeoipModel;
use Kosmo\Profile\Classes\FB;
use Kosmo\Profile\Classes\VK;
use Kosmo\Profile\Classes\AuthManager;
use Kosmo\Quiz\Models\Prize;
use Kosmo\Quiz\Classes\Notifier;
use Kosmo\Quiz\Classes\ScheduleHelper;
use Kosmo\Profile\Connectors\LoadCodeService as LCS;
use Kosmo\Profile\Models\Location;
use RainLab\Translate\Classes\Translator;
use Kosmo\Profile\Classes\FormatHelper;
use Exception;

class Account extends ComponentBase
{
    protected $authManager;
    protected $socialManager;

    public function componentDetails()
    {
        return [
            'name'        => 'kosmo.profile::lang.account.account',
            'description' => 'kosmo.profile::lang.account.account_desc'
        ];
    }

    public function defineProperties()
    {
        return [
            'redirect' => [
                'title'       => 'kosmo.profile::lang.account.redirect_to',
                'description' => 'kosmo.profile::lang.account.redirect_to_desc',
                'type'        => 'dropdown',
                'default'     => ''
            ],
            'greetings' => [
                'title'       => 'kosmo.profile::lang.account.redirect_to',
                'description' => 'kosmo.profile::lang.account.redirect_to_desc',
                'type'        => 'dropdown',
                'default'     => ''
            ],
            'paramCode' => [
                'title'       => 'kosmo.profile::lang.account.code_param',
                'description' => 'kosmo.profile::lang.account.code_param_desc',
                'type'        => 'string',
                'default'     => 'code'
            ],
            'callback' => [
                'title'       => 'Callback',
                'description' => 'Switch on if need callback',
                'type'        => 'checkbox',
                'default'     => ''
            ]
        ];
    }

    public function getRedirectOptions()
    {
        return [''=>'- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function getGreetingsOptions()
    {
        return [''=>'- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function init()
    {
        $this->authManager = AuthManager::instance();
        $this->socialManager = [
            // 'fb' => FB::instance(),
            // 'vk' => VK::instance(),
        ];
    }

    public function onRun()
    {
        $routeParameter = $this->property('paramCode');

        /*
         * Callback
         */
        if ($this->property('callback')) {
            $callback = $this->onCallback();
            if ($callback) {
                return $callback;
            }
            return Redirect::to('signin');
        }

        /*
         * Login attributes
         */
        $this->page['user'] = $this->user();
        $this->page['loginAttribute'] = $this->loginAttribute();
        $this->page['loginAttributeLabel'] = $this->loginAttributeLabel();

        foreach ($this->socialManager as $key => $value) {
            $this->page[$key.'Url'] = $this->socialManager[$key]->getLoginUrl();
        }

        /*
         * Phone mask
         */
        $this->page['phoneMask'] = FormatHelper::phoneMask();
    }

    public function user()
    {
        if (!$this->authManager->check())
            return null;

        return $this->authManager->getUser();
    }

    public function loginAttribute()
    {
        return UserModel::$loginAttribute;
    }

    public function loginAttributeLabel()
    {
        return UserModel::$loginAttributeLabel;
    }

    public function onSignin()
    {
        /*
         * Validate input
         */
        $data = post();

        if (array_key_exists('phone', $data) && array_key_exists('phoneCode', $data)) {
            $data['phone'] = $data['phoneCode'].$data['phone'];
            unset($data['phoneCode']);
        }

        if (!array_key_exists('login', $data)) {
            $data['login'] = str_replace(' ', '', $data['phone']);
            unset($data['phone']);
        }

        $rules = [
            'password' => 'required|between:4,16'
        ];

        if (in_array(env('APP_ENV'), ['kz', 'dev', 'prod'])) {
            $rules['login'] = 'required|numeric|digits:10';
        }
        if (in_array(env('APP_ENV'), ['az', 'by', 'kg'])) {
            $rules['login'] = 'required|numeric|digits:9';
        }

        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        /*
         * Authenticate user
         */
        $user = $this->authManager->authenticate([
            'login' => array_get($data, 'login'),
            'password' => array_get($data, 'password')
        ], true);

        /*
         * Birthday
         */
        if (!$user->birthday) {
            if (SessionService::has('birthday')) {
                $birthday = SessionService::get('birthday');
                if (!empty($birthday) && is_array($birthday)) {
                    $user->birthday = Carbon::parse($birthday['year'].'-'.$birthday['month'].'-'.$birthday['day']);
                    $user->save();
                }
            }
        }

        /*
         * Redirect to the intended page after successful sign in
         */
        $redirectUrl = $this->pageUrl($this->property('redirect'))
            ?: $this->property('redirect');
        if ($redirectUrl = input('redirect', $redirectUrl)) {
            return Redirect::to($redirectUrl);
        }
    }

    public function onRegister()
    {
        if ($main = ScheduleHelper::instance()->getMainSchedule()) {
            if ($main->isPast()) {
                return Redirect::to('/');
            }
        }
        try {
            $data = post();
            if (array_key_exists('phone', $data) && array_key_exists('phoneCode', $data)) {
                $data['phone'] = str_replace(' ', '', $data['phoneCode'].$data['phone']);
                unset($data['phoneCode']);
            }

            $data['ip'] = $this->authManager->ipAddress;
            $geoip = GeoipModel::where('ip', $this->authManager->ipAddress)->first();
            if ($geoip) {
                $data['country_id'] = $geoip->country_id;
                if (!array_key_exists('city_id', $data)) {
                    $data['city_id'] = $geoip->city_id;
                }
            }

            if (!array_key_exists('is_suitable', $data)) {
                if (SessionService::get('is_suitable')) {
                    $data['is_suitable'] =  1;
                }
            }

            if (!array_key_exists('birthday', $data)) {
                if (SessionService::has('birthday')) {
                    $birthday = SessionService::get('birthday');
                    if (!empty($birthday) && is_array($birthday)) {
                        $data['birthday'] = Carbon::parse($birthday['year'].'-'.$birthday['month'].'-'.$birthday['day']);
                    }
                }
            }
            if (array_key_exists('age', $data)) {
                if ($data['age'] >= 18 ) {
                    $data['is_suitable'] =  1;
                }
            }

            if (array_key_exists('email', $data)) {
                $data['email'] = mb_strtolower($data['email']);
            }

            if (array_key_exists('street', $data)) {
                $data['street'] = (!$data['street']) ? '-' : $data['street'];
            }

            if (array_key_exists('house', $data)) {
                $data['house'] = (!$data['house']) ? '-' : $data['house'];
            }

            if (array_key_exists('apartment', $data)) {
                $data['apartment'] = (!$data['apartment']) ? '-' : $data['apartment'];
            }

            $rules = [
                'name' => 'required|max:64',
                'email' => 'required|unique:kosmo_profile_users|email|between:6,64',
                'city_id' => 'required',
                'is_agree' => 'required',
                'is_suitable' => 'required'
            ];

            if (in_array(env('APP_ENV'), ['kz', 'dev'])) {
                $rules['phone']  = 'required|numeric|unique:kosmo_profile_users|digits:10';
                $rules['age']    = 'required|numeric';
                $rules['gender'] = 'required';
            }
            if (in_array(env('APP_ENV'), ['kg', 'az'])) {
                $rules['phone'] = 'required|numeric|unique:kosmo_profile_users|digits:9';
            }
            if (in_array(env('APP_ENV'), ['by'])) {
                $rules['phone'] = 'required|numeric|unique:kosmo_profile_users|digits:9';
                $rules['surname'] = 'required|max:64';
                $rules['middlename'] = 'required|max:64';
                $rules['street'] = 'required|max:64';
                $rules['house'] = 'required|max:10';
                $rules['apartment'] = 'required|max:10';
                $rules['state_name'] = 'max:64';
                $rules['district_name'] = 'max:64';
                $rules['zip'] = 'max:6';
            }

            $social = $this->param('social');
            if (!$social && !isset($this->socialManager[$social])) {
                $rules['password']  = 'required|between:4,16';
            }

            $validation = Validator::make($data, $rules);
            if ($validation->fails()) {
                throw new ValidationException($validation);
            }

            $rglData = [];
            $rglData['promo_language']    = Translator::instance()->getLocale();
            $rglData['user_phone']        = FormatHelper::phoneWithPrefix($data['phone'], false);
            $rglData['user_email']        = $data['email'];
            $rglData['user_firstname']    = $data['name'];
            $rglData['user_lastname']     = '';
            $rglData['user_middlename']   = '';
            $rglData['user_age']          = $data['age'];
            $rglData['user_gender']       = $data['gender'];
            if ($city = Location::find($data['city_id'])) {
                $rglData['user_city']     = $city->name;
            } else {
                $rglData['user_city']     = '';
            }

            if (env('USE_RGL')) {
                $response = LCS::instance()->checkPhone($rglData);
            }

            /*
             * Register inviter
             */

            // if (in_array(env('APP_ENV'), ['kz', 'az', 'dev'])) {
            //     $prizesCount = Prize::available()->assured()->count();
            //     $referrer = (int)Request::cookie('referrer');
            //     if ($referrer && $prizesCount) {
            //         if ($inviter = UserModel::find($referrer))
            //         {
            //             if ($inviter->isFriendsNotLimit()) {
            //                 $data['referrer_id'] = $inviter->id;
            //             }
            //         }
            //     }
            // }

            /*
             * Register user
             */
            $user = $this->authManager->register($data, true);
            $this->authManager->login($user);

            /*
             * Notify
             */

            // if (in_array(env('APP_ENV'), ['kz', 'az', 'dev'])) {
            //     if (isset($data['referrer_id'])) {
            //         $notifier = new Notifier('Referral');
            //         $notifier->notify(['id' => $data['referrer_id']]);
            //     }
            // }


            /*
            * Redirect to the intended page after successful sign in
            */

            // if (env('APP_ENV') == 'by') {
            //     $redirectUrl = $this->pageUrl($this->property('redirect'))
            //         ?: $this->property('redirect');
            //     if ($redirectUrl = input('redirect', $redirectUrl)) {
            //         return Redirect::to($redirectUrl);
            //     }
            // } else {

                $redirectUrl = $this->pageUrl($this->property('greetings'))
                    ?: $this->property('greetings');
                if ($redirectUrl = input('greetings', $redirectUrl)) {
                    return Redirect::to($redirectUrl);
                }

            // }

        }
        catch (Exception $ex) {
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }
    }

    public function onCallback()
    {
        $social = $this->param('social');
        if (!$social && !isset($this->authManager[$social])) {
            return;
        }

        $user = $this->onSocialSignin($social);
        if (!$user) {
            return $this->onSocialRegister($social);
        }

        $this->authManager->login($user);

        /*
         * Redirect to the intended page after successful sign in
         */
        $redirectUrl = $this->pageUrl($this->property('redirect'))
            ?: $this->property('redirect');

        if ($redirectUrl = input('redirect', $redirectUrl)) {
            return Redirect::intended($redirectUrl);
        }
        return;
    }

    public function onSocialSignin($social)
    {
        if (!isset($this->socialManager[$social])) {
            return;
        }

        $userId = $this->socialManager[$social]->getUserId(Request::get('code'));
        if (!$userId) {
            return;
        }

        $user = UserModel::findBySocial($social, $userId);
        if (!$user) {
            return;
        }

        return $user;
    }

    public function onSocialRegister($social)
    {
        if(!!env('noreg'))
            return Redirect::to('/');

        if (!isset($this->socialManager[$social])) {
            return;
        }

        $userProfile = $this->socialManager[$social]->getUserProfile();
        if (!$userProfile) {
            return;
        }
        SessionService::put('profile', $userProfile);
        return Redirect::to('signup/'.$social);
    }
}
