<?php namespace Kosmo\Profile\Components;

use Cms\Classes\ComponentBase;
use Mail;
use Cookie;
use Validator;
use ValidationException;
use ApplicationException;
use Kosmo\Profile\Classes\AuthManager;
use Kosmo\Profile\Models\User as UserModel;

class ResetPassword extends ComponentBase
{
    const RESTORED_PERIOD = 60;

    protected $authManager;

    public function componentDetails()
    {
        return [
            'name'        => 'kosmo.profile::lang.reset_password.reset_password',
            'description' => 'kosmo.profile::lang.reset_password.reset_password_desc'
        ];
    }

    public function defineProperties()
    {
        return [
            'paramCode' => [
                'title'       => 'kosmo.profile::lang.reset_password.code_param',
                'description' => 'kosmo.profile::lang.reset_password.code_param_desc',
                'type'        => 'string',
                'default'     => 'code'
            ]
        ];
    }

    public function init()
    {
        $this->authManager = AuthManager::instance();
    }

    public function onRun()
    {
        $this->page['reset'] = $this->code();
    }

    /**
     * Trigger the password reset email
     */
    public function onRestorePassword()
    {
        if ($this->getRestored()) {
            return;
        }
        $rules = [
            'email' => 'required|email|between:2,64'
        ];

        $validation = Validator::make(post(), $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        if (!$user = UserModel::findByEmail(post('email'))) {
            throw new ApplicationException('restore');
        }

        $code = implode('!', [$user->id, $user->getResetPasswordCode()]);
        $link = $this->controller->currentPageUrl([
            $this->property('paramCode') => $code
        ]);

        $data = [
            'name' => $user->name,
            'link' => $link,
            'code' => $code
        ];

        Mail::send('kosmo.profile.mail.restore', $data, function($message) use ($user) {
            $message->to($user->email, $user->name);
        });
        $this->setRestored();
        return;
    }

    private function getRestored()
    {
        if (env('FEEDBACK_TEST')) {
            return;
        }
        if ($restored = Cookie::get('pswrestored')) {
            return true;
        }
    }

    private function setRestored()
    {
        if (env('FEEDBACK_TEST')) {
            return;
        }
        Cookie::queue('pswrestored', time(), self::RESTORED_PERIOD);
    }

    /**
     * Perform the password reset
     */
    public function onResetPassword()
    {
        $rules = [
            'code' => 'required',
            'password' => 'required|between:4,16',
            'password-confirmation' => 'same:password'
        ];

        $validation = Validator::make(post(), $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        /*
         * Break up the code parts
         */
        $parts = explode('!', post('code'));
        if (count($parts) != 2) {
            throw new ApplicationException('reset');
        }

        list($userId, $code) = $parts;

        if (!strlen(trim($userId)) || !($user = $this->authManager->findUserById($userId))) {
            throw new ApplicationException('reset');
        }

        if (!$user->attemptResetPassword($code, post('password'))) {
            throw new ApplicationException('reset');
        }
    }

    /**
     * Returns the reset password code from the URL
     * @return string
     */
    public function code()
    {
        $routeParameter = $this->property('paramCode');
        return $this->param($routeParameter);
    }

}
