<?php namespace Kosmo\Profile\Components;

use Kosmo\Profile\Classes\AuthManager;
use Flash;
use Lang;
use Request;
use Redirect;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use ValidationException;

class Session extends ComponentBase
{
    const ALLOW_ALL = 'all';
    const ALLOW_GUEST = 'guest';
    const ALLOW_USER = 'user';

    protected $authManager;

    public function componentDetails()
    {
        return [
            'name'        => 'kosmo.profile::lang.session.session',
            'description' => 'kosmo.profile::lang.session.session_desc'
        ];
    }

    public function defineProperties()
    {
        return [
            'security' => [
                'title'       => 'kosmo.profile::lang.session.security_title',
                'description' => 'kosmo.profile::lang.session.security_desc',
                'type'        => 'dropdown',
                'default'     => 'all',
                'options'     => [
                    'all'   => 'kosmo.profile::lang.session.all',
                    'user'  => 'kosmo.profile::lang.session.users',
                    'guest' => 'kosmo.profile::lang.session.guests'
                ]
            ],
            'redirect' => [
                'title'       => 'kosmo.profile::lang.session.redirect_title',
                'description' => 'kosmo.profile::lang.session.redirect_desc',
                'type'        => 'dropdown',
                'default'     => ''
            ]
        ];
    }

    public function init()
    {
        $this->authManager = AuthManager::instance();
    }

    public function getRedirectOptions()
    {
        return [''=>'- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    /**
     * Executed when this component is bound to a page or layout.
     */
    public function onRun()
    {
        $redirectUrl = $this->controller->pageUrl($this->property('redirect'));
        $allowedGroup = $this->property('security', self::ALLOW_ALL);
        $isAuthenticated = $this->authManager->check();

        if (!$isAuthenticated && $allowedGroup == self::ALLOW_USER) {
            return Redirect::guest($redirectUrl);
        }
        elseif ($isAuthenticated && $allowedGroup == self::ALLOW_GUEST) {
            return Redirect::guest($redirectUrl);
        }

        $this->page['user'] = $this->user();
    }

    /**
     * Log out the user
     *
     * Usage:
     *   <a data-request="onLogout">Sign out</a>
     *
     * With the optional redirect parameter:
     *   <a data-request="onLogout" data-request-data="redirect: '/good-bye'">Sign out</a>
     *
     */
    public function onLogout()
    {
        $this->authManager->logout();
        $url = post('redirect', Request::fullUrl());
        Flash::success(Lang::get('kosmo.profile::lang.session.logout'));

        return Redirect::to($url);
    }

    /**
     * Returns the logged in user, if available
     */
    public function user()
    {
        if (!$this->authManager->check()) {
            return null;
        }

        return $this->authManager->getUser();
    }
}
