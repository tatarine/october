<?php namespace Kosmo\Profile\Components;

use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Mail;
use Validator;
use Request;
use Redirect;
use ValidationException;
use ApplicationException;
use Kosmo\Quiz\Classes\Quiz;
use Kosmo\Profile\Classes\AuthManager;
use Kosmo\Profile\Models\User as UserModel;

class ConfirmEmail extends ComponentBase
{
    protected $authManager;

    public function componentDetails()
    {
        return [
            'name'        => 'kosmo.profile::lang.confirm_email.confirm_email',
            'description' => 'kosmo.profile::lang.confirm_email.confirm_email_desc'
        ];
    }

    public function defineProperties()
    {
        return [
            'paramCode' => [
                'title'       => 'kosmo.profile::lang.confirm_email.code_param',
                'description' => 'kosmo.profile::lang.confirm_email.code_param_desc',
                'type'        => 'string',
                'default'     => 'code'
            ],
            'redirect' => [
                'title'       => 'kosmo.profile::lang.session.redirect_title',
                'description' => 'kosmo.profile::lang.session.redirect_desc',
                'type'        => 'dropdown',
                'default'     => 'confirm'
            ],
            'confirmationCode' => [
                'title'       => 'kosmo.profile::lang.session.redirect_title',
                'description' => 'kosmo.profile::lang.session.redirect_desc',
                'type'        => 'string',
                'default'     => ''
            ],
        ];
    }

    public function init()
    {
        $this->authManager = AuthManager::instance();
    }

    public function onRun()
    {
        $code = $this->property('confirmationCode');
        if (!$code) {
            return;
        }

        $rules = [
            $this->property('paramCode') => 'required'
        ];

        $validation = Validator::make([$this->property('paramCode') => $code], $rules);
        if ($validation->fails()) {
            return Redirect::to('cabinet');
        }

        /*
         * Break up the code parts
         */
        $parts = explode('!', $code);
        if (count($parts) != 2) {
            return Redirect::to('cabinet');
        }

        list($userId, $code) = $parts;

        if (!strlen(trim($userId)) || !($user = $this->authManager->findUserById($userId))) {
            return Redirect::to('cabinet');
        }

        if (!$user->attemptActivation($code)) {
            return Redirect::to('cabinet');
        }
        /*
         * Raffle prize
         */
        // Quiz::raffleReferral($user->referrer);
        // Quiz::raffleReferral($user);

        return Redirect::to('cabinet');
    }

    public function getRedirectOptions()
    {
        return [''=>'- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    /**
     * Trigger the password reset email
     */
    public function onConfirmationRequest($user = null)
    {
        if (!$user) {
            if (!$user = $this->user()) {
                throw new ApplicationException(trans('kosmo.profile::lang.account.invalid_user'));
            }
        }
        $code = implode('!', [$user->id, $user->getActivationCode()]);

        if ($this->controller) {
            $link = $this->controller->pageUrl($this->property('redirect'), [
                $this->property('paramCode') => $code
            ]);
        } else {
            $link = 'http://'.env('APP_DOMAIN').'/'.$this->property('redirect').'/'.$code;
        }

        $data = [
            'name' => $user->name,
            'link' => $link,
            'code' => $code
        ];

        $user->is_activated = 0;
        $user->activated_at = null;
        $user->save();

        Mail::send('kosmo.profile.mail.confirm', $data, function($message) use ($user) {
            $message->to($user->email, $user->name);
        });
    }

    /**
     * Perform the password reset
     */
    public function onEmailConfirm()
    {
        $rules = [
            $this->property('paramCode') => 'required'
        ];

        $validation = Validator::make(post(), $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        /*
         * Break up the code parts
         */
        $parts = explode('!', $this->code());
        if (count($parts) != 2) {
            throw new ValidationException(['code' => trans('kosmo.profile::lang.account.invalid_activation_code')]);
        }

        list($userId, $code) = $parts;

        if (!strlen(trim($userId)) || !($user = $this->authManager->findUserById($userId))) {
            throw new ApplicationException(trans('kosmo.profile::lang.account.invalid_user'));
        }

        if (!$user->attemptActivation($code)) {
            throw new ValidationException(['code' => trans('kosmo.profile::lang.account.invalid_activation_code')]);
        }
    }

    /**
     * Returns the reset password code from the URL
     * @return string
     */
    public function code()
    {
        $routeParameter = $this->property('paramCode');
        return $this->param($routeParameter);
    }

    /**
     * Returns the logged in user, if available
     */
    public function user()
    {
        if (!$this->authManager->check()) {
            return null;
        }

        return $this->authManager->getUser();
    }
}
